﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SciLeague.APIGateway.Helpers;
using SciLeague.APIGateway.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.APIGateway.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class GatewayController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public GatewayController(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        [HttpPost("InternalLogin")]
        public async Task<String> InternalLogin(LoginRequest loginRequest)
        {
            HttpRequests httpRequests = new HttpRequests();

            string uri = Configuration["ServicesBaseURL:AuthService"] + "accounts/login";
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(loginRequest);
            string contentType = "application/json";
            return await httpRequests.PostAsync(uri, data, contentType);  
        }

        [HttpPost("SignUp")]
        public async Task<String> SignUp(SignupRequest signupRequest)
        {
            HttpRequests httpRequests = new HttpRequests();

            string uri = Configuration["ServicesBaseURL:AuthService"] + "accounts/CreateAccount";
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(signupRequest);
            string contentType = "application/json";
            return await httpRequests.PostAsync(uri, data, contentType);
        }

        [HttpPost("EditProfile")]
        public async Task<String> EditProfile(ProfileRequest profileRequest)
        {
            string result;

            HttpRequests httpRequests = new HttpRequests();

            string uri = Configuration["ServicesBaseURL:AuthService"] + "profiles/CreateProfile";
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(profileRequest);
            string contentType = "application/json";

            result = await httpRequests.PostAsync(uri, data, contentType);

            //Criar UserBadge.
            uri = Configuration["ServicesBaseURL:BadgeService"] + "userBadges/CreateUserBadgeForUsername?username=" + profileRequest.Username + "&level=" + 0;
            await httpRequests.PostAsync(uri, "", contentType);
            return result;
        }

        [HttpGet("GetUserPoints")]
        public async Task<String> GetUserPoints([FromHeader] string authorization)
        {
            string username;
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:AuthService"] + "profiles/GetTotalUserPoints?username=" + username;
            return await httpRequests.GetAsync(uri);
        }

        [HttpGet("GetMyProfile")]
        public async Task<String> GetMyProfile([FromHeader] string authorization)
        {
            string username;
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:AuthService"] + "profiles/getMyProfile?username=" + username;
            return await httpRequests.GetAsync(uri);
        }

        [HttpGet("GetLeaderboardPosition")]
        public async Task<String> GetLeaderboardPosition([FromHeader] string authorization)
        {
            string username;
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:AuthService"] + "profiles/GetLeaderboardPosition?username=" + username;
            return await httpRequests.GetAsync(uri);
        }


        [HttpPost("VerifyBadgeStatus")]
        public async Task<String> VerifyBadgeStatus([FromHeader] string authorization)
        {
            string username; 
            int numberDays;
            
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            HttpRequests httpRequests = new HttpRequests();

            // Get number of consecutive days (not) played.
            string uri = Configuration["ServicesBaseURL:TaskService"] + "playerTasks/GetNumberOfConsecutivePlayDays?username=" + username;
            numberDays = int.Parse(await httpRequests.GetAsync(uri));

            // Verify if there is a badge promotion/removal.
            uri = Configuration["ServicesBaseURL:BadgeService"] + "userBadges/VerifyBadgeStatus?numberDays=" + numberDays + "&username=" + username;
            string contentType = "application/json";
            return await httpRequests.PostAsync(uri, "", contentType);
        }

        [HttpGet("GetAllActiveBadgesFromUser")]
        public async Task<String> GetAllActiveBadgesFromUser([FromHeader] string authorization)
        {
            string username;
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:BadgeService"] + "userBadges/GetAllActiveBadgesFromUser?username=" + username;
            return await httpRequests.GetAsync(uri);
        }

        [HttpGet("GetLeaderboardPlayers")]
        public async Task<String> GetLeaderboardPlayers(int limit)
        {
            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:AuthService"] + "profiles/GetMonthlyTopPlayers?limit=" + limit;
            return await httpRequests.GetAsync(uri);
        }


        [HttpGet("GetTaskPoints")]
        public async Task<String> GetTaskPoints(int taskID)
        {
            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:TaskService"] + "tasks/GetTaskPoints?taskID=" + taskID;
            return await httpRequests.GetAsync(uri);
        }


        [HttpGet("AskForAvailableMultiChoiceTask")]
        public async Task<String> AskForAvailableMultiChoiceTask([FromHeader] string authorization)
        {
            string username;
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:TaskService"] + "multipleChoiceTasks/GetAvailableTaskForUser?username=" + username;
            return await httpRequests.GetAsync(uri);
        }

        [HttpGet("AskForMultiChoiceTaskMainBody")]
        public async Task<String> AskForMultiChoiceTaskMainBody(string taskID)
        {
            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:TaskService"] + "multipleChoiceTasks/GetMultipleChoiceTask?id=" + taskID;
            return await httpRequests.GetAsync(uri);
        }

        [HttpGet("AskForMultiChoiceTaskChoices")]
        public async Task<String> AskForMultiChoiceTaskChoices(string taskID)
        {
            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:TaskService"] + "choices/GetChoices?id=" + taskID;
            return await httpRequests.GetAsync(uri);
        }

        [HttpPost("MultipleChoiceCompleted")]
        public async Task<String> MultipleChoiceCompleted([FromBody] MultipleChoiceCompletedRequest body, [FromHeader] string authorization)
        {
            string username;
            AuthenticateToken authenticateToken = new AuthenticateToken(Configuration);
            if ((username = await authenticateToken.GetUserAuth(authorization, "player")) == null)
            {
                return "The user doesn't exist, is not logged or doesn't have the necessary role.";
            }

            // Create PlayerTasks.
            HttpRequests httpRequests = new HttpRequests();
            string uri = Configuration["ServicesBaseURL:TaskService"] + "playerTasks/PostPlayerTasks";

            var data = new { Username = username, TaskId = body.TaskId };
            string jsonData = JsonConvert.SerializeObject(data);
            string contentType = "application/json";

            int playerTasksID = int.Parse(await httpRequests.PostAsync(uri, jsonData, contentType));

            // Create ContributionMultiChoiceTask.
            httpRequests = new HttpRequests();
            uri = Configuration["ServicesBaseURL:TaskService"] + "ContributionMultiChoiceTasks/PostContributionMultiChoiceTask";

            var data2 = new
            {
                playerTasksID = playerTasksID,
                correctChoiceAnswered = body.CorrectChoiceSelected,
                attempsNeeded = body.AttemptsNeeded,
                pointsConquered = body.Points,
                selectedChoiceID = body.SelectedChoiceID
            };

            string jsonData2 = JsonConvert.SerializeObject(data2);
            await httpRequests.PostAsync(uri, jsonData2, contentType);

            // Update user's points.
            httpRequests = new HttpRequests();
            uri = Configuration["ServicesBaseURL:AuthService"] + "profiles/UpdatePoints?username=" + username + "&points=" + body.Points;
            await httpRequests.PostAsync(uri, "", contentType);

            return "Submitted";
        }
    }
}
