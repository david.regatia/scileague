﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SciLeague.APIGateway.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SciLeague.APIGateway.Helpers
{
    public class AuthenticateToken
    {
        private readonly IConfiguration Configuration;
        public AuthenticateToken(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<string> GetUserAuth(string token, string rolePermitted)
        {
            string uri = Configuration["ServicesBaseURL:AuthService"] + "accounts/GetUserAuthAsync";
            UserIdentified userIdentified = new UserIdentified();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.Headers.Add("Authorization", token);

            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                userIdentified = JsonConvert.DeserializeObject<UserIdentified>(await reader.ReadToEndAsync());

                if (userIdentified.Username != "" && userIdentified.Role == rolePermitted)
                {
                    return userIdentified.Username;
                }
            }

            return null;
        } 
    }
}
