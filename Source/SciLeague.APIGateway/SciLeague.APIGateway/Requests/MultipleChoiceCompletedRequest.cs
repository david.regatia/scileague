﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.APIGateway.Requests
{
    public class MultipleChoiceCompletedRequest
    {
        public int TaskId { get; set; }
        public int SelectedChoiceID { get; set; }
        public int Points { get; set; }
        public int AttemptsNeeded { get; set; }
        public bool CorrectChoiceSelected { get; set; }

    }
}
