﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31;
// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>
struct Action_1_t7A6E0E94B21008B707D8BEA0DA19B65A96D58416;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>
struct Dictionary_2_tAFDD9B13B8A8D35E8017B1AD49EE8DCF947CD740;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<SimpleJSON.JSONNode>
struct List_1_t5575902E70199AF34CD1695997CE7E53A1509646;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<AU_ReportController/Imposter>
struct List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>[]
struct Action_1U5BU5D_t960376E04EEB1EB1830312AF536B2B78F2FF40B0;
// AU_InteractableController[]
struct AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.InputSystem.InputBinding[]
struct InputBindingU5BU5D_t3CCBD5F5A51F370BFD0A373A770C8720F2A6115E;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// AU_ReportController/Imposter[]
struct ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC;
// AU_BotController
struct AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308;
// AU_CompassController
struct AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4;
// AU_Events_ManagerController
struct AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4;
// AU_Game_ManagerController
struct AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148;
// AU_ImposterController
struct AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8;
// AU_InteractableController
struct AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9;
// AU_Multiple_Choice_TaskController
struct AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430;
// AU_PlayerController
struct AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6;
// AU_ReportController
struct AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073;
// AU_Task_APIController
struct AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Text.DecoderFallback
struct DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D;
// System.Text.EncoderFallback
struct EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.InputSystem.InputAction
struct InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48;
// UnityEngine.InputSystem.InputActionMap
struct InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// SimpleJSON.JSONNode
struct JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// MultipleChoiceRequest
struct MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// TimerController
struct TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA;
// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// AU_ReportController/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308;
// AU_ReportController/Imposter
struct Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7;
// AU_Task_APIController/<GetImposterCitationSentimentTask>d__19
struct U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B;
// AU_Task_APIController/<SendTask>d__23
struct U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// TimerController/<UpdateTimer>d__11
struct U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB;

IL2CPP_EXTERN_C RuntimeClass* AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral12030968A66BA0D54A39FD2688C2FDF347383378;
IL2CPP_EXTERN_C String_t* _stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D;
IL2CPP_EXTERN_C String_t* _stringLiteral1896B6FF3B9E74F32807076D8C351919AEB693FC;
IL2CPP_EXTERN_C String_t* _stringLiteral1D4C35F5A2200BC0FC64F8385F11A8183DCE826C;
IL2CPP_EXTERN_C String_t* _stringLiteral2293036C634A08D112127B21C69759506FF3E36E;
IL2CPP_EXTERN_C String_t* _stringLiteral299E01A3C227A338CCCF7D17E88F26B036E2B8EC;
IL2CPP_EXTERN_C String_t* _stringLiteral2B4DA93E5AFF4264BB03A9D00D4D58669A0E137D;
IL2CPP_EXTERN_C String_t* _stringLiteral331F4DFDDDEF540325D068392B749A7EF80E4C56;
IL2CPP_EXTERN_C String_t* _stringLiteral3EC7D17D68A82AF5B0C004C2365B58852ECE2F06;
IL2CPP_EXTERN_C String_t* _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078;
IL2CPP_EXTERN_C String_t* _stringLiteral4EFFFD13F5002C5417C2092510108CFA784C4B16;
IL2CPP_EXTERN_C String_t* _stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18;
IL2CPP_EXTERN_C String_t* _stringLiteral5C70E315F7123B7B54B08D851EEFD2C450442E31;
IL2CPP_EXTERN_C String_t* _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32;
IL2CPP_EXTERN_C String_t* _stringLiteral627BB866689D36FF5337A5C9A7B4D5A35A72534C;
IL2CPP_EXTERN_C String_t* _stringLiteral64058CC688A96A90239811EF06C9D20DB0499C3E;
IL2CPP_EXTERN_C String_t* _stringLiteral64CFC92D261DA2B74CED8151D781601D05C3224F;
IL2CPP_EXTERN_C String_t* _stringLiteral67E5FF74526E1D92F88CA14F6FF0C12733893B44;
IL2CPP_EXTERN_C String_t* _stringLiteral7649AEE062EE200D5810926162F39A75BCEE5287;
IL2CPP_EXTERN_C String_t* _stringLiteral88B3600712D8F762131B3FE13B12305C2B95E0BD;
IL2CPP_EXTERN_C String_t* _stringLiteral90A8DF8EB02BB70BC2CB29F3C32E3EA12510D67F;
IL2CPP_EXTERN_C String_t* _stringLiteral9151D78C7350390CAFAC55F797FC835910057561;
IL2CPP_EXTERN_C String_t* _stringLiteral9669FB3E21BDE2C2BFC3E709CFA73EF5A97B3B2C;
IL2CPP_EXTERN_C String_t* _stringLiteral996E5360F80E16B2189CC1E536C91CE68083F694;
IL2CPP_EXTERN_C String_t* _stringLiteral9C504D43C27CCB8D70AE2159F1DCAF7433D4FBF0;
IL2CPP_EXTERN_C String_t* _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907;
IL2CPP_EXTERN_C String_t* _stringLiteralADE30E7D46F70F2DF18542CB2B831FC2271C5335;
IL2CPP_EXTERN_C String_t* _stringLiteralAE8900DC91B51B8520DDF29118082DF79022F298;
IL2CPP_EXTERN_C String_t* _stringLiteralB07D84E310F5267DA77D2245B52128D405C8FF6F;
IL2CPP_EXTERN_C String_t* _stringLiteralB0876FD70181B47400B01C8D4BDD907A9F69D4D5;
IL2CPP_EXTERN_C String_t* _stringLiteralB8CD08AB65F2F3D1C9BE01211D5F2BF4AC263690;
IL2CPP_EXTERN_C String_t* _stringLiteralC0A82FDE1B57568EAC7AE4186DC9B277D397A29C;
IL2CPP_EXTERN_C String_t* _stringLiteralC15F9B9EBEDDDB20A27F1D25C7E06F7F21F3F96D;
IL2CPP_EXTERN_C String_t* _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70;
IL2CPP_EXTERN_C String_t* _stringLiteralCDEBA5192C137E7688193AAC71C2B4DE462929A3;
IL2CPP_EXTERN_C String_t* _stringLiteralDBED589A0467FDD14250BED4A3A8E475671181AD;
IL2CPP_EXTERN_C String_t* _stringLiteralEB5328652C8433B5660F353A2C264BE20A9E2B2C;
IL2CPP_EXTERN_C String_t* _stringLiteralEDC146DE35595FFABFFF8DA1CBB936C9E75A305F;
IL2CPP_EXTERN_C String_t* _stringLiteralF15E72E03C607420BD2E3D65E07F0C50155C1C30;
IL2CPP_EXTERN_C String_t* _stringLiteralF1FFE25608DB132DAE3BB59C8C9FDD13CE5C0399;
IL2CPP_EXTERN_C String_t* _stringLiteralF3A86EBDBF8EA9FCD0E9713006C9251860EBB9CA;
IL2CPP_EXTERN_C String_t* _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9;
IL2CPP_EXTERN_C const RuntimeMethod* AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mCBD44A97260E33F58E2FBBD76E0432C8F120C30E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_m07218953750B94DD5E57038A5CBDD7CCDA8C35D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA_m11D762F94106F90C52A14CE1D94F8480687FAEA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m6A1D5267B49F39503A0BF02B983C18FA2EB5094A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mAB159CED4EA42F6E700527382D795CCD31EF293F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m07C610356AB5C68760B3F1A16391671DCCD8FC89_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m809A2F3C03A4DFC84691D8615C8413FBC0039401_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mC3C92BFD7725EFE888D9CD9022D7FD8B62797903_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m77F12A63EB4FD22AA4D725B91DC135C20B32AE0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m85FD3AD1E7C2C143E7D5491BBB781F1C3DA22B85_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetImposterCitationSentimentTaskU3Ed__19_System_Collections_IEnumerator_Reset_m7069DA4DD0A91724FC51133EB53DC3B8908AB7BA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSendTaskU3Ed__23_System_Collections_IEnumerator_Reset_m9630B623DDBD3BFEE709FFD4E1B3F61382167D55_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass21_0_U3CSubmitTaskInfoU3Eb__0_m06BB4E9F1DF5E5B8AE81EB38B23A4075576B0BEB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CUpdateTimerU3Ed__11_System_Collections_IEnumerator_Reset_m3A48CBE0E25B23CA678E3A24B8D4E29A699B263E_RuntimeMethod_var;
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com;
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke;
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com;

struct AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570;
struct ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B;
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.EmptyArray`1<System.Object>
struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E, ____items_1)); }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* get__items_1() const { return ____items_1; }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_StaticFields, ____emptyArray_5)); }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____items_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<AU_ReportController/Imposter>
struct List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67, ____items_1)); }
	inline ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC* get__items_1() const { return ____items_1; }
	inline ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_StaticFields, ____emptyArray_5)); }
	inline ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ImposterU5BU5D_t62EFD8182C9A27F74043ED6C295E86478ECFB2AC* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E  : public RuntimeObject
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jobject
	GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * ___m_jobject_1;
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jclass
	GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * ___m_jclass_2;

public:
	inline static int32_t get_offset_of_m_jobject_1() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E, ___m_jobject_1)); }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * get_m_jobject_1() const { return ___m_jobject_1; }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 ** get_address_of_m_jobject_1() { return &___m_jobject_1; }
	inline void set_m_jobject_1(GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * value)
	{
		___m_jobject_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jobject_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_jclass_2() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E, ___m_jclass_2)); }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * get_m_jclass_2() const { return ___m_jclass_2; }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 ** get_address_of_m_jclass_2() { return &___m_jclass_2; }
	inline void set_m_jclass_2(GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * value)
	{
		___m_jclass_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jclass_2), (void*)value);
	}
};

struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_StaticFields
{
public:
	// System.Boolean UnityEngine.AndroidJavaObject::enableDebugPrints
	bool ___enableDebugPrints_0;

public:
	inline static int32_t get_offset_of_enableDebugPrints_0() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_StaticFields, ___enableDebugPrints_0)); }
	inline bool get_enableDebugPrints_0() const { return ___enableDebugPrints_0; }
	inline bool* get_address_of_enableDebugPrints_0() { return &___enableDebugPrints_0; }
	inline void set_enableDebugPrints_0(bool value)
	{
		___enableDebugPrints_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___dataItem_10)); }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t09A62F57142BF0456C8F414898A37E79BCC9F09E * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataItem_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___encoderFallback_13)); }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_t02AC990075E17EB09F0D7E4831C3B3F264025CC4 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoderFallback_13), (void*)value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827, ___decoderFallback_14)); }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_tF86D337D6576E81E5DA285E5673183EBC66DEF8D * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___decoderFallback_14), (void*)value);
	}
};

struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultEncoding_0), (void*)value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unicodeEncoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bigEndianUnicode_2), (void*)value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf7Encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf8Encoding_4), (void*)value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___utf32Encoding_5), (void*)value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asciiEncoding_6), (void*)value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latin1Encoding_7), (void*)value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___encodings_8)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encodings_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InternalSyncObject_15), (void*)value);
	}
};


// SimpleJSON.JSONNode
struct JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043  : public RuntimeObject
{
public:

public:
};

struct JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_StaticFields
{
public:
	// System.Boolean SimpleJSON.JSONNode::forceASCII
	bool ___forceASCII_0;

public:
	inline static int32_t get_offset_of_forceASCII_0() { return static_cast<int32_t>(offsetof(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_StaticFields, ___forceASCII_0)); }
	inline bool get_forceASCII_0() const { return ___forceASCII_0; }
	inline bool* get_address_of_forceASCII_0() { return &___forceASCII_0; }
	inline void set_forceASCII_0(bool value)
	{
		___forceASCII_0 = value;
	}
};

struct JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_ThreadStaticFields
{
public:
	// System.Text.StringBuilder SimpleJSON.JSONNode::m_EscapeBuilder
	StringBuilder_t * ___m_EscapeBuilder_1;

public:
	inline static int32_t get_offset_of_m_EscapeBuilder_1() { return static_cast<int32_t>(offsetof(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_ThreadStaticFields, ___m_EscapeBuilder_1)); }
	inline StringBuilder_t * get_m_EscapeBuilder_1() const { return ___m_EscapeBuilder_1; }
	inline StringBuilder_t ** get_address_of_m_EscapeBuilder_1() { return &___m_EscapeBuilder_1; }
	inline void set_m_EscapeBuilder_1(StringBuilder_t * value)
	{
		___m_EscapeBuilder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EscapeBuilder_1), (void*)value);
	}
};


// MultipleChoiceRequest
struct MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290  : public RuntimeObject
{
public:
	// System.String MultipleChoiceRequest::Username
	String_t* ___Username_0;
	// System.Int32 MultipleChoiceRequest::TaskID
	int32_t ___TaskID_1;
	// System.Int32 MultipleChoiceRequest::SelectedChoiceID
	int32_t ___SelectedChoiceID_2;
	// System.Int32 MultipleChoiceRequest::Points
	int32_t ___Points_3;
	// System.Int32 MultipleChoiceRequest::AttemptsNeeded
	int32_t ___AttemptsNeeded_4;
	// System.Boolean MultipleChoiceRequest::CorrectChoiceSelected
	bool ___CorrectChoiceSelected_5;

public:
	inline static int32_t get_offset_of_Username_0() { return static_cast<int32_t>(offsetof(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290, ___Username_0)); }
	inline String_t* get_Username_0() const { return ___Username_0; }
	inline String_t** get_address_of_Username_0() { return &___Username_0; }
	inline void set_Username_0(String_t* value)
	{
		___Username_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Username_0), (void*)value);
	}

	inline static int32_t get_offset_of_TaskID_1() { return static_cast<int32_t>(offsetof(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290, ___TaskID_1)); }
	inline int32_t get_TaskID_1() const { return ___TaskID_1; }
	inline int32_t* get_address_of_TaskID_1() { return &___TaskID_1; }
	inline void set_TaskID_1(int32_t value)
	{
		___TaskID_1 = value;
	}

	inline static int32_t get_offset_of_SelectedChoiceID_2() { return static_cast<int32_t>(offsetof(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290, ___SelectedChoiceID_2)); }
	inline int32_t get_SelectedChoiceID_2() const { return ___SelectedChoiceID_2; }
	inline int32_t* get_address_of_SelectedChoiceID_2() { return &___SelectedChoiceID_2; }
	inline void set_SelectedChoiceID_2(int32_t value)
	{
		___SelectedChoiceID_2 = value;
	}

	inline static int32_t get_offset_of_Points_3() { return static_cast<int32_t>(offsetof(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290, ___Points_3)); }
	inline int32_t get_Points_3() const { return ___Points_3; }
	inline int32_t* get_address_of_Points_3() { return &___Points_3; }
	inline void set_Points_3(int32_t value)
	{
		___Points_3 = value;
	}

	inline static int32_t get_offset_of_AttemptsNeeded_4() { return static_cast<int32_t>(offsetof(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290, ___AttemptsNeeded_4)); }
	inline int32_t get_AttemptsNeeded_4() const { return ___AttemptsNeeded_4; }
	inline int32_t* get_address_of_AttemptsNeeded_4() { return &___AttemptsNeeded_4; }
	inline void set_AttemptsNeeded_4(int32_t value)
	{
		___AttemptsNeeded_4 = value;
	}

	inline static int32_t get_offset_of_CorrectChoiceSelected_5() { return static_cast<int32_t>(offsetof(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290, ___CorrectChoiceSelected_5)); }
	inline bool get_CorrectChoiceSelected_5() const { return ___CorrectChoiceSelected_5; }
	inline bool* get_address_of_CorrectChoiceSelected_5() { return &___CorrectChoiceSelected_5; }
	inline void set_CorrectChoiceSelected_5(bool value)
	{
		___CorrectChoiceSelected_5 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// AU_ReportController/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308  : public RuntimeObject
{
public:
	// System.Int32 AU_ReportController/<>c__DisplayClass21_0::tempInt
	int32_t ___tempInt_0;
	// AU_ReportController AU_ReportController/<>c__DisplayClass21_0::<>4__this
	AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_tempInt_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308, ___tempInt_0)); }
	inline int32_t get_tempInt_0() const { return ___tempInt_0; }
	inline int32_t* get_address_of_tempInt_0() { return &___tempInt_0; }
	inline void set_tempInt_0(int32_t value)
	{
		___tempInt_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308, ___U3CU3E4__this_1)); }
	inline AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// AU_ReportController/Imposter
struct Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7  : public RuntimeObject
{
public:
	// System.Int32 AU_ReportController/Imposter::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.Boolean AU_ReportController/Imposter::<Available>k__BackingField
	bool ___U3CAvailableU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAvailableU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7, ___U3CAvailableU3Ek__BackingField_1)); }
	inline bool get_U3CAvailableU3Ek__BackingField_1() const { return ___U3CAvailableU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAvailableU3Ek__BackingField_1() { return &___U3CAvailableU3Ek__BackingField_1; }
	inline void set_U3CAvailableU3Ek__BackingField_1(bool value)
	{
		___U3CAvailableU3Ek__BackingField_1 = value;
	}
};


// AU_Task_APIController/<GetImposterCitationSentimentTask>d__19
struct U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B  : public RuntimeObject
{
public:
	// System.Int32 AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AU_Task_APIController AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<>4__this
	AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<taskIDRequest>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CtaskIDRequestU3E5__2_3;
	// System.Int32 AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<id>5__3
	int32_t ___U3CidU3E5__3_4;
	// UnityEngine.Networking.UnityWebRequest AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<taskPointsRequest>5__4
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CtaskPointsRequestU3E5__4_5;
	// UnityEngine.Networking.UnityWebRequest AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<taskDetailsRequest>5__5
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CtaskDetailsRequestU3E5__5_6;
	// System.String AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<question>5__6
	String_t* ___U3CquestionU3E5__6_7;
	// UnityEngine.Networking.UnityWebRequest AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::<taskChoicesRequest>5__7
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CtaskChoicesRequestU3E5__7_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CU3E4__this_2)); }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtaskIDRequestU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CtaskIDRequestU3E5__2_3)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CtaskIDRequestU3E5__2_3() const { return ___U3CtaskIDRequestU3E5__2_3; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CtaskIDRequestU3E5__2_3() { return &___U3CtaskIDRequestU3E5__2_3; }
	inline void set_U3CtaskIDRequestU3E5__2_3(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CtaskIDRequestU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtaskIDRequestU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CidU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CidU3E5__3_4)); }
	inline int32_t get_U3CidU3E5__3_4() const { return ___U3CidU3E5__3_4; }
	inline int32_t* get_address_of_U3CidU3E5__3_4() { return &___U3CidU3E5__3_4; }
	inline void set_U3CidU3E5__3_4(int32_t value)
	{
		___U3CidU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CtaskPointsRequestU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CtaskPointsRequestU3E5__4_5)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CtaskPointsRequestU3E5__4_5() const { return ___U3CtaskPointsRequestU3E5__4_5; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CtaskPointsRequestU3E5__4_5() { return &___U3CtaskPointsRequestU3E5__4_5; }
	inline void set_U3CtaskPointsRequestU3E5__4_5(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CtaskPointsRequestU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtaskPointsRequestU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtaskDetailsRequestU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CtaskDetailsRequestU3E5__5_6)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CtaskDetailsRequestU3E5__5_6() const { return ___U3CtaskDetailsRequestU3E5__5_6; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CtaskDetailsRequestU3E5__5_6() { return &___U3CtaskDetailsRequestU3E5__5_6; }
	inline void set_U3CtaskDetailsRequestU3E5__5_6(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CtaskDetailsRequestU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtaskDetailsRequestU3E5__5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CquestionU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CquestionU3E5__6_7)); }
	inline String_t* get_U3CquestionU3E5__6_7() const { return ___U3CquestionU3E5__6_7; }
	inline String_t** get_address_of_U3CquestionU3E5__6_7() { return &___U3CquestionU3E5__6_7; }
	inline void set_U3CquestionU3E5__6_7(String_t* value)
	{
		___U3CquestionU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CquestionU3E5__6_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtaskChoicesRequestU3E5__7_8() { return static_cast<int32_t>(offsetof(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B, ___U3CtaskChoicesRequestU3E5__7_8)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CtaskChoicesRequestU3E5__7_8() const { return ___U3CtaskChoicesRequestU3E5__7_8; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CtaskChoicesRequestU3E5__7_8() { return &___U3CtaskChoicesRequestU3E5__7_8; }
	inline void set_U3CtaskChoicesRequestU3E5__7_8(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CtaskChoicesRequestU3E5__7_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtaskChoicesRequestU3E5__7_8), (void*)value);
	}
};


// AU_Task_APIController/<SendTask>d__23
struct U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A  : public RuntimeObject
{
public:
	// System.Int32 AU_Task_APIController/<SendTask>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AU_Task_APIController/<SendTask>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AU_Task_APIController AU_Task_APIController/<SendTask>d__23::<>4__this
	AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest AU_Task_APIController/<SendTask>d__23::<request>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CrequestU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A, ___U3CU3E4__this_2)); }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A, ___U3CrequestU3E5__2_3)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CrequestU3E5__2_3() const { return ___U3CrequestU3E5__2_3; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CrequestU3E5__2_3() { return &___U3CrequestU3E5__2_3; }
	inline void set_U3CrequestU3E5__2_3(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CrequestU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrequestU3E5__2_3), (void*)value);
	}
};


// TimerController/<UpdateTimer>d__11
struct U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB  : public RuntimeObject
{
public:
	// System.Int32 TimerController/<UpdateTimer>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TimerController/<UpdateTimer>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TimerController TimerController/<UpdateTimer>d__11::<>4__this
	TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB, ___U3CU3E4__this_2)); }
	inline TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>
struct Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t5575902E70199AF34CD1695997CE7E53A1509646 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA, ___list_0)); }
	inline List_1_t5575902E70199AF34CD1695997CE7E53A1509646 * get_list_0() const { return ___list_0; }
	inline List_1_t5575902E70199AF34CD1695997CE7E53A1509646 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t5575902E70199AF34CD1695997CE7E53A1509646 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA, ___current_3)); }
	inline JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * get_current_3() const { return ___current_3; }
	inline JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___list_0)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_list_0() const { return ___list_0; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>>
struct InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	Action_1_t7A6E0E94B21008B707D8BEA0DA19B65A96D58416 * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	Action_1U5BU5D_t960376E04EEB1EB1830312AF536B2B78F2FF40B0* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E, ___firstValue_1)); }
	inline Action_1_t7A6E0E94B21008B707D8BEA0DA19B65A96D58416 * get_firstValue_1() const { return ___firstValue_1; }
	inline Action_1_t7A6E0E94B21008B707D8BEA0DA19B65A96D58416 ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(Action_1_t7A6E0E94B21008B707D8BEA0DA19B65A96D58416 * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E, ___additionalValues_2)); }
	inline Action_1U5BU5D_t960376E04EEB1EB1830312AF536B2B78F2FF40B0* get_additionalValues_2() const { return ___additionalValues_2; }
	inline Action_1U5BU5D_t960376E04EEB1EB1830312AF536B2B78F2FF40B0** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(Action_1U5BU5D_t960376E04EEB1EB1830312AF536B2B78F2FF40B0* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>
struct KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46, ___value_1)); }
	inline JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * get_value_1() const { return ___value_1; }
	inline JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4  : public AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>
struct Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_tAFDD9B13B8A8D35E8017B1AD49EE8DCF947CD740 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC, ___dictionary_0)); }
	inline Dictionary_2_tAFDD9B13B8A8D35E8017B1AD49EE8DCF947CD740 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tAFDD9B13B8A8D35E8017B1AD49EE8DCF947CD740 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tAFDD9B13B8A8D35E8017B1AD49EE8DCF947CD740 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC, ___current_3)); }
	inline KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_completeCallback_1)); }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_completeCallback_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.InputSystem.InputActionType
struct InputActionType_t913CC58784CA34E9791C18B15436C791CD465901 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionType_t913CC58784CA34E9791C18B15436C791CD465901, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Space
struct Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_19)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};


// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputBinding/Flags
struct Flags_t96BD9B15406A59FB60DE4A1F11DF96FB70426BF5 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputBinding/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t96BD9B15406A59FB60DE4A1F11DF96FB70426BF5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SimpleJSON.JSONNode/Enumerator/Type
struct Type_t07F558D771609856890AD0FD5D23575FAB14F4B5 
{
public:
	// System.Int32 SimpleJSON.JSONNode/Enumerator/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t07F558D771609856890AD0FD5D23575FAB14F4B5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.InputSystem.InputBinding
struct InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD 
{
public:
	// System.String UnityEngine.InputSystem.InputBinding::m_Name
	String_t* ___m_Name_2;
	// System.String UnityEngine.InputSystem.InputBinding::m_Id
	String_t* ___m_Id_3;
	// System.String UnityEngine.InputSystem.InputBinding::m_Path
	String_t* ___m_Path_4;
	// System.String UnityEngine.InputSystem.InputBinding::m_Interactions
	String_t* ___m_Interactions_5;
	// System.String UnityEngine.InputSystem.InputBinding::m_Processors
	String_t* ___m_Processors_6;
	// System.String UnityEngine.InputSystem.InputBinding::m_Groups
	String_t* ___m_Groups_7;
	// System.String UnityEngine.InputSystem.InputBinding::m_Action
	String_t* ___m_Action_8;
	// UnityEngine.InputSystem.InputBinding/Flags UnityEngine.InputSystem.InputBinding::m_Flags
	int32_t ___m_Flags_9;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverridePath
	String_t* ___m_OverridePath_10;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideInteractions
	String_t* ___m_OverrideInteractions_11;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideProcessors
	String_t* ___m_OverrideProcessors_12;

public:
	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Name_2)); }
	inline String_t* get_m_Name_2() const { return ___m_Name_2; }
	inline String_t** get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(String_t* value)
	{
		___m_Name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_3() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Id_3)); }
	inline String_t* get_m_Id_3() const { return ___m_Id_3; }
	inline String_t** get_address_of_m_Id_3() { return &___m_Id_3; }
	inline void set_m_Id_3(String_t* value)
	{
		___m_Id_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Path_4() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Path_4)); }
	inline String_t* get_m_Path_4() const { return ___m_Path_4; }
	inline String_t** get_address_of_m_Path_4() { return &___m_Path_4; }
	inline void set_m_Path_4(String_t* value)
	{
		___m_Path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactions_5() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Interactions_5)); }
	inline String_t* get_m_Interactions_5() const { return ___m_Interactions_5; }
	inline String_t** get_address_of_m_Interactions_5() { return &___m_Interactions_5; }
	inline void set_m_Interactions_5(String_t* value)
	{
		___m_Interactions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactions_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Processors_6() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Processors_6)); }
	inline String_t* get_m_Processors_6() const { return ___m_Processors_6; }
	inline String_t** get_address_of_m_Processors_6() { return &___m_Processors_6; }
	inline void set_m_Processors_6(String_t* value)
	{
		___m_Processors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Processors_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Groups_7() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Groups_7)); }
	inline String_t* get_m_Groups_7() const { return ___m_Groups_7; }
	inline String_t** get_address_of_m_Groups_7() { return &___m_Groups_7; }
	inline void set_m_Groups_7(String_t* value)
	{
		___m_Groups_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Groups_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Action_8() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Action_8)); }
	inline String_t* get_m_Action_8() const { return ___m_Action_8; }
	inline String_t** get_address_of_m_Action_8() { return &___m_Action_8; }
	inline void set_m_Action_8(String_t* value)
	{
		___m_Action_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Action_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_9() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Flags_9)); }
	inline int32_t get_m_Flags_9() const { return ___m_Flags_9; }
	inline int32_t* get_address_of_m_Flags_9() { return &___m_Flags_9; }
	inline void set_m_Flags_9(int32_t value)
	{
		___m_Flags_9 = value;
	}

	inline static int32_t get_offset_of_m_OverridePath_10() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_OverridePath_10)); }
	inline String_t* get_m_OverridePath_10() const { return ___m_OverridePath_10; }
	inline String_t** get_address_of_m_OverridePath_10() { return &___m_OverridePath_10; }
	inline void set_m_OverridePath_10(String_t* value)
	{
		___m_OverridePath_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverridePath_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideInteractions_11() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_OverrideInteractions_11)); }
	inline String_t* get_m_OverrideInteractions_11() const { return ___m_OverrideInteractions_11; }
	inline String_t** get_address_of_m_OverrideInteractions_11() { return &___m_OverrideInteractions_11; }
	inline void set_m_OverrideInteractions_11(String_t* value)
	{
		___m_OverrideInteractions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideInteractions_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideProcessors_12() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_OverrideProcessors_12)); }
	inline String_t* get_m_OverrideProcessors_12() const { return ___m_OverrideProcessors_12; }
	inline String_t** get_address_of_m_OverrideProcessors_12() { return &___m_OverrideProcessors_12; }
	inline void set_m_OverrideProcessors_12(String_t* value)
	{
		___m_OverrideProcessors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideProcessors_12), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD_marshaled_pinvoke
{
	char* ___m_Name_2;
	char* ___m_Id_3;
	char* ___m_Path_4;
	char* ___m_Interactions_5;
	char* ___m_Processors_6;
	char* ___m_Groups_7;
	char* ___m_Action_8;
	int32_t ___m_Flags_9;
	char* ___m_OverridePath_10;
	char* ___m_OverrideInteractions_11;
	char* ___m_OverrideProcessors_12;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD_marshaled_com
{
	Il2CppChar* ___m_Name_2;
	Il2CppChar* ___m_Id_3;
	Il2CppChar* ___m_Path_4;
	Il2CppChar* ___m_Interactions_5;
	Il2CppChar* ___m_Processors_6;
	Il2CppChar* ___m_Groups_7;
	Il2CppChar* ___m_Action_8;
	int32_t ___m_Flags_9;
	Il2CppChar* ___m_OverridePath_10;
	Il2CppChar* ___m_OverrideInteractions_11;
	Il2CppChar* ___m_OverrideProcessors_12;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_DownloadHandler_1)); }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DownloadHandler_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_UploadHandler_2)); }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UploadHandler_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CertificateHandler_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_CertificateHandler_3)); }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * get_m_CertificateHandler_3() const { return ___m_CertificateHandler_3; }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E ** get_address_of_m_CertificateHandler_3() { return &___m_CertificateHandler_3; }
	inline void set_m_CertificateHandler_3(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * value)
	{
		___m_CertificateHandler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CertificateHandler_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uri_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Uri_4)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_m_Uri_4() const { return ___m_Uri_4; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_m_Uri_4() { return &___m_Uri_4; }
	inline void set_m_Uri_4(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___m_Uri_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uri_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5)); }
	inline bool get_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() const { return ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return &___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline void set_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(bool value)
	{
		___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com* ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396  : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CwebRequestU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CwebRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396, ___U3CwebRequestU3Ek__BackingField_2)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CwebRequestU3Ek__BackingField_2() const { return ___U3CwebRequestU3Ek__BackingField_2; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CwebRequestU3Ek__BackingField_2() { return &___U3CwebRequestU3Ek__BackingField_2; }
	inline void set_U3CwebRequestU3Ek__BackingField_2(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CwebRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwebRequestU3Ek__BackingField_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396_marshaled_pinvoke : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396_marshaled_com : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// SimpleJSON.JSONNode/Enumerator
struct Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8 
{
public:
	// SimpleJSON.JSONNode/Enumerator/Type SimpleJSON.JSONNode/Enumerator::type
	int32_t ___type_0;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::m_Object
	Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC  ___m_Object_1;
	// System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::m_Array
	Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA  ___m_Array_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_m_Object_1() { return static_cast<int32_t>(offsetof(Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8, ___m_Object_1)); }
	inline Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC  get_m_Object_1() const { return ___m_Object_1; }
	inline Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC * get_address_of_m_Object_1() { return &___m_Object_1; }
	inline void set_m_Object_1(Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC  value)
	{
		___m_Object_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Object_1))->___dictionary_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_Object_1))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_Object_1))->___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Array_2() { return static_cast<int32_t>(offsetof(Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8, ___m_Array_2)); }
	inline Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA  get_m_Array_2() const { return ___m_Array_2; }
	inline Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA * get_address_of_m_Array_2() { return &___m_Array_2; }
	inline void set_m_Array_2(Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA  value)
	{
		___m_Array_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Array_2))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Array_2))->___current_3), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of SimpleJSON.JSONNode/Enumerator
struct Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8_marshaled_pinvoke
{
	int32_t ___type_0;
	Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC  ___m_Object_1;
	Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA  ___m_Array_2;
};
// Native definition for COM marshalling of SimpleJSON.JSONNode/Enumerator
struct Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8_marshaled_com
{
	int32_t ___type_0;
	Enumerator_tCE541D75AA851CBCC2F41CB77FDD2C3DA76C09AC  ___m_Object_1;
	Enumerator_tDA1783FBD1372348634FED4EDC7396A489A211FA  ___m_Array_2;
};

// System.Nullable`1<UnityEngine.InputSystem.InputBinding>
struct Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104 
{
public:
	// T System.Nullable`1::value
	InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104, ___value_0)); }
	inline InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD  get_value_0() const { return ___value_0; }
	inline InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD  value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D  : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB
{
public:
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerBuffer::m_NativeData
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_NativeData_1;

public:
	inline static int32_t get_offset_of_m_NativeData_1() { return static_cast<int32_t>(offsetof(DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D, ___m_NativeData_1)); }
	inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  get_m_NativeData_1() const { return ___m_NativeData_1; }
	inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 * get_address_of_m_NativeData_1() { return &___m_NativeData_1; }
	inline void set_m_NativeData_1(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  value)
	{
		___m_NativeData_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_marshaled_pinvoke : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_marshaled_com : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_NativeData_1;
};

// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669  : public UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA
{
public:
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.UploadHandlerRaw::m_Payload
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_Payload_1;

public:
	inline static int32_t get_offset_of_m_Payload_1() { return static_cast<int32_t>(offsetof(UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669, ___m_Payload_1)); }
	inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  get_m_Payload_1() const { return ___m_Payload_1; }
	inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 * get_address_of_m_Payload_1() { return &___m_Payload_1; }
	inline void set_m_Payload_1(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  value)
	{
		___m_Payload_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669_marshaled_pinvoke : public UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke
{
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_Payload_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669_marshaled_com : public UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com
{
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_Payload_1;
};

// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.InputSystem.InputAction
struct InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48  : public RuntimeObject
{
public:
	// System.String UnityEngine.InputSystem.InputAction::m_Name
	String_t* ___m_Name_0;
	// UnityEngine.InputSystem.InputActionType UnityEngine.InputSystem.InputAction::m_Type
	int32_t ___m_Type_1;
	// System.String UnityEngine.InputSystem.InputAction::m_ExpectedControlType
	String_t* ___m_ExpectedControlType_2;
	// System.String UnityEngine.InputSystem.InputAction::m_Id
	String_t* ___m_Id_3;
	// System.String UnityEngine.InputSystem.InputAction::m_Processors
	String_t* ___m_Processors_4;
	// System.String UnityEngine.InputSystem.InputAction::m_Interactions
	String_t* ___m_Interactions_5;
	// UnityEngine.InputSystem.InputBinding[] UnityEngine.InputSystem.InputAction::m_SingletonActionBindings
	InputBindingU5BU5D_t3CCBD5F5A51F370BFD0A373A770C8720F2A6115E* ___m_SingletonActionBindings_6;
	// System.Nullable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputAction::m_BindingMask
	Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104  ___m_BindingMask_7;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_BindingsStartIndex
	int32_t ___m_BindingsStartIndex_8;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_BindingsCount
	int32_t ___m_BindingsCount_9;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_ControlStartIndex
	int32_t ___m_ControlStartIndex_10;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_ControlCount
	int32_t ___m_ControlCount_11;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_ActionIndexInState
	int32_t ___m_ActionIndexInState_12;
	// UnityEngine.InputSystem.InputActionMap UnityEngine.InputSystem.InputAction::m_ActionMap
	InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * ___m_ActionMap_13;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputAction::m_OnStarted
	InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  ___m_OnStarted_14;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputAction::m_OnCanceled
	InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  ___m_OnCanceled_15;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputAction::m_OnPerformed
	InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  ___m_OnPerformed_16;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_ExpectedControlType_2() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_ExpectedControlType_2)); }
	inline String_t* get_m_ExpectedControlType_2() const { return ___m_ExpectedControlType_2; }
	inline String_t** get_address_of_m_ExpectedControlType_2() { return &___m_ExpectedControlType_2; }
	inline void set_m_ExpectedControlType_2(String_t* value)
	{
		___m_ExpectedControlType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExpectedControlType_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_3() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_Id_3)); }
	inline String_t* get_m_Id_3() const { return ___m_Id_3; }
	inline String_t** get_address_of_m_Id_3() { return &___m_Id_3; }
	inline void set_m_Id_3(String_t* value)
	{
		___m_Id_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Processors_4() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_Processors_4)); }
	inline String_t* get_m_Processors_4() const { return ___m_Processors_4; }
	inline String_t** get_address_of_m_Processors_4() { return &___m_Processors_4; }
	inline void set_m_Processors_4(String_t* value)
	{
		___m_Processors_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Processors_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactions_5() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_Interactions_5)); }
	inline String_t* get_m_Interactions_5() const { return ___m_Interactions_5; }
	inline String_t** get_address_of_m_Interactions_5() { return &___m_Interactions_5; }
	inline void set_m_Interactions_5(String_t* value)
	{
		___m_Interactions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactions_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_SingletonActionBindings_6() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_SingletonActionBindings_6)); }
	inline InputBindingU5BU5D_t3CCBD5F5A51F370BFD0A373A770C8720F2A6115E* get_m_SingletonActionBindings_6() const { return ___m_SingletonActionBindings_6; }
	inline InputBindingU5BU5D_t3CCBD5F5A51F370BFD0A373A770C8720F2A6115E** get_address_of_m_SingletonActionBindings_6() { return &___m_SingletonActionBindings_6; }
	inline void set_m_SingletonActionBindings_6(InputBindingU5BU5D_t3CCBD5F5A51F370BFD0A373A770C8720F2A6115E* value)
	{
		___m_SingletonActionBindings_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SingletonActionBindings_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingMask_7() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_BindingMask_7)); }
	inline Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104  get_m_BindingMask_7() const { return ___m_BindingMask_7; }
	inline Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104 * get_address_of_m_BindingMask_7() { return &___m_BindingMask_7; }
	inline void set_m_BindingMask_7(Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104  value)
	{
		___m_BindingMask_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_7))->___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_BindingsStartIndex_8() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_BindingsStartIndex_8)); }
	inline int32_t get_m_BindingsStartIndex_8() const { return ___m_BindingsStartIndex_8; }
	inline int32_t* get_address_of_m_BindingsStartIndex_8() { return &___m_BindingsStartIndex_8; }
	inline void set_m_BindingsStartIndex_8(int32_t value)
	{
		___m_BindingsStartIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_BindingsCount_9() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_BindingsCount_9)); }
	inline int32_t get_m_BindingsCount_9() const { return ___m_BindingsCount_9; }
	inline int32_t* get_address_of_m_BindingsCount_9() { return &___m_BindingsCount_9; }
	inline void set_m_BindingsCount_9(int32_t value)
	{
		___m_BindingsCount_9 = value;
	}

	inline static int32_t get_offset_of_m_ControlStartIndex_10() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_ControlStartIndex_10)); }
	inline int32_t get_m_ControlStartIndex_10() const { return ___m_ControlStartIndex_10; }
	inline int32_t* get_address_of_m_ControlStartIndex_10() { return &___m_ControlStartIndex_10; }
	inline void set_m_ControlStartIndex_10(int32_t value)
	{
		___m_ControlStartIndex_10 = value;
	}

	inline static int32_t get_offset_of_m_ControlCount_11() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_ControlCount_11)); }
	inline int32_t get_m_ControlCount_11() const { return ___m_ControlCount_11; }
	inline int32_t* get_address_of_m_ControlCount_11() { return &___m_ControlCount_11; }
	inline void set_m_ControlCount_11(int32_t value)
	{
		___m_ControlCount_11 = value;
	}

	inline static int32_t get_offset_of_m_ActionIndexInState_12() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_ActionIndexInState_12)); }
	inline int32_t get_m_ActionIndexInState_12() const { return ___m_ActionIndexInState_12; }
	inline int32_t* get_address_of_m_ActionIndexInState_12() { return &___m_ActionIndexInState_12; }
	inline void set_m_ActionIndexInState_12(int32_t value)
	{
		___m_ActionIndexInState_12 = value;
	}

	inline static int32_t get_offset_of_m_ActionMap_13() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_ActionMap_13)); }
	inline InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * get_m_ActionMap_13() const { return ___m_ActionMap_13; }
	inline InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 ** get_address_of_m_ActionMap_13() { return &___m_ActionMap_13; }
	inline void set_m_ActionMap_13(InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * value)
	{
		___m_ActionMap_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionMap_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnStarted_14() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_OnStarted_14)); }
	inline InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  get_m_OnStarted_14() const { return ___m_OnStarted_14; }
	inline InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E * get_address_of_m_OnStarted_14() { return &___m_OnStarted_14; }
	inline void set_m_OnStarted_14(InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  value)
	{
		___m_OnStarted_14 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnStarted_14))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnStarted_14))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_OnCanceled_15() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_OnCanceled_15)); }
	inline InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  get_m_OnCanceled_15() const { return ___m_OnCanceled_15; }
	inline InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E * get_address_of_m_OnCanceled_15() { return &___m_OnCanceled_15; }
	inline void set_m_OnCanceled_15(InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  value)
	{
		___m_OnCanceled_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnCanceled_15))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnCanceled_15))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_OnPerformed_16() { return static_cast<int32_t>(offsetof(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48, ___m_OnPerformed_16)); }
	inline InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  get_m_OnPerformed_16() const { return ___m_OnPerformed_16; }
	inline InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E * get_address_of_m_OnPerformed_16() { return &___m_OnPerformed_16; }
	inline void set_m_OnPerformed_16(InlinedArray_1_tD8F648109EC431FFA4D6E31DC56D88C27692A37E  value)
	{
		___m_OnPerformed_16 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnPerformed_16))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnPerformed_16))->___additionalValues_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// AU_BotController
struct AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Rigidbody AU_BotController::myRB
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___myRB_4;
	// UnityEngine.Transform AU_BotController::myAvatar
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___myAvatar_5;
	// UnityEngine.Animator AU_BotController::myAnim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___myAnim_6;
	// System.Boolean AU_BotController::isImposter
	bool ___isImposter_7;
	// System.String AU_BotController::dialog
	String_t* ___dialog_8;
	// System.Single AU_BotController::accelerationTime
	float ___accelerationTime_9;
	// System.Single AU_BotController::maxSpeed
	float ___maxSpeed_10;
	// UnityEngine.Vector2 AU_BotController::movement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___movement_11;
	// System.Single AU_BotController::timeLeft
	float ___timeLeft_12;
	// System.Boolean AU_BotController::playerIsAnsweringTask
	bool ___playerIsAnsweringTask_13;

public:
	inline static int32_t get_offset_of_myRB_4() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___myRB_4)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_myRB_4() const { return ___myRB_4; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_myRB_4() { return &___myRB_4; }
	inline void set_myRB_4(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___myRB_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRB_4), (void*)value);
	}

	inline static int32_t get_offset_of_myAvatar_5() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___myAvatar_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_myAvatar_5() const { return ___myAvatar_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_myAvatar_5() { return &___myAvatar_5; }
	inline void set_myAvatar_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___myAvatar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myAvatar_5), (void*)value);
	}

	inline static int32_t get_offset_of_myAnim_6() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___myAnim_6)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_myAnim_6() const { return ___myAnim_6; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_myAnim_6() { return &___myAnim_6; }
	inline void set_myAnim_6(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___myAnim_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myAnim_6), (void*)value);
	}

	inline static int32_t get_offset_of_isImposter_7() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___isImposter_7)); }
	inline bool get_isImposter_7() const { return ___isImposter_7; }
	inline bool* get_address_of_isImposter_7() { return &___isImposter_7; }
	inline void set_isImposter_7(bool value)
	{
		___isImposter_7 = value;
	}

	inline static int32_t get_offset_of_dialog_8() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___dialog_8)); }
	inline String_t* get_dialog_8() const { return ___dialog_8; }
	inline String_t** get_address_of_dialog_8() { return &___dialog_8; }
	inline void set_dialog_8(String_t* value)
	{
		___dialog_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialog_8), (void*)value);
	}

	inline static int32_t get_offset_of_accelerationTime_9() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___accelerationTime_9)); }
	inline float get_accelerationTime_9() const { return ___accelerationTime_9; }
	inline float* get_address_of_accelerationTime_9() { return &___accelerationTime_9; }
	inline void set_accelerationTime_9(float value)
	{
		___accelerationTime_9 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_10() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___maxSpeed_10)); }
	inline float get_maxSpeed_10() const { return ___maxSpeed_10; }
	inline float* get_address_of_maxSpeed_10() { return &___maxSpeed_10; }
	inline void set_maxSpeed_10(float value)
	{
		___maxSpeed_10 = value;
	}

	inline static int32_t get_offset_of_movement_11() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___movement_11)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_movement_11() const { return ___movement_11; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_movement_11() { return &___movement_11; }
	inline void set_movement_11(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___movement_11 = value;
	}

	inline static int32_t get_offset_of_timeLeft_12() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___timeLeft_12)); }
	inline float get_timeLeft_12() const { return ___timeLeft_12; }
	inline float* get_address_of_timeLeft_12() { return &___timeLeft_12; }
	inline void set_timeLeft_12(float value)
	{
		___timeLeft_12 = value;
	}

	inline static int32_t get_offset_of_playerIsAnsweringTask_13() { return static_cast<int32_t>(offsetof(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308, ___playerIsAnsweringTask_13)); }
	inline bool get_playerIsAnsweringTask_13() const { return ___playerIsAnsweringTask_13; }
	inline bool* get_address_of_playerIsAnsweringTask_13() { return &___playerIsAnsweringTask_13; }
	inline void set_playerIsAnsweringTask_13(bool value)
	{
		___playerIsAnsweringTask_13 = value;
	}
};


// AU_CompassController
struct AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AU_CompassController::compass
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___compass_4;
	// UnityEngine.GameObject AU_CompassController::player
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___player_5;
	// UnityEngine.GameObject AU_CompassController::target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___target_6;
	// System.Single AU_CompassController::speed
	float ___speed_7;

public:
	inline static int32_t get_offset_of_compass_4() { return static_cast<int32_t>(offsetof(AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4, ___compass_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_compass_4() const { return ___compass_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_compass_4() { return &___compass_4; }
	inline void set_compass_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___compass_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compass_4), (void*)value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4, ___player_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_player_5() const { return ___player_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_5), (void*)value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4, ___target_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_target_6() const { return ___target_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_6), (void*)value);
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}
};


// AU_Events_ManagerController
struct AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Events.UnityEvent AU_Events_ManagerController::TaskCompleted
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___TaskCompleted_4;
	// UnityEngine.Events.UnityEvent AU_Events_ManagerController::PlayerKilled
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___PlayerKilled_5;
	// UnityEngine.Events.UnityEvent AU_Events_ManagerController::StopBots
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___StopBots_6;
	// UnityEngine.Events.UnityEvent AU_Events_ManagerController::StartBots
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___StartBots_7;
	// UnityEngine.Events.UnityEvent AU_Events_ManagerController::WrongChoiceSelected
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___WrongChoiceSelected_8;

public:
	inline static int32_t get_offset_of_TaskCompleted_4() { return static_cast<int32_t>(offsetof(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4, ___TaskCompleted_4)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_TaskCompleted_4() const { return ___TaskCompleted_4; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_TaskCompleted_4() { return &___TaskCompleted_4; }
	inline void set_TaskCompleted_4(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___TaskCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskCompleted_4), (void*)value);
	}

	inline static int32_t get_offset_of_PlayerKilled_5() { return static_cast<int32_t>(offsetof(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4, ___PlayerKilled_5)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_PlayerKilled_5() const { return ___PlayerKilled_5; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_PlayerKilled_5() { return &___PlayerKilled_5; }
	inline void set_PlayerKilled_5(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___PlayerKilled_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerKilled_5), (void*)value);
	}

	inline static int32_t get_offset_of_StopBots_6() { return static_cast<int32_t>(offsetof(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4, ___StopBots_6)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_StopBots_6() const { return ___StopBots_6; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_StopBots_6() { return &___StopBots_6; }
	inline void set_StopBots_6(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___StopBots_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StopBots_6), (void*)value);
	}

	inline static int32_t get_offset_of_StartBots_7() { return static_cast<int32_t>(offsetof(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4, ___StartBots_7)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_StartBots_7() const { return ___StartBots_7; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_StartBots_7() { return &___StartBots_7; }
	inline void set_StartBots_7(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___StartBots_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartBots_7), (void*)value);
	}

	inline static int32_t get_offset_of_WrongChoiceSelected_8() { return static_cast<int32_t>(offsetof(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4, ___WrongChoiceSelected_8)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_WrongChoiceSelected_8() const { return ___WrongChoiceSelected_8; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_WrongChoiceSelected_8() { return &___WrongChoiceSelected_8; }
	inline void set_WrongChoiceSelected_8(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___WrongChoiceSelected_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WrongChoiceSelected_8), (void*)value);
	}
};


// AU_Game_ManagerController
struct AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button AU_Game_ManagerController::startButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___startButton_4;
	// UnityEngine.GameObject AU_Game_ManagerController::startPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___startPanel_5;
	// UnityEngine.GameObject AU_Game_ManagerController::exitGamePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___exitGamePanel_6;
	// UnityEngine.GameObject AU_Game_ManagerController::gameOverPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameOverPanel_7;
	// UnityEngine.UI.Text AU_Game_ManagerController::gameOverPointsTextPanel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___gameOverPointsTextPanel_8;
	// AU_Task_APIController AU_Game_ManagerController::TasksManagerController
	AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * ___TasksManagerController_9;

public:
	inline static int32_t get_offset_of_startButton_4() { return static_cast<int32_t>(offsetof(AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148, ___startButton_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_startButton_4() const { return ___startButton_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_startButton_4() { return &___startButton_4; }
	inline void set_startButton_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___startButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startButton_4), (void*)value);
	}

	inline static int32_t get_offset_of_startPanel_5() { return static_cast<int32_t>(offsetof(AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148, ___startPanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_startPanel_5() const { return ___startPanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_startPanel_5() { return &___startPanel_5; }
	inline void set_startPanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___startPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_exitGamePanel_6() { return static_cast<int32_t>(offsetof(AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148, ___exitGamePanel_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_exitGamePanel_6() const { return ___exitGamePanel_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_exitGamePanel_6() { return &___exitGamePanel_6; }
	inline void set_exitGamePanel_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___exitGamePanel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exitGamePanel_6), (void*)value);
	}

	inline static int32_t get_offset_of_gameOverPanel_7() { return static_cast<int32_t>(offsetof(AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148, ___gameOverPanel_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gameOverPanel_7() const { return ___gameOverPanel_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gameOverPanel_7() { return &___gameOverPanel_7; }
	inline void set_gameOverPanel_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gameOverPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameOverPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_gameOverPointsTextPanel_8() { return static_cast<int32_t>(offsetof(AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148, ___gameOverPointsTextPanel_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_gameOverPointsTextPanel_8() const { return ___gameOverPointsTextPanel_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_gameOverPointsTextPanel_8() { return &___gameOverPointsTextPanel_8; }
	inline void set_gameOverPointsTextPanel_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___gameOverPointsTextPanel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameOverPointsTextPanel_8), (void*)value);
	}

	inline static int32_t get_offset_of_TasksManagerController_9() { return static_cast<int32_t>(offsetof(AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148, ___TasksManagerController_9)); }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * get_TasksManagerController_9() const { return ___TasksManagerController_9; }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C ** get_address_of_TasksManagerController_9() { return &___TasksManagerController_9; }
	inline void set_TasksManagerController_9(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * value)
	{
		___TasksManagerController_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TasksManagerController_9), (void*)value);
	}
};


// AU_ImposterController
struct AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// AU_Events_ManagerController AU_ImposterController::eventsManagerController
	AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * ___eventsManagerController_4;
	// UnityEngine.Rigidbody AU_ImposterController::myRB
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___myRB_5;
	// UnityEngine.Transform AU_ImposterController::myAvatar
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___myAvatar_6;
	// UnityEngine.Animator AU_ImposterController::myAnim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___myAnim_7;
	// System.Single AU_ImposterController::accelerationTime
	float ___accelerationTime_8;
	// System.Single AU_ImposterController::maxSpeed
	float ___maxSpeed_9;
	// UnityEngine.Vector2 AU_ImposterController::movement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___movement_10;
	// System.Single AU_ImposterController::timeLeft
	float ___timeLeft_11;
	// System.Boolean AU_ImposterController::playerIsDead
	bool ___playerIsDead_12;
	// UnityEngine.Quaternion AU_ImposterController::originalRotationValue
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___originalRotationValue_13;
	// UnityEngine.Transform AU_ImposterController::_player
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____player_14;
	// System.Boolean AU_ImposterController::followingPlayer
	bool ___followingPlayer_15;
	// UnityEngine.Transform AU_ImposterController::_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____transform_16;
	// System.Single AU_ImposterController::_moveSpeed
	float ____moveSpeed_17;
	// System.Boolean AU_ImposterController::playerIsAnsweringTask
	bool ___playerIsAnsweringTask_18;

public:
	inline static int32_t get_offset_of_eventsManagerController_4() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___eventsManagerController_4)); }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * get_eventsManagerController_4() const { return ___eventsManagerController_4; }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 ** get_address_of_eventsManagerController_4() { return &___eventsManagerController_4; }
	inline void set_eventsManagerController_4(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * value)
	{
		___eventsManagerController_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventsManagerController_4), (void*)value);
	}

	inline static int32_t get_offset_of_myRB_5() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___myRB_5)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_myRB_5() const { return ___myRB_5; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_myRB_5() { return &___myRB_5; }
	inline void set_myRB_5(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___myRB_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRB_5), (void*)value);
	}

	inline static int32_t get_offset_of_myAvatar_6() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___myAvatar_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_myAvatar_6() const { return ___myAvatar_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_myAvatar_6() { return &___myAvatar_6; }
	inline void set_myAvatar_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___myAvatar_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myAvatar_6), (void*)value);
	}

	inline static int32_t get_offset_of_myAnim_7() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___myAnim_7)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_myAnim_7() const { return ___myAnim_7; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_myAnim_7() { return &___myAnim_7; }
	inline void set_myAnim_7(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___myAnim_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myAnim_7), (void*)value);
	}

	inline static int32_t get_offset_of_accelerationTime_8() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___accelerationTime_8)); }
	inline float get_accelerationTime_8() const { return ___accelerationTime_8; }
	inline float* get_address_of_accelerationTime_8() { return &___accelerationTime_8; }
	inline void set_accelerationTime_8(float value)
	{
		___accelerationTime_8 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_9() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___maxSpeed_9)); }
	inline float get_maxSpeed_9() const { return ___maxSpeed_9; }
	inline float* get_address_of_maxSpeed_9() { return &___maxSpeed_9; }
	inline void set_maxSpeed_9(float value)
	{
		___maxSpeed_9 = value;
	}

	inline static int32_t get_offset_of_movement_10() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___movement_10)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_movement_10() const { return ___movement_10; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_movement_10() { return &___movement_10; }
	inline void set_movement_10(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___movement_10 = value;
	}

	inline static int32_t get_offset_of_timeLeft_11() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___timeLeft_11)); }
	inline float get_timeLeft_11() const { return ___timeLeft_11; }
	inline float* get_address_of_timeLeft_11() { return &___timeLeft_11; }
	inline void set_timeLeft_11(float value)
	{
		___timeLeft_11 = value;
	}

	inline static int32_t get_offset_of_playerIsDead_12() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___playerIsDead_12)); }
	inline bool get_playerIsDead_12() const { return ___playerIsDead_12; }
	inline bool* get_address_of_playerIsDead_12() { return &___playerIsDead_12; }
	inline void set_playerIsDead_12(bool value)
	{
		___playerIsDead_12 = value;
	}

	inline static int32_t get_offset_of_originalRotationValue_13() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___originalRotationValue_13)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_originalRotationValue_13() const { return ___originalRotationValue_13; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_originalRotationValue_13() { return &___originalRotationValue_13; }
	inline void set_originalRotationValue_13(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___originalRotationValue_13 = value;
	}

	inline static int32_t get_offset_of__player_14() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ____player_14)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__player_14() const { return ____player_14; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__player_14() { return &____player_14; }
	inline void set__player_14(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____player_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____player_14), (void*)value);
	}

	inline static int32_t get_offset_of_followingPlayer_15() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___followingPlayer_15)); }
	inline bool get_followingPlayer_15() const { return ___followingPlayer_15; }
	inline bool* get_address_of_followingPlayer_15() { return &___followingPlayer_15; }
	inline void set_followingPlayer_15(bool value)
	{
		___followingPlayer_15 = value;
	}

	inline static int32_t get_offset_of__transform_16() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ____transform_16)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__transform_16() const { return ____transform_16; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__transform_16() { return &____transform_16; }
	inline void set__transform_16(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____transform_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transform_16), (void*)value);
	}

	inline static int32_t get_offset_of__moveSpeed_17() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ____moveSpeed_17)); }
	inline float get__moveSpeed_17() const { return ____moveSpeed_17; }
	inline float* get_address_of__moveSpeed_17() { return &____moveSpeed_17; }
	inline void set__moveSpeed_17(float value)
	{
		____moveSpeed_17 = value;
	}

	inline static int32_t get_offset_of_playerIsAnsweringTask_18() { return static_cast<int32_t>(offsetof(AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8, ___playerIsAnsweringTask_18)); }
	inline bool get_playerIsAnsweringTask_18() const { return ___playerIsAnsweringTask_18; }
	inline bool* get_address_of_playerIsAnsweringTask_18() { return &___playerIsAnsweringTask_18; }
	inline void set_playerIsAnsweringTask_18(bool value)
	{
		___playerIsAnsweringTask_18 = value;
	}
};


// AU_InteractableController
struct AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AU_InteractableController::taskPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___taskPanel_4;
	// UnityEngine.GameObject AU_InteractableController::highlight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___highlight_5;
	// AU_Events_ManagerController AU_InteractableController::eventsController
	AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * ___eventsController_6;
	// System.Boolean AU_InteractableController::interactableIsActive
	bool ___interactableIsActive_7;
	// UnityEngine.GameObject AU_InteractableController::activeMiniGame
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___activeMiniGame_8;

public:
	inline static int32_t get_offset_of_taskPanel_4() { return static_cast<int32_t>(offsetof(AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9, ___taskPanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_taskPanel_4() const { return ___taskPanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_taskPanel_4() { return &___taskPanel_4; }
	inline void set_taskPanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___taskPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___taskPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_highlight_5() { return static_cast<int32_t>(offsetof(AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9, ___highlight_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_highlight_5() const { return ___highlight_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_highlight_5() { return &___highlight_5; }
	inline void set_highlight_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___highlight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___highlight_5), (void*)value);
	}

	inline static int32_t get_offset_of_eventsController_6() { return static_cast<int32_t>(offsetof(AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9, ___eventsController_6)); }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * get_eventsController_6() const { return ___eventsController_6; }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 ** get_address_of_eventsController_6() { return &___eventsController_6; }
	inline void set_eventsController_6(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * value)
	{
		___eventsController_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventsController_6), (void*)value);
	}

	inline static int32_t get_offset_of_interactableIsActive_7() { return static_cast<int32_t>(offsetof(AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9, ___interactableIsActive_7)); }
	inline bool get_interactableIsActive_7() const { return ___interactableIsActive_7; }
	inline bool* get_address_of_interactableIsActive_7() { return &___interactableIsActive_7; }
	inline void set_interactableIsActive_7(bool value)
	{
		___interactableIsActive_7 = value;
	}

	inline static int32_t get_offset_of_activeMiniGame_8() { return static_cast<int32_t>(offsetof(AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9, ___activeMiniGame_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_activeMiniGame_8() const { return ___activeMiniGame_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_activeMiniGame_8() { return &___activeMiniGame_8; }
	inline void set_activeMiniGame_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___activeMiniGame_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeMiniGame_8), (void*)value);
	}
};


// AU_Multiple_Choice_TaskController
struct AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AU_Multiple_Choice_TaskController::GamePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___GamePanel_4;
	// UnityEngine.UI.Text AU_Multiple_Choice_TaskController::QuestionText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___QuestionText_5;
	// UnityEngine.UI.Text AU_Multiple_Choice_TaskController::BodyText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___BodyText_6;
	// UnityEngine.UI.Button[] AU_Multiple_Choice_TaskController::Buttons
	ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* ___Buttons_7;
	// TimerController AU_Multiple_Choice_TaskController::timerController
	TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * ___timerController_8;
	// UnityEngine.GameObject AU_Multiple_Choice_TaskController::timeObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___timeObject_9;
	// AU_Events_ManagerController AU_Multiple_Choice_TaskController::eventsManagerController
	AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * ___eventsManagerController_10;

public:
	inline static int32_t get_offset_of_GamePanel_4() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___GamePanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_GamePanel_4() const { return ___GamePanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_GamePanel_4() { return &___GamePanel_4; }
	inline void set_GamePanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___GamePanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GamePanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_QuestionText_5() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___QuestionText_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_QuestionText_5() const { return ___QuestionText_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_QuestionText_5() { return &___QuestionText_5; }
	inline void set_QuestionText_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___QuestionText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___QuestionText_5), (void*)value);
	}

	inline static int32_t get_offset_of_BodyText_6() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___BodyText_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_BodyText_6() const { return ___BodyText_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_BodyText_6() { return &___BodyText_6; }
	inline void set_BodyText_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___BodyText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BodyText_6), (void*)value);
	}

	inline static int32_t get_offset_of_Buttons_7() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___Buttons_7)); }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* get_Buttons_7() const { return ___Buttons_7; }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B** get_address_of_Buttons_7() { return &___Buttons_7; }
	inline void set_Buttons_7(ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* value)
	{
		___Buttons_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Buttons_7), (void*)value);
	}

	inline static int32_t get_offset_of_timerController_8() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___timerController_8)); }
	inline TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * get_timerController_8() const { return ___timerController_8; }
	inline TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA ** get_address_of_timerController_8() { return &___timerController_8; }
	inline void set_timerController_8(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * value)
	{
		___timerController_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timerController_8), (void*)value);
	}

	inline static int32_t get_offset_of_timeObject_9() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___timeObject_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_timeObject_9() const { return ___timeObject_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_timeObject_9() { return &___timeObject_9; }
	inline void set_timeObject_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___timeObject_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timeObject_9), (void*)value);
	}

	inline static int32_t get_offset_of_eventsManagerController_10() { return static_cast<int32_t>(offsetof(AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430, ___eventsManagerController_10)); }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * get_eventsManagerController_10() const { return ___eventsManagerController_10; }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 ** get_address_of_eventsManagerController_10() { return &___eventsManagerController_10; }
	inline void set_eventsManagerController_10(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * value)
	{
		___eventsManagerController_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventsManagerController_10), (void*)value);
	}
};


// AU_PlayerController
struct AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Rigidbody AU_PlayerController::myRB
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___myRB_4;
	// UnityEngine.Transform AU_PlayerController::myAvatar
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___myAvatar_5;
	// UnityEngine.Animator AU_PlayerController::myAnim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___myAnim_6;
	// UnityEngine.InputSystem.InputAction AU_PlayerController::WASD
	InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * ___WASD_7;
	// UnityEngine.Vector2 AU_PlayerController::movementInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___movementInput_8;
	// System.Single AU_PlayerController::movementSpeed
	float ___movementSpeed_9;
	// AU_BotController AU_PlayerController::target
	AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * ___target_10;
	// UnityEngine.Collider AU_PlayerController::myCollider
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___myCollider_11;
	// System.Boolean AU_PlayerController::isDead
	bool ___isDead_12;

public:
	inline static int32_t get_offset_of_myRB_4() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___myRB_4)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_myRB_4() const { return ___myRB_4; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_myRB_4() { return &___myRB_4; }
	inline void set_myRB_4(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___myRB_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRB_4), (void*)value);
	}

	inline static int32_t get_offset_of_myAvatar_5() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___myAvatar_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_myAvatar_5() const { return ___myAvatar_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_myAvatar_5() { return &___myAvatar_5; }
	inline void set_myAvatar_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___myAvatar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myAvatar_5), (void*)value);
	}

	inline static int32_t get_offset_of_myAnim_6() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___myAnim_6)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_myAnim_6() const { return ___myAnim_6; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_myAnim_6() { return &___myAnim_6; }
	inline void set_myAnim_6(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___myAnim_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myAnim_6), (void*)value);
	}

	inline static int32_t get_offset_of_WASD_7() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___WASD_7)); }
	inline InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * get_WASD_7() const { return ___WASD_7; }
	inline InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 ** get_address_of_WASD_7() { return &___WASD_7; }
	inline void set_WASD_7(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * value)
	{
		___WASD_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WASD_7), (void*)value);
	}

	inline static int32_t get_offset_of_movementInput_8() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___movementInput_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_movementInput_8() const { return ___movementInput_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_movementInput_8() { return &___movementInput_8; }
	inline void set_movementInput_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___movementInput_8 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_9() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___movementSpeed_9)); }
	inline float get_movementSpeed_9() const { return ___movementSpeed_9; }
	inline float* get_address_of_movementSpeed_9() { return &___movementSpeed_9; }
	inline void set_movementSpeed_9(float value)
	{
		___movementSpeed_9 = value;
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___target_10)); }
	inline AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * get_target_10() const { return ___target_10; }
	inline AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_10), (void*)value);
	}

	inline static int32_t get_offset_of_myCollider_11() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___myCollider_11)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_myCollider_11() const { return ___myCollider_11; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_myCollider_11() { return &___myCollider_11; }
	inline void set_myCollider_11(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___myCollider_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myCollider_11), (void*)value);
	}

	inline static int32_t get_offset_of_isDead_12() { return static_cast<int32_t>(offsetof(AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6, ___isDead_12)); }
	inline bool get_isDead_12() const { return ___isDead_12; }
	inline bool* get_address_of_isDead_12() { return &___isDead_12; }
	inline void set_isDead_12(bool value)
	{
		___isDead_12 = value;
	}
};


// AU_ReportController
struct AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// AU_Task_APIController AU_ReportController::taskAPIController
	AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * ___taskAPIController_4;
	// AU_Events_ManagerController AU_ReportController::eventsController
	AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * ___eventsController_5;
	// UnityEngine.GameObject AU_ReportController::prefabButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefabButton_6;
	// UnityEngine.RectTransform AU_ReportController::ParentPanel
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___ParentPanel_7;
	// UnityEngine.UI.Text AU_ReportController::descriptionText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___descriptionText_8;
	// UnityEngine.UI.Text AU_ReportController::questionText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___questionText_9;
	// UnityEngine.GameObject AU_ReportController::GamePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___GamePanel_10;
	// UnityEngine.UI.Image AU_ReportController::crewmate
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___crewmate_11;
	// System.Collections.Generic.List`1<System.String> AU_ReportController::colors
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___colors_12;
	// System.Int32 AU_ReportController::selectedCrewmate
	int32_t ___selectedCrewmate_13;
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> AU_ReportController::buttons
	List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * ___buttons_14;
	// AU_Events_ManagerController AU_ReportController::eventsManagerController
	AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * ___eventsManagerController_15;
	// System.Int32 AU_ReportController::numRightOption
	int32_t ___numRightOption_16;
	// System.Collections.Generic.List`1<System.String> AU_ReportController::citationsText
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___citationsText_17;
	// System.Int32 AU_ReportController::selectedOption
	int32_t ___selectedOption_18;
	// System.Int32 AU_ReportController::numAttempts
	int32_t ___numAttempts_19;
	// System.Boolean AU_ReportController::correctChoiceSelected
	bool ___correctChoiceSelected_20;
	// System.Collections.Generic.List`1<AU_ReportController/Imposter> AU_ReportController::availableCrewmates
	List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * ___availableCrewmates_21;

public:
	inline static int32_t get_offset_of_taskAPIController_4() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___taskAPIController_4)); }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * get_taskAPIController_4() const { return ___taskAPIController_4; }
	inline AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C ** get_address_of_taskAPIController_4() { return &___taskAPIController_4; }
	inline void set_taskAPIController_4(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * value)
	{
		___taskAPIController_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___taskAPIController_4), (void*)value);
	}

	inline static int32_t get_offset_of_eventsController_5() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___eventsController_5)); }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * get_eventsController_5() const { return ___eventsController_5; }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 ** get_address_of_eventsController_5() { return &___eventsController_5; }
	inline void set_eventsController_5(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * value)
	{
		___eventsController_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventsController_5), (void*)value);
	}

	inline static int32_t get_offset_of_prefabButton_6() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___prefabButton_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_prefabButton_6() const { return ___prefabButton_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_prefabButton_6() { return &___prefabButton_6; }
	inline void set_prefabButton_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___prefabButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_ParentPanel_7() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___ParentPanel_7)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_ParentPanel_7() const { return ___ParentPanel_7; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_ParentPanel_7() { return &___ParentPanel_7; }
	inline void set_ParentPanel_7(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___ParentPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ParentPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_descriptionText_8() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___descriptionText_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_descriptionText_8() const { return ___descriptionText_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_descriptionText_8() { return &___descriptionText_8; }
	inline void set_descriptionText_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___descriptionText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___descriptionText_8), (void*)value);
	}

	inline static int32_t get_offset_of_questionText_9() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___questionText_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_questionText_9() const { return ___questionText_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_questionText_9() { return &___questionText_9; }
	inline void set_questionText_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___questionText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___questionText_9), (void*)value);
	}

	inline static int32_t get_offset_of_GamePanel_10() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___GamePanel_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_GamePanel_10() const { return ___GamePanel_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_GamePanel_10() { return &___GamePanel_10; }
	inline void set_GamePanel_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___GamePanel_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GamePanel_10), (void*)value);
	}

	inline static int32_t get_offset_of_crewmate_11() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___crewmate_11)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_crewmate_11() const { return ___crewmate_11; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_crewmate_11() { return &___crewmate_11; }
	inline void set_crewmate_11(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___crewmate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crewmate_11), (void*)value);
	}

	inline static int32_t get_offset_of_colors_12() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___colors_12)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_colors_12() const { return ___colors_12; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_colors_12() { return &___colors_12; }
	inline void set_colors_12(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___colors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_12), (void*)value);
	}

	inline static int32_t get_offset_of_selectedCrewmate_13() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___selectedCrewmate_13)); }
	inline int32_t get_selectedCrewmate_13() const { return ___selectedCrewmate_13; }
	inline int32_t* get_address_of_selectedCrewmate_13() { return &___selectedCrewmate_13; }
	inline void set_selectedCrewmate_13(int32_t value)
	{
		___selectedCrewmate_13 = value;
	}

	inline static int32_t get_offset_of_buttons_14() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___buttons_14)); }
	inline List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * get_buttons_14() const { return ___buttons_14; }
	inline List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E ** get_address_of_buttons_14() { return &___buttons_14; }
	inline void set_buttons_14(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * value)
	{
		___buttons_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttons_14), (void*)value);
	}

	inline static int32_t get_offset_of_eventsManagerController_15() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___eventsManagerController_15)); }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * get_eventsManagerController_15() const { return ___eventsManagerController_15; }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 ** get_address_of_eventsManagerController_15() { return &___eventsManagerController_15; }
	inline void set_eventsManagerController_15(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * value)
	{
		___eventsManagerController_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventsManagerController_15), (void*)value);
	}

	inline static int32_t get_offset_of_numRightOption_16() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___numRightOption_16)); }
	inline int32_t get_numRightOption_16() const { return ___numRightOption_16; }
	inline int32_t* get_address_of_numRightOption_16() { return &___numRightOption_16; }
	inline void set_numRightOption_16(int32_t value)
	{
		___numRightOption_16 = value;
	}

	inline static int32_t get_offset_of_citationsText_17() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___citationsText_17)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_citationsText_17() const { return ___citationsText_17; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_citationsText_17() { return &___citationsText_17; }
	inline void set_citationsText_17(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___citationsText_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___citationsText_17), (void*)value);
	}

	inline static int32_t get_offset_of_selectedOption_18() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___selectedOption_18)); }
	inline int32_t get_selectedOption_18() const { return ___selectedOption_18; }
	inline int32_t* get_address_of_selectedOption_18() { return &___selectedOption_18; }
	inline void set_selectedOption_18(int32_t value)
	{
		___selectedOption_18 = value;
	}

	inline static int32_t get_offset_of_numAttempts_19() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___numAttempts_19)); }
	inline int32_t get_numAttempts_19() const { return ___numAttempts_19; }
	inline int32_t* get_address_of_numAttempts_19() { return &___numAttempts_19; }
	inline void set_numAttempts_19(int32_t value)
	{
		___numAttempts_19 = value;
	}

	inline static int32_t get_offset_of_correctChoiceSelected_20() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___correctChoiceSelected_20)); }
	inline bool get_correctChoiceSelected_20() const { return ___correctChoiceSelected_20; }
	inline bool* get_address_of_correctChoiceSelected_20() { return &___correctChoiceSelected_20; }
	inline void set_correctChoiceSelected_20(bool value)
	{
		___correctChoiceSelected_20 = value;
	}

	inline static int32_t get_offset_of_availableCrewmates_21() { return static_cast<int32_t>(offsetof(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073, ___availableCrewmates_21)); }
	inline List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * get_availableCrewmates_21() const { return ___availableCrewmates_21; }
	inline List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 ** get_address_of_availableCrewmates_21() { return &___availableCrewmates_21; }
	inline void set_availableCrewmates_21(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * value)
	{
		___availableCrewmates_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___availableCrewmates_21), (void*)value);
	}
};


// AU_Task_APIController
struct AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] AU_Task_APIController::interactableObjects
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___interactableObjects_4;
	// AU_InteractableController[] AU_Task_APIController::interactableControllers
	AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* ___interactableControllers_5;
	// UnityEngine.GameObject AU_Task_APIController::multipleChoicePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___multipleChoicePanel_6;
	// AU_ReportController AU_Task_APIController::reportController
	AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * ___reportController_7;
	// AU_Events_ManagerController AU_Task_APIController::eventsController
	AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * ___eventsController_8;
	// UnityEngine.UI.Text AU_Task_APIController::pointsText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___pointsText_9;
	// AU_CompassController AU_Task_APIController::compassController
	AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * ___compassController_10;
	// System.Int32 AU_Task_APIController::currentInteractable
	int32_t ___currentInteractable_11;
	// System.Int32 AU_Task_APIController::numChoices
	int32_t ___numChoices_12;
	// System.Int32 AU_Task_APIController::totalPoints
	int32_t ___totalPoints_13;
	// System.String AU_Task_APIController::playerUsername
	String_t* ___playerUsername_14;
	// System.Int32 AU_Task_APIController::taskID
	int32_t ___taskID_15;
	// System.Int32 AU_Task_APIController::points
	int32_t ___points_16;
	// System.Int32 AU_Task_APIController::attemptsNeeded
	int32_t ___attemptsNeeded_17;
	// System.Int32 AU_Task_APIController::selectedChoiceID
	int32_t ___selectedChoiceID_18;
	// System.String AU_Task_APIController::clientToken
	String_t* ___clientToken_19;
	// System.String AU_Task_APIController::baseURL
	String_t* ___baseURL_20;

public:
	inline static int32_t get_offset_of_interactableObjects_4() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___interactableObjects_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_interactableObjects_4() const { return ___interactableObjects_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_interactableObjects_4() { return &___interactableObjects_4; }
	inline void set_interactableObjects_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___interactableObjects_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactableObjects_4), (void*)value);
	}

	inline static int32_t get_offset_of_interactableControllers_5() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___interactableControllers_5)); }
	inline AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* get_interactableControllers_5() const { return ___interactableControllers_5; }
	inline AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570** get_address_of_interactableControllers_5() { return &___interactableControllers_5; }
	inline void set_interactableControllers_5(AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* value)
	{
		___interactableControllers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactableControllers_5), (void*)value);
	}

	inline static int32_t get_offset_of_multipleChoicePanel_6() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___multipleChoicePanel_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_multipleChoicePanel_6() const { return ___multipleChoicePanel_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_multipleChoicePanel_6() { return &___multipleChoicePanel_6; }
	inline void set_multipleChoicePanel_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___multipleChoicePanel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multipleChoicePanel_6), (void*)value);
	}

	inline static int32_t get_offset_of_reportController_7() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___reportController_7)); }
	inline AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * get_reportController_7() const { return ___reportController_7; }
	inline AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 ** get_address_of_reportController_7() { return &___reportController_7; }
	inline void set_reportController_7(AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * value)
	{
		___reportController_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reportController_7), (void*)value);
	}

	inline static int32_t get_offset_of_eventsController_8() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___eventsController_8)); }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * get_eventsController_8() const { return ___eventsController_8; }
	inline AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 ** get_address_of_eventsController_8() { return &___eventsController_8; }
	inline void set_eventsController_8(AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * value)
	{
		___eventsController_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventsController_8), (void*)value);
	}

	inline static int32_t get_offset_of_pointsText_9() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___pointsText_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_pointsText_9() const { return ___pointsText_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_pointsText_9() { return &___pointsText_9; }
	inline void set_pointsText_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___pointsText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointsText_9), (void*)value);
	}

	inline static int32_t get_offset_of_compassController_10() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___compassController_10)); }
	inline AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * get_compassController_10() const { return ___compassController_10; }
	inline AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 ** get_address_of_compassController_10() { return &___compassController_10; }
	inline void set_compassController_10(AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * value)
	{
		___compassController_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compassController_10), (void*)value);
	}

	inline static int32_t get_offset_of_currentInteractable_11() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___currentInteractable_11)); }
	inline int32_t get_currentInteractable_11() const { return ___currentInteractable_11; }
	inline int32_t* get_address_of_currentInteractable_11() { return &___currentInteractable_11; }
	inline void set_currentInteractable_11(int32_t value)
	{
		___currentInteractable_11 = value;
	}

	inline static int32_t get_offset_of_numChoices_12() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___numChoices_12)); }
	inline int32_t get_numChoices_12() const { return ___numChoices_12; }
	inline int32_t* get_address_of_numChoices_12() { return &___numChoices_12; }
	inline void set_numChoices_12(int32_t value)
	{
		___numChoices_12 = value;
	}

	inline static int32_t get_offset_of_totalPoints_13() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___totalPoints_13)); }
	inline int32_t get_totalPoints_13() const { return ___totalPoints_13; }
	inline int32_t* get_address_of_totalPoints_13() { return &___totalPoints_13; }
	inline void set_totalPoints_13(int32_t value)
	{
		___totalPoints_13 = value;
	}

	inline static int32_t get_offset_of_playerUsername_14() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___playerUsername_14)); }
	inline String_t* get_playerUsername_14() const { return ___playerUsername_14; }
	inline String_t** get_address_of_playerUsername_14() { return &___playerUsername_14; }
	inline void set_playerUsername_14(String_t* value)
	{
		___playerUsername_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerUsername_14), (void*)value);
	}

	inline static int32_t get_offset_of_taskID_15() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___taskID_15)); }
	inline int32_t get_taskID_15() const { return ___taskID_15; }
	inline int32_t* get_address_of_taskID_15() { return &___taskID_15; }
	inline void set_taskID_15(int32_t value)
	{
		___taskID_15 = value;
	}

	inline static int32_t get_offset_of_points_16() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___points_16)); }
	inline int32_t get_points_16() const { return ___points_16; }
	inline int32_t* get_address_of_points_16() { return &___points_16; }
	inline void set_points_16(int32_t value)
	{
		___points_16 = value;
	}

	inline static int32_t get_offset_of_attemptsNeeded_17() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___attemptsNeeded_17)); }
	inline int32_t get_attemptsNeeded_17() const { return ___attemptsNeeded_17; }
	inline int32_t* get_address_of_attemptsNeeded_17() { return &___attemptsNeeded_17; }
	inline void set_attemptsNeeded_17(int32_t value)
	{
		___attemptsNeeded_17 = value;
	}

	inline static int32_t get_offset_of_selectedChoiceID_18() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___selectedChoiceID_18)); }
	inline int32_t get_selectedChoiceID_18() const { return ___selectedChoiceID_18; }
	inline int32_t* get_address_of_selectedChoiceID_18() { return &___selectedChoiceID_18; }
	inline void set_selectedChoiceID_18(int32_t value)
	{
		___selectedChoiceID_18 = value;
	}

	inline static int32_t get_offset_of_clientToken_19() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___clientToken_19)); }
	inline String_t* get_clientToken_19() const { return ___clientToken_19; }
	inline String_t** get_address_of_clientToken_19() { return &___clientToken_19; }
	inline void set_clientToken_19(String_t* value)
	{
		___clientToken_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientToken_19), (void*)value);
	}

	inline static int32_t get_offset_of_baseURL_20() { return static_cast<int32_t>(offsetof(AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C, ___baseURL_20)); }
	inline String_t* get_baseURL_20() const { return ___baseURL_20; }
	inline String_t** get_address_of_baseURL_20() { return &___baseURL_20; }
	inline void set_baseURL_20(String_t* value)
	{
		___baseURL_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseURL_20), (void*)value);
	}
};


// TimerController
struct TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text TimerController::timeCounter
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___timeCounter_4;
	// System.TimeSpan TimerController::timePlaying
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___timePlaying_5;
	// System.Boolean TimerController::timerGoing
	bool ___timerGoing_6;
	// System.Single TimerController::elapsedTime
	float ___elapsedTime_7;
	// UnityEngine.GameObject[] TimerController::bots
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___bots_8;
	// UnityEngine.GameObject[] TimerController::imposters
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___imposters_9;
	// System.Int32 TimerController::alreadySpawn
	int32_t ___alreadySpawn_10;
	// System.Boolean TimerController::spawned
	bool ___spawned_11;

public:
	inline static int32_t get_offset_of_timeCounter_4() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___timeCounter_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_timeCounter_4() const { return ___timeCounter_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_timeCounter_4() { return &___timeCounter_4; }
	inline void set_timeCounter_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___timeCounter_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timeCounter_4), (void*)value);
	}

	inline static int32_t get_offset_of_timePlaying_5() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___timePlaying_5)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_timePlaying_5() const { return ___timePlaying_5; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_timePlaying_5() { return &___timePlaying_5; }
	inline void set_timePlaying_5(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___timePlaying_5 = value;
	}

	inline static int32_t get_offset_of_timerGoing_6() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___timerGoing_6)); }
	inline bool get_timerGoing_6() const { return ___timerGoing_6; }
	inline bool* get_address_of_timerGoing_6() { return &___timerGoing_6; }
	inline void set_timerGoing_6(bool value)
	{
		___timerGoing_6 = value;
	}

	inline static int32_t get_offset_of_elapsedTime_7() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___elapsedTime_7)); }
	inline float get_elapsedTime_7() const { return ___elapsedTime_7; }
	inline float* get_address_of_elapsedTime_7() { return &___elapsedTime_7; }
	inline void set_elapsedTime_7(float value)
	{
		___elapsedTime_7 = value;
	}

	inline static int32_t get_offset_of_bots_8() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___bots_8)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_bots_8() const { return ___bots_8; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_bots_8() { return &___bots_8; }
	inline void set_bots_8(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___bots_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bots_8), (void*)value);
	}

	inline static int32_t get_offset_of_imposters_9() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___imposters_9)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_imposters_9() const { return ___imposters_9; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_imposters_9() { return &___imposters_9; }
	inline void set_imposters_9(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___imposters_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imposters_9), (void*)value);
	}

	inline static int32_t get_offset_of_alreadySpawn_10() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___alreadySpawn_10)); }
	inline int32_t get_alreadySpawn_10() const { return ___alreadySpawn_10; }
	inline int32_t* get_address_of_alreadySpawn_10() { return &___alreadySpawn_10; }
	inline void set_alreadySpawn_10(int32_t value)
	{
		___alreadySpawn_10 = value;
	}

	inline static int32_t get_offset_of_spawned_11() { return static_cast<int32_t>(offsetof(TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA, ___spawned_11)); }
	inline bool get_spawned_11() const { return ___spawned_11; }
	inline bool* get_address_of_spawned_11() { return &___spawned_11; }
	inline void set_spawned_11(bool value)
	{
		___spawned_11 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * m_Items[1];

public:
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// AU_InteractableController[]
struct AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * m_Items[1];

public:
	inline AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_GetStatic_TisRuntimeObject_mADE90D1F6955D96985F1F2BDE05094C3578F3AEC_gshared (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * __this, String_t* ___fieldName0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_gshared_inline (const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m6339FC2D3D1CE4FA13CF21C7F9FC58CA4441BF0C_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m2D53662347629A70E2A610B61ACC705D63F22D34_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputAction::ReadValue<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C_gshared (InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m5E241FA6C20E2C54A2317FEE950354EB3BCE4454_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m533C28B362284747FD5138B02D183642545146E8_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// System.Void AU_BotController::RandomMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Sign(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB (float ___f0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// System.String UnityEngine.Component::get_tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___euler0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA (float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_mEFF9F51871F231955D97DABDE9AB4A6B4EDA5541 (AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * __this, String_t* ___className0, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * __this, String_t* ___fieldName0, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * (*) (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E *, String_t*, const RuntimeMethod*))AndroidJavaObject_GetStatic_TisRuntimeObject_mADE90D1F6955D96985F1F2BDE05094C3578F3AEC_gshared)(__this, ___fieldName0, method);
}
// !!0[] System.Array::Empty<System.Object>()
inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_inline (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_gshared_inline)(method);
}
// System.Void UnityEngine.AndroidJavaObject::Call(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject_Call_mBB226DA52CE5A2FCD9A2D42BC7FB4272E094B76D (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * __this, String_t* ___methodName0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void AU_Game_ManagerController::CloseExitGamePanel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_CloseExitGamePanel_mB753B64D3E9E06B1539DBF14EBF301BE8AF0EB85 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method);
// System.Void AU_Game_ManagerController::OpenGameOverPanel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_OpenGameOverPanel_m6CE48B636467247A9FDD4FB3F17183B7499A5F1E (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method);
// System.Void AU_ImposterController::RandomMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_RandomMovement_m11651730AC62838E8DD6BCDCACE0CC3F46F946FE (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * Component_GetComponent_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_m07218953750B94DD5E57038A5CBDD7CCDA8C35D9 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// System.Void AU_ImposterController::KillPlayer(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_KillPlayer_mEB9F1A4AB3B3CD2162B7F145E72ABFCFE1A7C535 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m61816C8A09C86A5E157EA89965A9CC0510A1B378 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulers0, int32_t ___relativeTo1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___translation0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void AU_Events_ManagerController::PlayerKilledFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_PlayerKilledFunction_mF395AF572D904283AE7ADB5C206B7A804616247B (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void AU_Events_ManagerController::StopBotsFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_StopBotsFunction_mA0761ACD1FDE56D7F6BBCEFDBC13235504EBABA6 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<TimerController>()
inline TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * Component_GetComponent_TisTimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA_m11D762F94106F90C52A14CE1D94F8480687FAEA1 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_mDC2250CC3F24F6FE45660AF6153056ABDA5ED60F_gshared)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  List_1_GetEnumerator_mC3C92BFD7725EFE888D9CD9022D7FD8B62797903 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_GetEnumerator_m6339FC2D3D1CE4FA13CF21C7F9FC58CA4441BF0C_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m6A1D5267B49F39503A0BF02B983C18FA2EB5094A_inline (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline)(__this, method);
}
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mCBD44A97260E33F58E2FBBD76E0432C8F120C30E (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m2D53662347629A70E2A610B61ACC705D63F22D34_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54 (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7 (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void AU_Multiple_Choice_TaskController::SetVisibilityPanel(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Multiple_Choice_TaskController_SetVisibilityPanel_m79EB6991B46DEB691805ACD980007923B64965D9 (AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void AU_Events_ManagerController::TaskCompletedFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_TaskCompletedFunction_m3FE657508E043AD47F726D7072B74DDB28F7A8C5 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_Enable_m21D09D41AA886DD0888BF47AFDFB089E1A73A022 (InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_Disable_m44F211FC0ADED664EE732F097D3DDD2998348ED4 (InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputAction::ReadValue<UnityEngine.Vector2>()
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C (InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 *, const RuntimeMethod*))InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1 (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * __this, bool ___value0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
inline String_t* List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_inline (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, int32_t, const RuntimeMethod*))List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline)(__this, ___index0, method);
}
// System.Boolean UnityEngine.ColorUtility::TryParseHtmlString(System.String,UnityEngine.Color&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069 (String_t* ___htmlString0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * ___color1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Button>::.ctor()
inline void List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795 (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, String_t*, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<AU_ReportController/Imposter>::.ctor()
inline void List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void AU_ReportController/<>c__DisplayClass21_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass21_0__ctor_m5A06A23E43E1C80F9D1A1F6A09ADE9B302DB567A (U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m85FD3AD1E7C2C143E7D5491BBB781F1C3DA22B85 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m5E241FA6C20E2C54A2317FEE950354EB3BCE4454_gshared)(___original0, method);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mAB159CED4EA42F6E700527382D795CCD31EF293F (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m533C28B362284747FD5138B02D183642545146E8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Button>::Add(!0)
inline void List_1_Add_m07C610356AB5C68760B3F1A16391671DCCD8FC89 (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * __this, Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E *, Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  Selectable_get_colors_m47C712DD0CFA000DAACD750853E81E981C90B7D9_inline (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ColorBlock_set_normalColor_m32EB40A0BB6DD89B8816945EF43CFED8A1ED78B9_inline (ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_set_colors_m2610F85E7DC191E0AA2D71E2447BA5B58B7C4621 (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___value0, const RuntimeMethod* method);
// System.Void AU_ReportController/Imposter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Imposter__ctor_m96B7959C59C5943F69B87E90948B635EC8C8D93B (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, const RuntimeMethod* method);
// System.Void AU_ReportController/Imposter::set_Id(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Imposter_set_Id_mB6C591DEF175F5270ADE9BAF9F241EAB046B050D_inline (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void AU_ReportController/Imposter::set_Available(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Imposter_set_Available_m219488AE60B148B7A5635AFCD25630259A4D91D4_inline (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<AU_ReportController/Imposter>::Add(!0)
inline void List_1_Add_m809A2F3C03A4DFC84691D8615C8413FBC0039401 (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * __this, Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 *, Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_inline (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<AU_ReportController/Imposter>::get_Count()
inline int32_t List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_inline (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<AU_ReportController/Imposter>::get_Item(System.Int32)
inline Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_inline (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * (*) (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 *, int32_t, const RuntimeMethod*))List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline)(__this, ___index0, method);
}
// System.Boolean AU_ReportController/Imposter::get_Available()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Imposter_get_Available_mC21C8E2B2BB1A37CA7209A6EE8A1C3D016DDFEE1_inline (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, const RuntimeMethod* method);
// System.Void AU_ReportController::ChangeImposterUI(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_ChangeImposterUI_m01A395D663821382271B97FEEE3C77FE0DBFE4C7 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, int32_t ___nextCrewmate0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.UI.Button>::get_Item(System.Int32)
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * List_1_get_Item_m77F12A63EB4FD22AA4D725B91DC135C20B32AE0A_inline (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E *, int32_t, const RuntimeMethod*))List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline)(__this, ___index0, method);
}
// System.Void AU_Events_ManagerController::WrongChoiceSelectedFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_WrongChoiceSelectedFunction_m7F64D8A07BBD19E74CF7D82C4B2688C0E2D786C1 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void AU_ReportController::SetVisibilityPanel(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_SetVisibilityPanel_m01A5A08C5033F9EEAAD518F92AB72EBB7014CC80 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, bool ___active0, const RuntimeMethod* method);
// System.Collections.IEnumerator AU_Task_APIController::GetImposterCitationSentimentTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AU_Task_APIController_GetImposterCitationSentimentTask_mCF122B6970729C70AEAE9A97BE63C61BE89DE02F (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void AU_Task_APIController::SelectRandInteractable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SelectRandInteractable_m3FA47C631CCF634FB783918BC4BD8CFFAD6F4F23 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method);
// System.Void AU_Task_APIController::SetNewTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SetNewTask_m8E43365A9441987808C31DCBA4042F71D3134E49 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method);
// System.Void AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetImposterCitationSentimentTaskU3Ed__19__ctor_m0EF99F80F43393ECE3B0AD38F827BAAE3F3A9C4C (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// System.Void AU_InteractableController::SetInteractableAsActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController_SetInteractableAsActive_m14B7EEFE6F3B356A150EE9D2594FED89D5D369AD (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, bool ___active0, const RuntimeMethod* method);
// System.Void AU_InteractableController::SetTaskToShow(UnityEngine.GameObject)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AU_InteractableController_SetTaskToShow_m1A1ECAFB9D7479F16572921C0862C9308B57CEAC_inline (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___taskPanelToShow0, const RuntimeMethod* method);
// System.Void AU_ReportController::SubmitTaskInfo(System.Collections.Generic.List`1<System.String>,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_SubmitTaskInfo_m07D0AC4A03AD44782B7E09A14155A4DC145D1165 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___citations0, String_t* ___question1, int32_t ___correctAnswerNumber2, const RuntimeMethod* method);
// System.Void AU_CompassController::SetTarget(UnityEngine.GameObject)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AU_CompassController_SetTarget_m333CA468882EF3AD43388DFC1FF8EFF5257179C1_inline (AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___newTarget0, const RuntimeMethod* method);
// System.Collections.IEnumerator AU_Task_APIController::SendTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AU_Task_APIController_SendTask_m7C78F6BB0F623750BF2345C08279C4F256A9C401 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method);
// System.Void AU_Task_APIController/<SendTask>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendTaskU3Ed__23__ctor_m8DE32D4A24776E5FC14967A368DFF03EA4D84AE6 (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void TimerController::BeginTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController_BeginTimer_mAA181BB489DE0D7B29773360AC850C0208BF3202 (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator TimerController::UpdateTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TimerController_UpdateTimer_m09B308F03DCC34431BA5C4231CC62E1983914F49 (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method);
// System.Void TimerController/<UpdateTimer>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateTimerU3Ed__11__ctor_m1AF49DBD7A9D3A6EFACE8ADAB4AFD02B3A3B0E98 (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Int32 System.TimeSpan::get_Seconds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TimeSpan_get_Seconds_m3324F3A1F96CA956DAEDDB69DB32CAA320A053F7 (TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * __this, const RuntimeMethod* method);
// System.Int32 System.TimeSpan::get_Minutes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TimeSpan_get_Minutes_mF5A78108FEB64953C298CEC19637378380881202 (TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void AU_ReportController::ButtonClicked(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_ButtonClicked_m3652675D81FFC11A06E6479BEFD08B94807386BE (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, int32_t ___numButton0, const RuntimeMethod* method);
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A (String_t* ___uri0, const RuntimeMethod* method);
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, String_t* ___name0, String_t* ___value1, const RuntimeMethod* method);
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isHttpError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isHttpError_m23ADCBE1CF082A940EF6AA75A15051583228B616 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.String UnityEngine.Networking.DownloadHandler::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A (DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * __this, const RuntimeMethod* method);
// SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D (String_t* ___aJSON0, const RuntimeMethod* method);
// System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF (JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * ___d0, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C (String_t* ___s0, const RuntimeMethod* method);
// System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50 (JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * ___d0, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46  Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9 (Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8 * __this, const RuntimeMethod* method);
// SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * JSONNode_op_Implicit_m757755AFE21B579A47662DAA9BD32620FC7BC7F2 (KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46  ___aKeyValue0, const RuntimeMethod* method);
// System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool JSONNode_op_Equality_mEB349D8644B8E3F87CD33A35457A00480EFB329A (JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * ___a0, RuntimeObject * ___b1, const RuntimeMethod* method);
// System.Boolean SimpleJSON.JSONNode/Enumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E (Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8 * __this, const RuntimeMethod* method);
// System.Void AU_Task_APIController::SetReportPanelSettings(System.Collections.Generic.List`1<System.String>,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SetReportPanelSettings_m1949E74AD77596BD8283CB0BC3AC1EE2DAFE1ACF (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___choices0, String_t* ___question1, int32_t ___correctAnswer2, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void AU_Events_ManagerController::StartBotsFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_StartBotsFunction_m16FA216A0E86CB53E68C745F17E2154D77DEF9C6 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD (float ___f0, const RuntimeMethod* method);
// System.Void MultipleChoiceRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultipleChoiceRequest__ctor_mEDE2D8308BB37F3210C7F4E4CDD9A78FA6BCDFFF (MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * __this, const RuntimeMethod* method);
// System.String UnityEngine.JsonUtility::ToJson(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest__ctor_mC2ED369A4ACE53AFF2E70A38BE95EB48D68D4975 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, String_t* ___url0, String_t* ___method1, const RuntimeMethod* method);
// System.Text.Encoding System.Text.Encoding::get_UTF8()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E (const RuntimeMethod* method);
// System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UploadHandlerRaw__ctor_mB46261D7AA64B605D5CA8FF9027A4A32E57A7BD9 (UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data0, const RuntimeMethod* method);
// System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_set_uploadHandler_m8D5DF24FBE7F8F0DCF27E11CE3C6CF4363DF23BA (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DownloadHandlerBuffer__ctor_m01FD35970E4B4FC45FC99A648408F53A8B164774 (DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_set_downloadHandler_m7496D2C5F755BEB68651A4F33EA9BDA319D092C2 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * ___value0, const RuntimeMethod* method);
// System.TimeSpan System.TimeSpan::FromSeconds(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  TimeSpan_FromSeconds_m4644EABECA69BC6C07AD649C5898A8E53F4FE7B0 (double ___value0, const RuntimeMethod* method);
// System.String System.TimeSpan::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TimeSpan_ToString_m7EA7DE3511D395B11A477C384FFFC40B40D3BA9F (TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * __this, String_t* ___format0, const RuntimeMethod* method);
// System.Void TimerController::GenerateBotAndImposter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController_GenerateBotAndImposter_mEBEF71D98829A65C13330DB1499566A993B4EA1B (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_BotController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_Start_m7796F437384EB35DC57043F13889BC9FA814B014 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// myRB = GetComponent<Rigidbody>();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0;
		L_0 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		__this->set_myRB_4(L_0);
		// myAvatar = transform.GetChild(0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_1, 0, /*hidden argument*/NULL);
		__this->set_myAvatar_5(L_2);
		// myAnim = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3;
		L_3 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var);
		__this->set_myAnim_6(L_3);
		// RandomMovement();
		AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_BotController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_Update_m81555C66C1CFF34117EEA2EE95A0A97C806DF437 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (playerIsAnsweringTask)
		bool L_0 = __this->get_playerIsAnsweringTask_13();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		// myRB.velocity = Vector3.zero;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_1 = __this->get_myRB_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_1, L_2, /*hidden argument*/NULL);
		// myRB.angularVelocity = Vector3.zero;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_3 = __this->get_myRB_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_3);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_3, L_4, /*hidden argument*/NULL);
		// }
		goto IL_009e;
	}

IL_002a:
	{
		// timeLeft -= Time.deltaTime;
		float L_5 = __this->get_timeLeft_12();
		float L_6;
		L_6 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timeLeft_12(((float)il2cpp_codegen_subtract((float)L_5, (float)L_6)));
		// if (timeLeft <= -1)
		float L_7 = __this->get_timeLeft_12();
		if ((!(((float)L_7) <= ((float)(-1.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		// RandomMovement();
		AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED(__this, /*hidden argument*/NULL);
		// timeLeft += accelerationTime;
		float L_8 = __this->get_timeLeft_12();
		float L_9 = __this->get_accelerationTime_9();
		__this->set_timeLeft_12(((float)il2cpp_codegen_add((float)L_8, (float)L_9)));
	}

IL_0062:
	{
		// if (movement.x != 0)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_10 = __this->get_address_of_movement_11();
		float L_11 = L_10->get_x_0();
		if ((((float)L_11) == ((float)(0.0f))))
		{
			goto IL_009e;
		}
	}
	{
		// myAvatar.localScale = new Vector2(Mathf.Sign(movement.x), 1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = __this->get_myAvatar_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_13 = __this->get_address_of_movement_11();
		float L_14 = L_13->get_x_0();
		float L_15;
		L_15 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_14, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_16), L_15, (1.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_12, L_17, /*hidden argument*/NULL);
	}

IL_009e:
	{
		// myAnim.SetFloat("Speed", movement.magnitude);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_18 = __this->get_myAnim_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_19 = __this->get_address_of_movement_11();
		float L_20;
		L_20 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_18, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_20, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_BotController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_FixedUpdate_m5AC5393A439659B745A73CED676F18DBC0962F38 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	{
		// if(!playerIsAnsweringTask)
		bool L_0 = __this->get_playerIsAnsweringTask_13();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		// myRB.velocity = movement * maxSpeed;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_1 = __this->get_myRB_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = __this->get_movement_11();
		float L_3 = __this->get_maxSpeed_10();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_2, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_1, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void AU_BotController::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_OnTriggerEnter_m5E17B2B114D06A85A27662651B3106ADBD7DA590 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7649AEE062EE200D5810926162F39A75BCEE5287);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Wall")
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteral7649AEE062EE200D5810926162F39A75BCEE5287, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// RandomMovement();
		AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void AU_BotController::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_OnTriggerExit_m6E8BC8ED7F435ABA288A109F06C3817ADC938596 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AU_BotController::RandomMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	{
		// movement = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		float L_0;
		L_0 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_1;
		L_1 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((-1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		__this->set_movement_11(L_2);
		// }
		return;
	}
}
// System.Void AU_BotController::StartBots()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_StartBots_mB75F8CE3EC8CB41150E88E80CAE58B803B7212CA (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	{
		// playerIsAnsweringTask = false;
		__this->set_playerIsAnsweringTask_13((bool)0);
		// }
		return;
	}
}
// System.Void AU_BotController::StopBots()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController_StopBots_m60CD651C7611C5E1D71834CEE6B14275AD5019B7 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	{
		// playerIsAnsweringTask = true;
		__this->set_playerIsAnsweringTask_13((bool)1);
		// }
		return;
	}
}
// System.Void AU_BotController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_BotController__ctor_mF59665130179C15CA492F8C274911D9106201AB1 (AU_BotController_t9510282E2BEC9C3F55FE164E4DC807AA53E13308 * __this, const RuntimeMethod* method)
{
	{
		// public float accelerationTime = 4f;
		__this->set_accelerationTime_9((4.0f));
		// public float maxSpeed = 5f;
		__this->set_maxSpeed_10((5.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_CompassController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_CompassController_Start_m1DAC490117E5A81A8E4A11EAE7F1F490EB2AE606 (AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AU_CompassController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_CompassController_Update_m4BA1F25FED3FA85620113A69E309375797C412B5 (AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// Vector3 dir = target.transform.position - player.transform.position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_target_6();
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_player_5();
		NullCheck(L_3);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		float L_8 = L_7.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = V_0;
		float L_10 = L_9.get_x_2();
		float L_11;
		L_11 = atan2f(L_8, L_10);
		V_1 = ((float)il2cpp_codegen_multiply((float)L_11, (float)(57.2957802f)));
		// Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, angle));
		float L_12 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_13), (0.0f), (0.0f), L_12, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_14;
		L_14 = Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		// compass.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 100);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_compass_4();
		NullCheck(L_15);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18;
		L_18 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_17, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_19 = V_2;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_20;
		L_20 = Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8(L_18, L_19, (100.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_16, L_20, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_CompassController::SetTarget(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_CompassController_SetTarget_m333CA468882EF3AD43388DFC1FF8EFF5257179C1 (AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___newTarget0, const RuntimeMethod* method)
{
	{
		// target = newTarget;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___newTarget0;
		__this->set_target_6(L_0);
		// }
		return;
	}
}
// System.Void AU_CompassController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_CompassController__ctor_m8B39F084268D2A88337D56AAA46BA018C09A4ADF (AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_Events_ManagerController::TaskCompletedFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_TaskCompletedFunction_m3FE657508E043AD47F726D7072B74DDB28F7A8C5 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method)
{
	{
		// TaskCompleted.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_TaskCompleted_4();
		NullCheck(L_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Events_ManagerController::PlayerKilledFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_PlayerKilledFunction_mF395AF572D904283AE7ADB5C206B7A804616247B (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method)
{
	{
		// PlayerKilled.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_PlayerKilled_5();
		NullCheck(L_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Events_ManagerController::StopBotsFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_StopBotsFunction_mA0761ACD1FDE56D7F6BBCEFDBC13235504EBABA6 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method)
{
	{
		// StopBots.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_StopBots_6();
		NullCheck(L_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Events_ManagerController::StartBotsFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_StartBotsFunction_m16FA216A0E86CB53E68C745F17E2154D77DEF9C6 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method)
{
	{
		// StartBots.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_StartBots_7();
		NullCheck(L_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Events_ManagerController::WrongChoiceSelectedFunction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController_WrongChoiceSelectedFunction_m7F64D8A07BBD19E74CF7D82C4B2688C0E2D786C1 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method)
{
	{
		// WrongChoiceSelected.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_WrongChoiceSelected_8();
		NullCheck(L_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Events_ManagerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Events_ManagerController__ctor_m286C0EE74E14A459FA8AD8A215E2A1E05037A092 (AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_Game_ManagerController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_Awake_m6215ABB8E34964976ED4523911102A892D419C6E (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12030968A66BA0D54A39FD2688C2FDF347383378);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * V_0 = NULL;
	AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// Time.timeScale = 0f;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// startPanel.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_startPanel_5();
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_1 = (AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 *)il2cpp_codegen_object_new(AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mEFF9F51871F231955D97DABDE9AB4A6B4EDA5541(L_1, _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0021:
	try
	{// begin try (depth: 1)
		{
			// using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_2 = V_0;
			NullCheck(L_2);
			AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_3;
			L_3 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B(L_2, _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B_RuntimeMethod_var);
			V_1 = L_3;
		}

IL_002d:
		try
		{// begin try (depth: 2)
			// obj_Activity.Call("getClientToken");
			AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_4 = V_1;
			ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5;
			L_5 = Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_RuntimeMethod_var);
			NullCheck(L_4);
			AndroidJavaObject_Call_mBB226DA52CE5A2FCD9A2D42BC7FB4272E094B76D(L_4, _stringLiteral12030968A66BA0D54A39FD2688C2FDF347383378, L_5, /*hidden argument*/NULL);
			// }
			IL2CPP_LEAVE(0x53, FINALLY_003f);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_003f;
		}

FINALLY_003f:
		{// begin finally (depth: 2)
			{
				AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_6 = V_1;
				if (!L_6)
				{
					goto IL_0048;
				}
			}

IL_0042:
			{
				AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_7 = V_1;
				NullCheck(L_7);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_7);
			}

IL_0048:
			{
				IL2CPP_END_FINALLY(63)
			}
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(63)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x53, FINALLY_0049);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{// begin finally (depth: 1)
		{
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_8 = V_0;
			if (!L_8)
			{
				goto IL_0052;
			}
		}

IL_004c:
		{
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_9 = V_0;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_9);
		}

IL_0052:
		{
			IL2CPP_END_FINALLY(73)
		}
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x53, IL_0053)
	}

IL_0053:
	{
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_OnEnable_m3B1F4AA5EF833EF601CD95DD89F8DA614EAAC770 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// startButton.onClick.AddListener(StartGame);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0 = __this->get_startButton_4();
		NullCheck(L_0);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_1;
		L_1 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_0, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_2 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_2, __this, (intptr_t)((intptr_t)AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_OnDisable_m83DA97F63DCCC218848A719EC5D88D6626E845DD (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// startButton.onClick.RemoveListener(StartGame);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0 = __this->get_startButton_4();
		NullCheck(L_0);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_1;
		L_1 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_0, /*hidden argument*/NULL);
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_2 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_2, __this, (intptr_t)((intptr_t)AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	{
		// Time.timeScale = 1f;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// startPanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_startPanel_5();
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::OpenExitGamePanel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_OpenExitGamePanel_mA249E1089C61FC06572104217472ED138F49383F (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	{
		// exitGamePanel.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_exitGamePanel_6();
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::CloseExitGamePanel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_CloseExitGamePanel_mB753B64D3E9E06B1539DBF14EBF301BE8AF0EB85 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	{
		// exitGamePanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_exitGamePanel_6();
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::OpenGameOverPanel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_OpenGameOverPanel_m6CE48B636467247A9FDD4FB3F17183B7499A5F1E (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral331F4DFDDDEF540325D068392B749A7EF80E4C56);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0876FD70181B47400B01C8D4BDD907A9F69D4D5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gameOverPointsTextPanel.text = "You received " + TasksManagerController.totalPoints + " points.";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_gameOverPointsTextPanel_8();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_1 = __this->get_TasksManagerController_9();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_totalPoints_13();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_2, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral331F4DFDDDEF540325D068392B749A7EF80E4C56, L_3, _stringLiteralB0876FD70181B47400B01C8D4BDD907A9F69D4D5, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		// gameOverPanel.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_gameOverPanel_7();
		NullCheck(L_5);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::ContinueButtonPressedGameOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_ContinueButtonPressedGameOver_m8CA89D68F0811FE3A2BAFB3C29B39A7CEB91DAE3 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF3A86EBDBF8EA9FCD0E9713006C9251860EBB9CA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * V_0 = NULL;
	AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_0 = (AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 *)il2cpp_codegen_object_new(AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mEFF9F51871F231955D97DABDE9AB4A6B4EDA5541(L_0, _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_000b:
	try
	{// begin try (depth: 1)
		{
			// using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_1 = V_0;
			NullCheck(L_1);
			AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_2;
			L_2 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B(L_1, _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_m630526394238171B0B85941DB7BD5FB885296D1B_RuntimeMethod_var);
			V_1 = L_2;
		}

IL_0017:
		try
		{// begin try (depth: 2)
			// obj_Activity.Call("exitGame");
			AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_3 = V_1;
			ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_4;
			L_4 = Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_RuntimeMethod_var);
			NullCheck(L_3);
			AndroidJavaObject_Call_mBB226DA52CE5A2FCD9A2D42BC7FB4272E094B76D(L_3, _stringLiteralF3A86EBDBF8EA9FCD0E9713006C9251860EBB9CA, L_4, /*hidden argument*/NULL);
			// }
			IL2CPP_LEAVE(0x3D, FINALLY_0029);
		}// end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0029;
		}

FINALLY_0029:
		{// begin finally (depth: 2)
			{
				AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_5 = V_1;
				if (!L_5)
				{
					goto IL_0032;
				}
			}

IL_002c:
			{
				AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_6 = V_1;
				NullCheck(L_6);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_6);
			}

IL_0032:
			{
				IL2CPP_END_FINALLY(41)
			}
		}// end finally (depth: 2)
		IL2CPP_CLEANUP(41)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x3D, FINALLY_0033);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{// begin finally (depth: 1)
		{
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_7 = V_0;
			if (!L_7)
			{
				goto IL_003c;
			}
		}

IL_0036:
		{
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_8);
		}

IL_003c:
		{
			IL2CPP_END_FINALLY(51)
		}
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
	}

IL_003d:
	{
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::ExitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_ExitGame_mC81BA9A3C36A15EAF0879928E66CAB83B0DE41E0 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	{
		// CloseExitGamePanel();
		AU_Game_ManagerController_CloseExitGamePanel_mB753B64D3E9E06B1539DBF14EBF301BE8AF0EB85(__this, /*hidden argument*/NULL);
		// OpenGameOverPanel();
		AU_Game_ManagerController_OpenGameOverPanel_m6CE48B636467247A9FDD4FB3F17183B7499A5F1E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::PlayerKilled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController_PlayerKilled_mD00BE7AF70571DED772F54B56C7C2D5B9F4D6BF6 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	{
		// OpenGameOverPanel();
		AU_Game_ManagerController_OpenGameOverPanel_m6CE48B636467247A9FDD4FB3F17183B7499A5F1E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Game_ManagerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Game_ManagerController__ctor_m89B9D1CD615856A679C922B15619DD0D755D57B9 (AU_Game_ManagerController_tB9F464B2A304834AD551AB907081A9A609706148 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_ImposterController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_Awake_mB0141E96BF25709E92D60B35EC54A1B908ABAA1F (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	{
		// _transform = transform;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		__this->set__transform_16(L_0);
		// }
		return;
	}
}
// System.Void AU_ImposterController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_Start_mEB9029353EA9D2F6A3F303548454F9334B10957F (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// myRB = GetComponent<Rigidbody>();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0;
		L_0 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		__this->set_myRB_5(L_0);
		// myAvatar = transform.GetChild(0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_1, 0, /*hidden argument*/NULL);
		__this->set_myAvatar_6(L_2);
		// myAnim = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3;
		L_3 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var);
		__this->set_myAnim_7(L_3);
		// originalRotationValue = transform.rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_5;
		L_5 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_4, /*hidden argument*/NULL);
		__this->set_originalRotationValue_13(L_5);
		// RandomMovement();
		AU_ImposterController_RandomMovement_m11651730AC62838E8DD6BCDCACE0CC3F46F946FE(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ImposterController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_Update_m428222B2448192968FB8E9EFB2BB62A54775CFC5 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_m07218953750B94DD5E57038A5CBDD7CCDA8C35D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (playerIsAnsweringTask)
		bool L_0 = __this->get_playerIsAnsweringTask_18();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		// myRB.velocity = Vector3.zero;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_1 = __this->get_myRB_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_1, L_2, /*hidden argument*/NULL);
		// myRB.angularVelocity = Vector3.zero;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_3 = __this->get_myRB_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_3);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0029:
	{
		// else if (Vector3.Distance(_transform.position, _player.position) < 1f)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get__transform_16();
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = __this->get__player_14();
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		float L_9;
		L_9 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_6, L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) < ((float)(1.0f)))))
		{
			goto IL_005d;
		}
	}
	{
		// KillPlayer(_player.GetComponent<Collider>());
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10 = __this->get__player_14();
		NullCheck(L_10);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_11;
		L_11 = Component_GetComponent_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_m07218953750B94DD5E57038A5CBDD7CCDA8C35D9(L_10, /*hidden argument*/Component_GetComponent_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_m07218953750B94DD5E57038A5CBDD7CCDA8C35D9_RuntimeMethod_var);
		AU_ImposterController_KillPlayer_mEB9F1A4AB3B3CD2162B7F145E72ABFCFE1A7C535(__this, L_11, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_005d:
	{
		// else if (followingPlayer)
		bool L_12 = __this->get_followingPlayer_15();
		if (!L_12)
		{
			goto IL_00eb;
		}
	}
	{
		// _moveSpeed = 2f;
		__this->set__moveSpeed_17((2.0f));
		// _transform.LookAt(_player.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13 = __this->get__transform_16();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14 = __this->get__player_14();
		NullCheck(L_14);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA(L_13, L_15, /*hidden argument*/NULL);
		// _transform.Rotate(new Vector3(0, -90, 0), Space.Self);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16 = __this->get__transform_16();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		memset((&L_17), 0, sizeof(L_17));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_17), (0.0f), (-90.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_Rotate_m61816C8A09C86A5E157EA89965A9CC0510A1B378(L_16, L_17, 1, /*hidden argument*/NULL);
		// _transform.Translate(new Vector3(_moveSpeed * Time.deltaTime, 0, 0));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18 = __this->get__transform_16();
		float L_19 = __this->get__moveSpeed_17();
		float L_20;
		L_20 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		memset((&L_21), 0, sizeof(L_21));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_21), ((float)il2cpp_codegen_multiply((float)L_19, (float)L_20)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0(L_18, L_21, /*hidden argument*/NULL);
		// myAnim.SetFloat("Speed", movement.magnitude);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_22 = __this->get_myAnim_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_23 = __this->get_address_of_movement_10();
		float L_24;
		L_24 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_22, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_24, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00eb:
	{
		// RandomMovement();
		AU_ImposterController_RandomMovement_m11651730AC62838E8DD6BCDCACE0CC3F46F946FE(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ImposterController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_FixedUpdate_mFFBDF390F80A578A7F9987E3BCB7B7B8CFE0C8D7 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	{
		// if (!followingPlayer && !playerIsAnsweringTask)
		bool L_0 = __this->get_followingPlayer_15();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		bool L_1 = __this->get_playerIsAnsweringTask_18();
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		// myRB.velocity = movement * maxSpeed;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_2 = __this->get_myRB_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = __this->get_movement_10();
		float L_4 = __this->get_maxSpeed_9();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_3, L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_2, L_6, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Void AU_ImposterController::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_OnTriggerEnter_m9F3F1AF16132D6E4E97FA8A2B92E0540C7083399 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7649AEE062EE200D5810926162F39A75BCEE5287);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Player")
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		// myRB.velocity = Vector3.zero;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_3 = __this->get_myRB_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_3);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_3, L_4, /*hidden argument*/NULL);
		// myRB.angularVelocity = Vector3.zero;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_5 = __this->get_myRB_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_5, L_6, /*hidden argument*/NULL);
		// followingPlayer = true;
		__this->set_followingPlayer_15((bool)1);
	}

IL_0039:
	{
		// if (other.tag == "Wall")
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_7 = ___other0;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_8, _stringLiteral7649AEE062EE200D5810926162F39A75BCEE5287, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0051;
		}
	}
	{
		// RandomMovement();
		AU_ImposterController_RandomMovement_m11651730AC62838E8DD6BCDCACE0CC3F46F946FE(__this, /*hidden argument*/NULL);
	}

IL_0051:
	{
		// }
		return;
	}
}
// System.Void AU_ImposterController::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_OnTriggerExit_mF6F432A02655B303C3821A93013089E4AE416173 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Player")
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		// _transform.rotation = Quaternion.Slerp(transform.rotation, originalRotationValue, Time.time * 2f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get__transform_16();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_5;
		L_5 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_4, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = __this->get_originalRotationValue_13();
		float L_7;
		L_7 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_8;
		L_8 = Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8(L_5, L_6, ((float)il2cpp_codegen_multiply((float)L_7, (float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_3, L_8, /*hidden argument*/NULL);
		// followingPlayer = false;
		__this->set_followingPlayer_15((bool)0);
	}

IL_0045:
	{
		// }
		return;
	}
}
// System.Void AU_ImposterController::RandomMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_RandomMovement_m11651730AC62838E8DD6BCDCACE0CC3F46F946FE (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// timeLeft -= Time.deltaTime;
		float L_0 = __this->get_timeLeft_11();
		float L_1;
		L_1 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timeLeft_11(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// if (timeLeft <= -1)
		float L_2 = __this->get_timeLeft_11();
		if ((!(((float)L_2) <= ((float)(-1.0f)))))
		{
			goto IL_005b;
		}
	}
	{
		// movement = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		float L_3;
		L_3 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((-1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_5), L_3, L_4, /*hidden argument*/NULL);
		__this->set_movement_10(L_5);
		// timeLeft += accelerationTime;
		float L_6 = __this->get_timeLeft_11();
		float L_7 = __this->get_accelerationTime_8();
		__this->set_timeLeft_11(((float)il2cpp_codegen_add((float)L_6, (float)L_7)));
	}

IL_005b:
	{
		// if (movement.x != 0)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_8 = __this->get_address_of_movement_10();
		float L_9 = L_8->get_x_0();
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0097;
		}
	}
	{
		// myAvatar.localScale = new Vector2(Mathf.Sign(movement.x), 1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10 = __this->get_myAvatar_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_11 = __this->get_address_of_movement_10();
		float L_12 = L_11->get_x_0();
		float L_13;
		L_13 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_12, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_14), L_13, (1.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_10, L_15, /*hidden argument*/NULL);
	}

IL_0097:
	{
		// myAnim.SetFloat("Speed", movement.magnitude);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_16 = __this->get_myAnim_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_17 = __this->get_address_of_movement_10();
		float L_18;
		L_18 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_16, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ImposterController::KillPlayer(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_KillPlayer_mEB9F1A4AB3B3CD2162B7F145E72ABFCFE1A7C535 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// myAvatar.position = other.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_myAvatar_6();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_1 = ___other0;
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_3, /*hidden argument*/NULL);
		// eventsManagerController.PlayerKilledFunction();
		AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * L_4 = __this->get_eventsManagerController_4();
		NullCheck(L_4);
		AU_Events_ManagerController_PlayerKilledFunction_mF395AF572D904283AE7ADB5C206B7A804616247B(L_4, /*hidden argument*/NULL);
		// playerIsDead = true;
		__this->set_playerIsDead_12((bool)1);
		// RandomMovement();
		AU_ImposterController_RandomMovement_m11651730AC62838E8DD6BCDCACE0CC3F46F946FE(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ImposterController::StartBots()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_StartBots_m4944E0C252C3FB1603DE8F1CFF2D3C4D62B5F260 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	{
		// playerIsAnsweringTask = false;
		__this->set_playerIsAnsweringTask_18((bool)0);
		// }
		return;
	}
}
// System.Void AU_ImposterController::StopBots()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_StopBots_m2958FA36E8853ED3E58219663EA18E08BD2DB5D1 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	{
		// playerIsAnsweringTask = true;
		__this->set_playerIsAnsweringTask_18((bool)1);
		// }
		return;
	}
}
// System.Void AU_ImposterController::IncreaseSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController_IncreaseSpeed_mBC079CB210B20D530A1D10EEC029C45E371859B5 (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	{
		// _moveSpeed += 0.15f;
		float L_0 = __this->get__moveSpeed_17();
		__this->set__moveSpeed_17(((float)il2cpp_codegen_add((float)L_0, (float)(0.150000006f))));
		// }
		return;
	}
}
// System.Void AU_ImposterController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ImposterController__ctor_mA9A8EFDE55055ACC818C6EBF28EB3F4AEA9F902A (AU_ImposterController_t657D4C822DCCC607E894A26C7DD0E98DE42CA2F8 * __this, const RuntimeMethod* method)
{
	{
		// public float accelerationTime = 4f;
		__this->set_accelerationTime_8((4.0f));
		// public float maxSpeed = 5f;
		__this->set_maxSpeed_9((5.0f));
		// private float _moveSpeed = 3f;
		__this->set__moveSpeed_17((3.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_InteractableController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController_OnEnable_mC1C1E50C6218B02CA515D38DB1FEFB271266D9FF (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, const RuntimeMethod* method)
{
	{
		// highlight = transform.GetChild(0).gameObject;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_0, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_1, /*hidden argument*/NULL);
		__this->set_highlight_5(L_2);
		// }
		return;
	}
}
// System.Void AU_InteractableController::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController_OnTriggerEnter_m72F7FEA6A0317E3A5B55DAA4F06422695886F46D (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Player" && interactableIsActive)
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		bool L_3 = __this->get_interactableIsActive_7();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		// taskPanel.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_taskPanel_4();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// eventsController.StopBotsFunction();
		AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * L_5 = __this->get_eventsController_6();
		NullCheck(L_5);
		AU_Events_ManagerController_StopBotsFunction_mA0761ACD1FDE56D7F6BBCEFDBC13235504EBABA6(L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Void AU_InteractableController::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController_OnTriggerExit_m916CC2265525EE04FD3B1ABFFE8B6DDDDD1EF965 (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.tag == "Player" && interactableIsActive == true)
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		bool L_3 = __this->get_interactableIsActive_7();
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		// taskPanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_taskPanel_4();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0026:
	{
		// }
		return;
	}
}
// System.Void AU_InteractableController::SetInteractableAsActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController_SetInteractableAsActive_m14B7EEFE6F3B356A150EE9D2594FED89D5D369AD (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		// interactableIsActive = active;
		bool L_0 = ___active0;
		__this->set_interactableIsActive_7(L_0);
		// highlight.SetActive(active);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_highlight_5();
		bool L_2 = ___active0;
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_InteractableController::SetTaskToShow(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController_SetTaskToShow_m1A1ECAFB9D7479F16572921C0862C9308B57CEAC (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___taskPanelToShow0, const RuntimeMethod* method)
{
	{
		// taskPanel = taskPanelToShow;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___taskPanelToShow0;
		__this->set_taskPanel_4(L_0);
		// }
		return;
	}
}
// System.Void AU_InteractableController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_InteractableController__ctor_m43F69DA379F9F73BA48B8851F6BF24C63E8C8E2E (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_Multiple_Choice_TaskController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Multiple_Choice_TaskController_Start_m2EDE4A0EC65A9E5AD76DFC609C25BA954EB8D142 (AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA_m11D762F94106F90C52A14CE1D94F8480687FAEA1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// timerController = timerController.GetComponent<TimerController>();
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_0 = __this->get_timerController_8();
		NullCheck(L_0);
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_1;
		L_1 = Component_GetComponent_TisTimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA_m11D762F94106F90C52A14CE1D94F8480687FAEA1(L_0, /*hidden argument*/Component_GetComponent_TisTimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA_m11D762F94106F90C52A14CE1D94F8480687FAEA1_RuntimeMethod_var);
		__this->set_timerController_8(L_1);
		// GamePanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_GamePanel_4();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Multiple_Choice_TaskController::SetTaskDataInUI(System.String,System.String,System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Multiple_Choice_TaskController_SetTaskDataInUI_mB9C1E557ECC4C641307C56ADD748F2798201055D (AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430 * __this, String_t* ___questionText0, String_t* ___bodyText1, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___choices2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mCBD44A97260E33F58E2FBBD76E0432C8F120C30E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m6A1D5267B49F39503A0BF02B983C18FA2EB5094A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mC3C92BFD7725EFE888D9CD9022D7FD8B62797903_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// QuestionText.text = questionText;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_QuestionText_5();
		String_t* L_1 = ___questionText0;
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		// BodyText.text = bodyText;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_BodyText_6();
		String_t* L_3 = ___bodyText1;
		NullCheck(L_2);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		// int i = 0;
		V_0 = 0;
		// foreach (var choiceText in choices)
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_4 = ___choices2;
		NullCheck(L_4);
		Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B  L_5;
		L_5 = List_1_GetEnumerator_mC3C92BFD7725EFE888D9CD9022D7FD8B62797903(L_4, /*hidden argument*/List_1_GetEnumerator_mC3C92BFD7725EFE888D9CD9022D7FD8B62797903_RuntimeMethod_var);
		V_1 = L_5;
	}

IL_0021:
	try
	{// begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0023:
		{
			// foreach (var choiceText in choices)
			String_t* L_6;
			L_6 = Enumerator_get_Current_m6A1D5267B49F39503A0BF02B983C18FA2EB5094A_inline((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_1), /*hidden argument*/Enumerator_get_Current_m6A1D5267B49F39503A0BF02B983C18FA2EB5094A_RuntimeMethod_var);
			V_2 = L_6;
			// Buttons[i].GetComponentInChildren<Text>().text = choiceText;
			ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* L_7 = __this->get_Buttons_7();
			int32_t L_8 = V_0;
			NullCheck(L_7);
			int32_t L_9 = L_8;
			Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
			NullCheck(L_10);
			Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_11;
			L_11 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mCBD44A97260E33F58E2FBBD76E0432C8F120C30E(L_10, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mCBD44A97260E33F58E2FBBD76E0432C8F120C30E_RuntimeMethod_var);
			String_t* L_12 = V_2;
			NullCheck(L_11);
			VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_12);
			// i++;
			int32_t L_13 = V_0;
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		}

IL_0042:
		{
			// foreach (var choiceText in choices)
			bool L_14;
			L_14 = Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_1), /*hidden argument*/Enumerator_MoveNext_mCE70417061695048D84E473D50556E46B8630F54_RuntimeMethod_var);
			if (L_14)
			{
				goto IL_0023;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004d);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004d;
	}

FINALLY_004d:
	{// begin finally (depth: 1)
		Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7((Enumerator_tCDCE241581BD00D8EDB03C9DC4133A65ADABF67B *)(&V_1), /*hidden argument*/Enumerator_Dispose_m65A91D17CADA79F187F4D68980A9C8640B6C9FC7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(77)
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(77)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
	}

IL_005b:
	{
		// }
		return;
	}
}
// System.Void AU_Multiple_Choice_TaskController::ButtonPressed(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Multiple_Choice_TaskController_ButtonPressed_m4A422F792DFC76A960E1DB0589D74423609D9009 (AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430 * __this, int32_t ___button0, const RuntimeMethod* method)
{
	{
		// SetVisibilityPanel(false);
		AU_Multiple_Choice_TaskController_SetVisibilityPanel_m79EB6991B46DEB691805ACD980007923B64965D9(__this, (bool)0, /*hidden argument*/NULL);
		// eventsManagerController.TaskCompletedFunction();
		AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * L_0 = __this->get_eventsManagerController_10();
		NullCheck(L_0);
		AU_Events_ManagerController_TaskCompletedFunction_m3FE657508E043AD47F726D7072B74DDB28F7A8C5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Multiple_Choice_TaskController::SetVisibilityPanel(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Multiple_Choice_TaskController_SetVisibilityPanel_m79EB6991B46DEB691805ACD980007923B64965D9 (AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		// GamePanel.SetActive(active);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_GamePanel_4();
		bool L_1 = ___active0;
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Multiple_Choice_TaskController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Multiple_Choice_TaskController__ctor_mA3D7CC25B357FE2B9C02CD73E7533B9A76299666 (AU_Multiple_Choice_TaskController_tF6F7F9ADA447E4CEDA32FAAE15AB087FD6B5B430 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_PlayerController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_Awake_m345BCE0A4729CA5A2185D769C8D16E21617487D4 (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AU_PlayerController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_OnEnable_m00A3E46DE1B22F49331AD274EE622B6298062219 (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	{
		// WASD.Enable();
		InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * L_0 = __this->get_WASD_7();
		NullCheck(L_0);
		InputAction_Enable_m21D09D41AA886DD0888BF47AFDFB089E1A73A022(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_PlayerController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_OnDisable_m0FF4AC76EDFE4B8C46315750B657E87C770A735F (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	{
		// WASD.Disable();
		InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * L_0 = __this->get_WASD_7();
		NullCheck(L_0);
		InputAction_Disable_m44F211FC0ADED664EE732F097D3DDD2998348ED4(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_PlayerController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_Start_mB8DDD548ED35BA81A4117BC2215EBC1C08F54E85 (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// myRB = GetComponent<Rigidbody>();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0;
		L_0 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mB4A92A619135F9258670FB93AE08F229A41D0980_RuntimeMethod_var);
		__this->set_myRB_4(L_0);
		// myAvatar = transform.GetChild(0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_1, 0, /*hidden argument*/NULL);
		__this->set_myAvatar_5(L_2);
		// myAnim = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3;
		L_3 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_mF86B79BED8570481BDFC54EAEF1F692E0508BA38_RuntimeMethod_var);
		__this->set_myAnim_6(L_3);
		// }
		return;
	}
}
// System.Void AU_PlayerController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_Update_m65FE80358152F56FCF82812EF29FC5D2B828848B (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// movementInput = WASD.ReadValue<Vector2>();
		InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * L_0 = __this->get_WASD_7();
		NullCheck(L_0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C(L_0, /*hidden argument*/InputAction_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mDB7528F59BC31D671F7E6C31A74F327A1869FD7C_RuntimeMethod_var);
		__this->set_movementInput_8(L_1);
		// if (movementInput.x != 0)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_movementInput_8();
		float L_3 = L_2->get_x_0();
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_004d;
		}
	}
	{
		// myAvatar.localScale = new Vector2(Mathf.Sign(movementInput.x), 1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get_myAvatar_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_5 = __this->get_address_of_movementInput_8();
		float L_6 = L_5->get_x_0();
		float L_7;
		L_7 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_6, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), L_7, (1.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_4, L_9, /*hidden argument*/NULL);
	}

IL_004d:
	{
		// myAnim.SetFloat("Speed", movementInput.magnitude);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_10 = __this->get_myAnim_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_11 = __this->get_address_of_movementInput_8();
		float L_12;
		L_12 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_10, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_PlayerController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_FixedUpdate_m942D4DD16DE26C5411BE7ABDCDDE527C7A16E0AE (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	{
		// myRB.velocity = movementInput * movementSpeed;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0 = __this->get_myRB_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = __this->get_movementInput_8();
		float L_2 = __this->get_movementSpeed_9();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_1, L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_0, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_PlayerController::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_OnTriggerEnter_mA4B0B165E1B69920872BAEB569266563D7A755B5 (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AU_PlayerController::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_OnTriggerExit_m32B48AD467169CB1CE911B346C1C33D06E65312E (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AU_PlayerController::PlayerKilled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController_PlayerKilled_m0F5B6776778A653C340661A20AD8B803D8B4EB7C (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE8900DC91B51B8520DDF29118082DF79022F298);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isDead = true;
		__this->set_isDead_12((bool)1);
		// myAnim.SetBool("isDead", isDead);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get_myAnim_6();
		bool L_1 = __this->get_isDead_12();
		NullCheck(L_0);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_0, _stringLiteralAE8900DC91B51B8520DDF29118082DF79022F298, L_1, /*hidden argument*/NULL);
		// myCollider.enabled = false;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_2 = __this->get_myCollider_11();
		NullCheck(L_2);
		Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1(L_2, (bool)0, /*hidden argument*/NULL);
		// WASD.Disable();
		InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * L_3 = __this->get_WASD_7();
		NullCheck(L_3);
		InputAction_Disable_m44F211FC0ADED664EE732F097D3DDD2998348ED4(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_PlayerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_PlayerController__ctor_mA6753EFBA4B72FB22758D458BD6BC90F95363895 (AU_PlayerController_t4DE111FDB1245426EF9104138C5CA4BCBE1CC9C6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_ReportController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_Start_mD4B0A3B17CAEA183E868C67B71D872AB40AC21B1 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Color myColor = new Color();
		il2cpp_codegen_initobj((&V_0), sizeof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 ));
		// ColorUtility.TryParseHtmlString(colors[0], out myColor);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_colors_12();
		NullCheck(L_0);
		String_t* L_1;
		L_1 = List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_inline(L_0, 0, /*hidden argument*/List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		bool L_2;
		L_2 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(L_1, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_0), /*hidden argument*/NULL);
		// crewmate.color = myColor;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_3 = __this->get_crewmate_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4 = V_0;
		NullCheck(L_3);
		VirtualActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_3, L_4);
		// }
		return;
	}
}
// System.Void AU_ReportController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_Update_m11FD6588FA53DAC8BB55657FA27BEBC398F22760 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AU_ReportController::SubmitTaskInfo(System.Collections.Generic.List`1<System.String>,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_SubmitTaskInfo_m07D0AC4A03AD44782B7E09A14155A4DC145D1165 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___citations0, String_t* ___question1, int32_t ___correctAnswerNumber2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mAB159CED4EA42F6E700527382D795CCD31EF293F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m07C610356AB5C68760B3F1A16391671DCCD8FC89_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m809A2F3C03A4DFC84691D8615C8413FBC0039401_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m85FD3AD1E7C2C143E7D5491BBB781F1C3DA22B85_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass21_0_U3CSubmitTaskInfoU3Eb__0_m06BB4E9F1DF5E5B8AE81EB38B23A4075576B0BEB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1896B6FF3B9E74F32807076D8C351919AEB693FC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral64CFC92D261DA2B74CED8151D781601D05C3224F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralADE30E7D46F70F2DF18542CB2B831FC2271C5335);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB8CD08AB65F2F3D1C9BE01211D5F2BF4AC263690);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0A82FDE1B57568EAC7AE4186DC9B277D397A29C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDBED589A0467FDD14250BED4A3A8E475671181AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEDC146DE35595FFABFFF8DA1CBB936C9E75A305F);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * V_1 = NULL;
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * V_2 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_3;
	memset((&V_3), 0, sizeof(V_3));
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * V_5 = NULL;
	{
		// buttons = new List<Button>();
		List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * L_0 = (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E *)il2cpp_codegen_object_new(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_il2cpp_TypeInfo_var);
		List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795(L_0, /*hidden argument*/List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795_RuntimeMethod_var);
		__this->set_buttons_14(L_0);
		// numRightOption = correctAnswerNumber;
		int32_t L_1 = ___correctAnswerNumber2;
		__this->set_numRightOption_16(L_1);
		// descriptionText.text = citations[0];
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_descriptionText_8();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3 = ___citations0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_inline(L_3, 0, /*hidden argument*/List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		NullCheck(L_2);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		// questionText.text = question;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5 = __this->get_questionText_9();
		String_t* L_6 = ___question1;
		NullCheck(L_5);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		// citationsText = citations;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_7 = ___citations0;
		__this->set_citationsText_17(L_7);
		// colors = new List<string>() { "#66ae0f", "#628386", "#672b20", "#3e93fc", "#8142d4", "#4825d9", "#ce8d09" };
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_8 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_8, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_9 = L_8;
		NullCheck(L_9);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_9, _stringLiteralDBED589A0467FDD14250BED4A3A8E475671181AD, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_10 = L_9;
		NullCheck(L_10);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_10, _stringLiteral64CFC92D261DA2B74CED8151D781601D05C3224F, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_11 = L_10;
		NullCheck(L_11);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_11, _stringLiteralB8CD08AB65F2F3D1C9BE01211D5F2BF4AC263690, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_12 = L_11;
		NullCheck(L_12);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_12, _stringLiteralADE30E7D46F70F2DF18542CB2B831FC2271C5335, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_13 = L_12;
		NullCheck(L_13);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_13, _stringLiteralEDC146DE35595FFABFFF8DA1CBB936C9E75A305F, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_14 = L_13;
		NullCheck(L_14);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_14, _stringLiteral1896B6FF3B9E74F32807076D8C351919AEB693FC, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_15 = L_14;
		NullCheck(L_15);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_15, _stringLiteralC0A82FDE1B57568EAC7AE4186DC9B277D397A29C, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		__this->set_colors_12(L_15);
		// availableCrewmates = new List<Imposter>();
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_16 = (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 *)il2cpp_codegen_object_new(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_il2cpp_TypeInfo_var);
		List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C(L_16, /*hidden argument*/List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C_RuntimeMethod_var);
		__this->set_availableCrewmates_21(L_16);
		// numAttempts = 0;
		__this->set_numAttempts_19(0);
		// for (int i = 0; i < citations.Count; i++)
		V_0 = 0;
		goto IL_017d;
	}

IL_00a8:
	{
		U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * L_17 = (U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass21_0__ctor_m5A06A23E43E1C80F9D1A1F6A09ADE9B302DB567A(L_17, /*hidden argument*/NULL);
		V_1 = L_17;
		U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * L_18 = V_1;
		NullCheck(L_18);
		L_18->set_U3CU3E4__this_1(__this);
		// GameObject goButton = (GameObject)Instantiate(prefabButton);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_prefabButton_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20;
		L_20 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m85FD3AD1E7C2C143E7D5491BBB781F1C3DA22B85(L_19, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m85FD3AD1E7C2C143E7D5491BBB781F1C3DA22B85_RuntimeMethod_var);
		// goButton.transform.SetParent(ParentPanel, false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21 = L_20;
		NullCheck(L_21);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_21, /*hidden argument*/NULL);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_23 = __this->get_ParentPanel_7();
		NullCheck(L_22);
		Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A(L_22, L_23, (bool)0, /*hidden argument*/NULL);
		// goButton.transform.localScale = new Vector3(1, 1, 1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = L_21;
		NullCheck(L_24);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_24, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_26), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_25, L_26, /*hidden argument*/NULL);
		// Button tempButton = goButton.GetComponent<Button>();
		NullCheck(L_24);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_27;
		L_27 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mAB159CED4EA42F6E700527382D795CCD31EF293F(L_24, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mAB159CED4EA42F6E700527382D795CCD31EF293F_RuntimeMethod_var);
		V_2 = L_27;
		// int tempInt = i;
		U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * L_28 = V_1;
		int32_t L_29 = V_0;
		NullCheck(L_28);
		L_28->set_tempInt_0(L_29);
		// buttons.Add(tempButton);
		List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * L_30 = __this->get_buttons_14();
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_31 = V_2;
		NullCheck(L_30);
		List_1_Add_m07C610356AB5C68760B3F1A16391671DCCD8FC89(L_30, L_31, /*hidden argument*/List_1_Add_m07C610356AB5C68760B3F1A16391671DCCD8FC89_RuntimeMethod_var);
		// Color myColor = new Color();
		il2cpp_codegen_initobj((&V_3), sizeof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 ));
		// ColorUtility.TryParseHtmlString(colors[i], out myColor);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_32 = __this->get_colors_12();
		int32_t L_33 = V_0;
		NullCheck(L_32);
		String_t* L_34;
		L_34 = List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_inline(L_32, L_33, /*hidden argument*/List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		bool L_35;
		L_35 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(L_34, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_3), /*hidden argument*/NULL);
		// ColorBlock cb = tempButton.colors;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_36 = V_2;
		NullCheck(L_36);
		ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  L_37;
		L_37 = Selectable_get_colors_m47C712DD0CFA000DAACD750853E81E981C90B7D9_inline(L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		// cb.normalColor = myColor;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_38 = V_3;
		ColorBlock_set_normalColor_m32EB40A0BB6DD89B8816945EF43CFED8A1ED78B9_inline((ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 *)(&V_4), L_38, /*hidden argument*/NULL);
		// tempButton.colors = cb;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_39 = V_2;
		ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  L_40 = V_4;
		NullCheck(L_39);
		Selectable_set_colors_m2610F85E7DC191E0AA2D71E2447BA5B58B7C4621(L_39, L_40, /*hidden argument*/NULL);
		// tempButton.onClick.AddListener(() => ButtonClicked(tempInt));
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_41 = V_2;
		NullCheck(L_41);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_42;
		L_42 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_41, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * L_43 = V_1;
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_44 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_44, L_43, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass21_0_U3CSubmitTaskInfoU3Eb__0_m06BB4E9F1DF5E5B8AE81EB38B23A4075576B0BEB_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_42);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_42, L_44, /*hidden argument*/NULL);
		// Imposter imposter = new Imposter();
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_45 = (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 *)il2cpp_codegen_object_new(Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7_il2cpp_TypeInfo_var);
		Imposter__ctor_m96B7959C59C5943F69B87E90948B635EC8C8D93B(L_45, /*hidden argument*/NULL);
		V_5 = L_45;
		// imposter.Id = i;
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_46 = V_5;
		int32_t L_47 = V_0;
		NullCheck(L_46);
		Imposter_set_Id_mB6C591DEF175F5270ADE9BAF9F241EAB046B050D_inline(L_46, L_47, /*hidden argument*/NULL);
		// imposter.Available = true;
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_48 = V_5;
		NullCheck(L_48);
		Imposter_set_Available_m219488AE60B148B7A5635AFCD25630259A4D91D4_inline(L_48, (bool)1, /*hidden argument*/NULL);
		// availableCrewmates.Add(imposter);
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_49 = __this->get_availableCrewmates_21();
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_50 = V_5;
		NullCheck(L_49);
		List_1_Add_m809A2F3C03A4DFC84691D8615C8413FBC0039401(L_49, L_50, /*hidden argument*/List_1_Add_m809A2F3C03A4DFC84691D8615C8413FBC0039401_RuntimeMethod_var);
		// for (int i = 0; i < citations.Count; i++)
		int32_t L_51 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_017d:
	{
		// for (int i = 0; i < citations.Count; i++)
		int32_t L_52 = V_0;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_53 = ___citations0;
		NullCheck(L_53);
		int32_t L_54;
		L_54 = List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_inline(L_53, /*hidden argument*/List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_00a8;
		}
	}
	{
		// }
		return;
	}
}
// System.Void AU_ReportController::ChangeCrewmateLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_ChangeCrewmateLeft_m8EC3DBBC9F2B812711680415A6CB8A15E84A8AA7 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		// bool found = false;
		V_0 = (bool)0;
		// int nextCrewmate = 0;
		V_1 = 0;
	}

IL_0004:
	{
		// found = false;
		V_0 = (bool)0;
		// nextCrewmate = selectedCrewmate - 1;
		int32_t L_0 = __this->get_selectedCrewmate_13();
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1));
		// if (nextCrewmate == -1)
		int32_t L_1 = V_1;
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0021;
		}
	}
	{
		// nextCrewmate = availableCrewmates.Count - 1;
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_2 = __this->get_availableCrewmates_21();
		NullCheck(L_2);
		int32_t L_3;
		L_3 = List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_inline(L_2, /*hidden argument*/List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_RuntimeMethod_var);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1));
	}

IL_0021:
	{
		// if (availableCrewmates[nextCrewmate].Available)
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_4 = __this->get_availableCrewmates_21();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_6;
		L_6 = List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var);
		NullCheck(L_6);
		bool L_7;
		L_7 = Imposter_get_Available_mC21C8E2B2BB1A37CA7209A6EE8A1C3D016DDFEE1_inline(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		// found = true;
		V_0 = (bool)1;
	}

IL_0036:
	{
		// selectedCrewmate = nextCrewmate;
		int32_t L_8 = V_1;
		__this->set_selectedCrewmate_13(L_8);
		// } while (found == false);
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0004;
		}
	}
	{
		// ChangeImposterUI(nextCrewmate);
		int32_t L_10 = V_1;
		AU_ReportController_ChangeImposterUI_m01A395D663821382271B97FEEE3C77FE0DBFE4C7(__this, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ReportController::ChangeCrewmateRight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_ChangeCrewmateRight_mED7F7E1598559EC34976A3B4AD8431CBE2C6F5E7 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		// bool found = false;
		V_0 = (bool)0;
		// int nextCrewmate = 0;
		V_1 = 0;
	}

IL_0004:
	{
		// found = false;
		V_0 = (bool)0;
		// nextCrewmate = selectedCrewmate + 1;
		int32_t L_0 = __this->get_selectedCrewmate_13();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1));
		// if (nextCrewmate == availableCrewmates.Count)
		int32_t L_1 = V_1;
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_2 = __this->get_availableCrewmates_21();
		NullCheck(L_2);
		int32_t L_3;
		L_3 = List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_inline(L_2, /*hidden argument*/List_1_get_Count_m0BFA842344DEBBCC22360B679897D8C498C00790_RuntimeMethod_var);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_001f;
		}
	}
	{
		// nextCrewmate = 0;
		V_1 = 0;
	}

IL_001f:
	{
		// if (availableCrewmates[nextCrewmate].Available)
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_4 = __this->get_availableCrewmates_21();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_6;
		L_6 = List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var);
		NullCheck(L_6);
		bool L_7;
		L_7 = Imposter_get_Available_mC21C8E2B2BB1A37CA7209A6EE8A1C3D016DDFEE1_inline(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		// found = true;
		V_0 = (bool)1;
	}

IL_0034:
	{
		// selectedCrewmate = nextCrewmate;
		int32_t L_8 = V_1;
		__this->set_selectedCrewmate_13(L_8);
		// } while (found == false);
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0004;
		}
	}
	{
		// ChangeImposterUI(nextCrewmate);
		int32_t L_10 = V_1;
		AU_ReportController_ChangeImposterUI_m01A395D663821382271B97FEEE3C77FE0DBFE4C7(__this, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ReportController::ChangeImposterUI(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_ChangeImposterUI_m01A395D663821382271B97FEEE3C77FE0DBFE4C7 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, int32_t ___nextCrewmate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Color myColor = new Color();
		il2cpp_codegen_initobj((&V_0), sizeof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 ));
		// ColorUtility.TryParseHtmlString(colors[nextCrewmate], out myColor);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = __this->get_colors_12();
		int32_t L_1 = ___nextCrewmate0;
		NullCheck(L_0);
		String_t* L_2;
		L_2 = List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		bool L_3;
		L_3 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(L_2, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_0), /*hidden argument*/NULL);
		// descriptionText.text = citationsText[nextCrewmate];
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4 = __this->get_descriptionText_8();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_5 = __this->get_citationsText_17();
		int32_t L_6 = ___nextCrewmate0;
		NullCheck(L_5);
		String_t* L_7;
		L_7 = List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m3F624A80E7853289E4759DE8F16CF23B0958365B_RuntimeMethod_var);
		NullCheck(L_4);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_7);
		// crewmate.color = myColor;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_8 = __this->get_crewmate_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_9 = V_0;
		NullCheck(L_8);
		VirtualActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_9);
		// }
		return;
	}
}
// System.Void AU_ReportController::ButtonClicked(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_ButtonClicked_m3652675D81FFC11A06E6479BEFD08B94807386BE (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, int32_t ___numButton0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m77F12A63EB4FD22AA4D725B91DC135C20B32AE0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// if (numButton != numRightOption)
		int32_t L_0 = ___numButton0;
		int32_t L_1 = __this->get_numRightOption_16();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_004c;
		}
	}
	{
		// numAttempts++;
		int32_t L_2 = __this->get_numAttempts_19();
		__this->set_numAttempts_19(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1)));
		// availableCrewmates[numButton].Available = false;
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_3 = __this->get_availableCrewmates_21();
		int32_t L_4 = ___numButton0;
		NullCheck(L_3);
		Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * L_5;
		L_5 = List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_inline(L_3, L_4, /*hidden argument*/List_1_get_Item_mCF2CE994E4F10793A2859B39FFD72CD74C727314_RuntimeMethod_var);
		NullCheck(L_5);
		Imposter_set_Available_m219488AE60B148B7A5635AFCD25630259A4D91D4_inline(L_5, (bool)0, /*hidden argument*/NULL);
		// buttons[numButton].gameObject.SetActive(false);
		List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * L_6 = __this->get_buttons_14();
		int32_t L_7 = ___numButton0;
		NullCheck(L_6);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_8;
		L_8 = List_1_get_Item_m77F12A63EB4FD22AA4D725B91DC135C20B32AE0A_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m77F12A63EB4FD22AA4D725B91DC135C20B32AE0A_RuntimeMethod_var);
		NullCheck(L_8);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_9, (bool)0, /*hidden argument*/NULL);
		// eventsController.WrongChoiceSelectedFunction();
		AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * L_10 = __this->get_eventsController_5();
		NullCheck(L_10);
		AU_Events_ManagerController_WrongChoiceSelectedFunction_m7F64D8A07BBD19E74CF7D82C4B2688C0E2D786C1(L_10, /*hidden argument*/NULL);
		// }
		return;
	}

IL_004c:
	{
		// foreach (Transform child in ParentPanel)
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_11 = __this->get_ParentPanel_7();
		NullCheck(L_11);
		RuntimeObject* L_12;
		L_12 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0058:
	try
	{// begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_005a:
		{
			// foreach (Transform child in ParentPanel)
			RuntimeObject* L_13 = V_0;
			NullCheck(L_13);
			RuntimeObject * L_14;
			L_14 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_13);
			// Destroy(child.gameObject);
			NullCheck(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_14, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)));
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
			L_15 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_14, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_15, /*hidden argument*/NULL);
		}

IL_006f:
		{
			// foreach (Transform child in ParentPanel)
			RuntimeObject* L_16 = V_0;
			NullCheck(L_16);
			bool L_17;
			L_17 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_005a;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_0079);
		}
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{// begin finally (depth: 1)
		{
			RuntimeObject* L_18 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_18, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_19 = V_1;
			if (!L_19)
			{
				goto IL_0089;
			}
		}

IL_0083:
		{
			RuntimeObject* L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_20);
		}

IL_0089:
		{
			IL2CPP_END_FINALLY(121)
		}
	}// end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
	}

IL_008a:
	{
		// SetVisibilityPanel(false);
		AU_ReportController_SetVisibilityPanel_m01A5A08C5033F9EEAAD518F92AB72EBB7014CC80(__this, (bool)0, /*hidden argument*/NULL);
		// selectedOption = numButton;
		int32_t L_21 = ___numButton0;
		__this->set_selectedOption_18(L_21);
		// correctChoiceSelected = true;
		__this->set_correctChoiceSelected_20((bool)1);
		// taskAPIController.attemptsNeeded = numAttempts;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_22 = __this->get_taskAPIController_4();
		int32_t L_23 = __this->get_numAttempts_19();
		NullCheck(L_22);
		L_22->set_attemptsNeeded_17(L_23);
		// taskAPIController.selectedChoiceID += numButton;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_24 = __this->get_taskAPIController_4();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_25 = L_24;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_selectedChoiceID_18();
		int32_t L_27 = ___numButton0;
		NullCheck(L_25);
		L_25->set_selectedChoiceID_18(((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)L_27)));
		// eventsManagerController.TaskCompletedFunction();
		AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * L_28 = __this->get_eventsManagerController_15();
		NullCheck(L_28);
		AU_Events_ManagerController_TaskCompletedFunction_m3FE657508E043AD47F726D7072B74DDB28F7A8C5(L_28, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ReportController::SetVisibilityPanel(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController_SetVisibilityPanel_m01A5A08C5033F9EEAAD518F92AB72EBB7014CC80 (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, bool ___active0, const RuntimeMethod* method)
{
	{
		// GamePanel.SetActive(active);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_GamePanel_10();
		bool L_1 = ___active0;
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_ReportController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_ReportController__ctor_m223F59B911A2E2E86EE9DF34C86FDC342635C30F (AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1896B6FF3B9E74F32807076D8C351919AEB693FC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral64CFC92D261DA2B74CED8151D781601D05C3224F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralADE30E7D46F70F2DF18542CB2B831FC2271C5335);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB8CD08AB65F2F3D1C9BE01211D5F2BF4AC263690);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0A82FDE1B57568EAC7AE4186DC9B277D397A29C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDBED589A0467FDD14250BED4A3A8E475671181AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEDC146DE35595FFABFFF8DA1CBB936C9E75A305F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<string> colors = new List<string>() { "#66ae0f", "#628386", "#672b20", "#3e93fc", "#8142d4", "#4825d9", "#ce8d09" };
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_0 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_0, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_1 = L_0;
		NullCheck(L_1);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_1, _stringLiteralDBED589A0467FDD14250BED4A3A8E475671181AD, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_2 = L_1;
		NullCheck(L_2);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_2, _stringLiteral64CFC92D261DA2B74CED8151D781601D05C3224F, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_3 = L_2;
		NullCheck(L_3);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_3, _stringLiteralB8CD08AB65F2F3D1C9BE01211D5F2BF4AC263690, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_4 = L_3;
		NullCheck(L_4);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_4, _stringLiteralADE30E7D46F70F2DF18542CB2B831FC2271C5335, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_5 = L_4;
		NullCheck(L_5);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_5, _stringLiteralEDC146DE35595FFABFFF8DA1CBB936C9E75A305F, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_6 = L_5;
		NullCheck(L_6);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_6, _stringLiteral1896B6FF3B9E74F32807076D8C351919AEB693FC, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_7 = L_6;
		NullCheck(L_7);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_7, _stringLiteralC0A82FDE1B57568EAC7AE4186DC9B277D397A29C, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		__this->set_colors_12(L_7);
		// private List<Button> buttons = new List<Button>();
		List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * L_8 = (List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E *)il2cpp_codegen_object_new(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E_il2cpp_TypeInfo_var);
		List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795(L_8, /*hidden argument*/List_1__ctor_mA1744A70D10E32296ECF132BFF63A811CB50A795_RuntimeMethod_var);
		__this->set_buttons_14(L_8);
		// public List<string> citationsText = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_9 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_9, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_citationsText_17(L_9);
		// int selectedOption = -1;
		__this->set_selectedOption_18((-1));
		// int numAttempts = -1;
		__this->set_numAttempts_19((-1));
		// public List<Imposter> availableCrewmates = new List<Imposter>();
		List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 * L_10 = (List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67 *)il2cpp_codegen_object_new(List_1_t765B3D3E29A7A0FE7C32A68118CBF83DEA293B67_il2cpp_TypeInfo_var);
		List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C(L_10, /*hidden argument*/List_1__ctor_m9357AB0BD82DC6590C1EA2B3E7FD10F8E43CB01C_RuntimeMethod_var);
		__this->set_availableCrewmates_21(L_10);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_Task_APIController::SetNewTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SetNewTask_m8E43365A9441987808C31DCBA4042F71D3134E49 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method)
{
	{
		// StartCoroutine(GetImposterCitationSentimentTask());
		RuntimeObject* L_0;
		L_0 = AU_Task_APIController_GetImposterCitationSentimentTask_mCF122B6970729C70AEAE9A97BE63C61BE89DE02F(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_1;
		L_1 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		// SelectRandInteractable();
		AU_Task_APIController_SelectRandInteractable_m3FA47C631CCF634FB783918BC4BD8CFFAD6F4F23(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Task_APIController::SetClientToken(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SetClientToken_mD2B5F9E39080997D9B51E70645CEBC0021A0EA2F (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, String_t* ___token0, const RuntimeMethod* method)
{
	{
		// clientToken = token;
		String_t* L_0 = ___token0;
		__this->set_clientToken_19(L_0);
		// SetNewTask();
		AU_Task_APIController_SetNewTask_m8E43365A9441987808C31DCBA4042F71D3134E49(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator AU_Task_APIController::GetImposterCitationSentimentTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AU_Task_APIController_GetImposterCitationSentimentTask_mCF122B6970729C70AEAE9A97BE63C61BE89DE02F (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * L_0 = (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B *)il2cpp_codegen_object_new(U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B_il2cpp_TypeInfo_var);
		U3CGetImposterCitationSentimentTaskU3Ed__19__ctor_m0EF99F80F43393ECE3B0AD38F827BAAE3F3A9C4C(L_0, 0, /*hidden argument*/NULL);
		U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void AU_Task_APIController::SelectRandInteractable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SelectRandInteractable_m3FA47C631CCF634FB783918BC4BD8CFFAD6F4F23 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;

IL_0000:
	{
		// selected = UnityEngine.Random.Range(0, interactableControllers.Length);
		AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* L_0 = __this->get_interactableControllers_5();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))), /*hidden argument*/NULL);
		V_0 = L_1;
		// } while (selected == currentInteractable);
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_currentInteractable_11();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0000;
		}
	}
	{
		// currentInteractable = selected;
		int32_t L_4 = V_0;
		__this->set_currentInteractable_11(L_4);
		// }
		return;
	}
}
// System.Void AU_Task_APIController::SetReportPanelSettings(System.Collections.Generic.List`1<System.String>,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_SetReportPanelSettings_m1949E74AD77596BD8283CB0BC3AC1EE2DAFE1ACF (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___choices0, String_t* ___question1, int32_t ___correctAnswer2, const RuntimeMethod* method)
{
	{
		// interactableControllers[currentInteractable].SetInteractableAsActive(true);
		AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* L_0 = __this->get_interactableControllers_5();
		int32_t L_1 = __this->get_currentInteractable_11();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		AU_InteractableController_SetInteractableAsActive_m14B7EEFE6F3B356A150EE9D2594FED89D5D369AD(L_3, (bool)1, /*hidden argument*/NULL);
		// interactableControllers[currentInteractable].SetTaskToShow(multipleChoicePanel);
		AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* L_4 = __this->get_interactableControllers_5();
		int32_t L_5 = __this->get_currentInteractable_11();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_multipleChoicePanel_6();
		NullCheck(L_7);
		AU_InteractableController_SetTaskToShow_m1A1ECAFB9D7479F16572921C0862C9308B57CEAC_inline(L_7, L_8, /*hidden argument*/NULL);
		// reportController.SubmitTaskInfo(choices, question, correctAnswer);
		AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * L_9 = __this->get_reportController_7();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_10 = ___choices0;
		String_t* L_11 = ___question1;
		int32_t L_12 = ___correctAnswer2;
		NullCheck(L_9);
		AU_ReportController_SubmitTaskInfo_m07D0AC4A03AD44782B7E09A14155A4DC145D1165(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		// compassController.SetTarget(interactableObjects[currentInteractable]);
		AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * L_13 = __this->get_compassController_10();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_14 = __this->get_interactableObjects_4();
		int32_t L_15 = __this->get_currentInteractable_11();
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_13);
		AU_CompassController_SetTarget_m333CA468882EF3AD43388DFC1FF8EFF5257179C1_inline(L_13, L_17, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AU_Task_APIController::TaskCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController_TaskCompleted_m8643DBD4DE9B620CF03AC10349C8F9B25D5D9F60 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method)
{
	{
		// StartCoroutine(SendTask());
		RuntimeObject* L_0;
		L_0 = AU_Task_APIController_SendTask_m7C78F6BB0F623750BF2345C08279C4F256A9C401(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_1;
		L_1 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator AU_Task_APIController::SendTask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AU_Task_APIController_SendTask_m7C78F6BB0F623750BF2345C08279C4F256A9C401 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * L_0 = (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A *)il2cpp_codegen_object_new(U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A_il2cpp_TypeInfo_var);
		U3CSendTaskU3Ed__23__ctor_m8DE32D4A24776E5FC14967A368DFF03EA4D84AE6(L_0, 0, /*hidden argument*/NULL);
		U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void AU_Task_APIController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AU_Task_APIController__ctor_m7A1DD4955F1F40620E05964D4A634E3B0EDDFFC4 (AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4EFFFD13F5002C5417C2092510108CFA784C4B16);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9151D78C7350390CAFAC55F797FC835910057561);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string playerUsername = "davidrferreira";
		__this->set_playerUsername_14(_stringLiteral9151D78C7350390CAFAC55F797FC835910057561);
		// private int taskID = -1;
		__this->set_taskID_15((-1));
		// public int attemptsNeeded = -1;
		__this->set_attemptsNeeded_17((-1));
		// public int selectedChoiceID = -1;
		__this->set_selectedChoiceID_18((-1));
		// private readonly string baseURL = "https://reality.utad.net/scileague/api/gateway/";
		__this->set_baseURL_20(_stringLiteral4EFFFD13F5002C5417C2092510108CFA784C4B16);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MultipleChoiceRequest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultipleChoiceRequest__ctor_mEDE2D8308BB37F3210C7F4E4CDD9A78FA6BCDFFF (MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TimerController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController_Start_m192651C0602F061E20482E7080024B13E4DE330F (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3EC7D17D68A82AF5B0C004C2365B58852ECE2F06);
		s_Il2CppMethodInitialized = true;
	}
	{
		// timeCounter.text = "Time: 00:00.00";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_timeCounter_4();
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral3EC7D17D68A82AF5B0C004C2365B58852ECE2F06);
		// timerGoing = false;
		__this->set_timerGoing_6((bool)0);
		// BeginTimer();
		TimerController_BeginTimer_mAA181BB489DE0D7B29773360AC850C0208BF3202(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TimerController::BeginTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController_BeginTimer_mAA181BB489DE0D7B29773360AC850C0208BF3202 (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	{
		// timerGoing = true;
		__this->set_timerGoing_6((bool)1);
		// elapsedTime = 0f;
		__this->set_elapsedTime_7((0.0f));
		// StartCoroutine(UpdateTimer());
		RuntimeObject* L_0;
		L_0 = TimerController_UpdateTimer_m09B308F03DCC34431BA5C4231CC62E1983914F49(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_1;
		L_1 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TimerController::EndTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController_EndTimer_mE6EAC849759F442EE09DED70216DFFD7FFDE5538 (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	{
		// timerGoing = false;
		__this->set_timerGoing_6((bool)0);
		// }
		return;
	}
}
// System.Collections.IEnumerator TimerController::UpdateTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TimerController_UpdateTimer_m09B308F03DCC34431BA5C4231CC62E1983914F49 (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * L_0 = (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB *)il2cpp_codegen_object_new(U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB_il2cpp_TypeInfo_var);
		U3CUpdateTimerU3Ed__11__ctor_m1AF49DBD7A9D3A6EFACE8ADAB4AFD02B3A3B0E98(L_0, 0, /*hidden argument*/NULL);
		U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Int32 TimerController::GetTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TimerController_GetTime_mE28DDEDA2EBD0426B04639F9610EEFB695533992 (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	{
		// return timePlaying.Seconds + timePlaying.Minutes * 60;
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * L_0 = __this->get_address_of_timePlaying_5();
		int32_t L_1;
		L_1 = TimeSpan_get_Seconds_m3324F3A1F96CA956DAEDDB69DB32CAA320A053F7((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)L_0, /*hidden argument*/NULL);
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * L_2 = __this->get_address_of_timePlaying_5();
		int32_t L_3;
		L_3 = TimeSpan_get_Minutes_mF5A78108FEB64953C298CEC19637378380881202((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_3, (int32_t)((int32_t)60)))));
	}
}
// System.Void TimerController::GenerateBotAndImposter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController_GenerateBotAndImposter_mEBEF71D98829A65C13330DB1499566A993B4EA1B (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral627BB866689D36FF5337A5C9A7B4D5A35A72534C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral90A8DF8EB02BB70BC2CB29F3C32E3EA12510D67F);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		// bool accepted = false;
		V_0 = (bool)0;
		// int numRandom = 0;
		V_1 = 0;
		// if (timePlaying.Seconds == 30 || timePlaying.Seconds == 59)
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * L_0 = __this->get_address_of_timePlaying_5();
		int32_t L_1;
		L_1 = TimeSpan_get_Seconds_m3324F3A1F96CA956DAEDDB69DB32CAA320A053F7((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)30))))
		{
			goto IL_0025;
		}
	}
	{
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * L_2 = __this->get_address_of_timePlaying_5();
		int32_t L_3;
		L_3 = TimeSpan_get_Seconds_m3324F3A1F96CA956DAEDDB69DB32CAA320A053F7((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)59)))))
		{
			goto IL_00c6;
		}
	}

IL_0025:
	{
		// if (alreadySpawn != imposters.Length && spawned == false)
		int32_t L_4 = __this->get_alreadySpawn_10();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_5 = __this->get_imposters_9();
		NullCheck(L_5);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))))))
		{
			goto IL_00cd;
		}
	}
	{
		bool L_6 = __this->get_spawned_11();
		if (L_6)
		{
			goto IL_00cd;
		}
	}

IL_0043:
	{
		// numRandom = UnityEngine.Random.Range(0, imposters.Length - 1);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_7 = __this->get_imposters_9();
		NullCheck(L_7);
		int32_t L_8;
		L_8 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))), (int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_8;
		// Debug.Log("\nRandom: " + numRandom);
		String_t* L_9;
		L_9 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_10;
		L_10 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral627BB866689D36FF5337A5C9A7B4D5A35A72534C, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_10, /*hidden argument*/NULL);
		// if (!imposters[numRandom].activeSelf)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_11 = __this->get_imposters_9();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		bool L_15;
		L_15 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0098;
		}
	}
	{
		// Debug.Log("\nSpawn: " + numRandom);
		String_t* L_16;
		L_16 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		String_t* L_17;
		L_17 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral90A8DF8EB02BB70BC2CB29F3C32E3EA12510D67F, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_17, /*hidden argument*/NULL);
		// accepted = true;
		V_0 = (bool)1;
		// spawned = true;
		__this->set_spawned_11((bool)1);
	}

IL_0098:
	{
		// } while (accepted == false);
		bool L_18 = V_0;
		if (!L_18)
		{
			goto IL_0043;
		}
	}
	{
		// alreadySpawn++;
		int32_t L_19 = __this->get_alreadySpawn_10();
		__this->set_alreadySpawn_10(((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1)));
		// imposters[numRandom].SetActive(true);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_20 = __this->get_imposters_9();
		int32_t L_21 = V_1;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_23, (bool)1, /*hidden argument*/NULL);
		// bots[numRandom].SetActive(true);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_24 = __this->get_bots_8();
		int32_t L_25 = V_1;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_27, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00c6:
	{
		// spawned = false;
		__this->set_spawned_11((bool)0);
	}

IL_00cd:
	{
		// }
		return;
	}
}
// System.Void TimerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimerController__ctor_m1C44AEBF40C4F225ED192C258B42CB3BE16DCDAE (TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * __this, const RuntimeMethod* method)
{
	{
		// private int alreadySpawn = 1;
		__this->set_alreadySpawn_10(1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_ReportController/<>c__DisplayClass21_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass21_0__ctor_m5A06A23E43E1C80F9D1A1F6A09ADE9B302DB567A (U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AU_ReportController/<>c__DisplayClass21_0::<SubmitTaskInfo>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass21_0_U3CSubmitTaskInfoU3Eb__0_m06BB4E9F1DF5E5B8AE81EB38B23A4075576B0BEB (U3CU3Ec__DisplayClass21_0_tFE97BDEB8CAB880A3047E110DD602C2488AB2308 * __this, const RuntimeMethod* method)
{
	{
		// tempButton.onClick.AddListener(() => ButtonClicked(tempInt));
		AU_ReportController_t5E16ADDDAC16C78DDFF56606CD55C875FCC50073 * L_0 = __this->get_U3CU3E4__this_1();
		int32_t L_1 = __this->get_tempInt_0();
		NullCheck(L_0);
		AU_ReportController_ButtonClicked_m3652675D81FFC11A06E6479BEFD08B94807386BE(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 AU_ReportController/Imposter::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Imposter_get_Id_mA43F19BC6A746D136D91517E40650E17B122E8CF (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, const RuntimeMethod* method)
{
	{
		// public int Id { get; set; }
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void AU_ReportController/Imposter::set_Id(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Imposter_set_Id_mB6C591DEF175F5270ADE9BAF9F241EAB046B050D (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Id { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean AU_ReportController/Imposter::get_Available()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Imposter_get_Available_mC21C8E2B2BB1A37CA7209A6EE8A1C3D016DDFEE1 (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, const RuntimeMethod* method)
{
	{
		// public bool Available { get; set; }
		bool L_0 = __this->get_U3CAvailableU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void AU_ReportController/Imposter::set_Available(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Imposter_set_Available_m219488AE60B148B7A5635AFCD25630259A4D91D4 (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool Available { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CAvailableU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void AU_ReportController/Imposter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Imposter__ctor_m96B7959C59C5943F69B87E90948B635EC8C8D93B (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetImposterCitationSentimentTaskU3Ed__19__ctor_m0EF99F80F43393ECE3B0AD38F827BAAE3F3A9C4C (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetImposterCitationSentimentTaskU3Ed__19_System_IDisposable_Dispose_mA4C020C73B295C8B03B18ADF429AAF6EA877BB86 (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetImposterCitationSentimentTaskU3Ed__19_MoveNext_m1FB4F7DE9AF3343C7E1EE2A5E2B0ABF23EC311AB (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1D4C35F5A2200BC0FC64F8385F11A8183DCE826C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2293036C634A08D112127B21C69759506FF3E36E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5C70E315F7123B7B54B08D851EEFD2C450442E31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral67E5FF74526E1D92F88CA14F6FF0C12733893B44);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9669FB3E21BDE2C2BFC3E709CFA73EF5A97B3B2C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral996E5360F80E16B2189CC1E536C91CE68083F694);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C504D43C27CCB8D70AE2159F1DCAF7433D4FBF0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC15F9B9EBEDDDB20A27F1D25C7E06F7F21F3F96D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCDEBA5192C137E7688193AAC71C2B4DE462929A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEB5328652C8433B5660F353A2C264BE20A9E2B2C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF15E72E03C607420BD2E3D65E07F0C50155C1C30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1FFE25608DB132DAE3BB59C8C9FDD13CE5C0399);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * V_1 = NULL;
	String_t* V_2 = NULL;
	JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * V_3 = NULL;
	JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * V_7 = NULL;
	Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8  V_8;
	memset((&V_8), 0, sizeof(V_8));
	JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * V_9 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_002a;
			}
			case 1:
			{
				goto IL_0084;
			}
			case 2:
			{
				goto IL_014f;
			}
			case 3:
			{
				goto IL_01ff;
			}
			case 4:
			{
				goto IL_02d9;
			}
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// string finalURL = baseURL + "AskForAvailableMultiChoiceTask" + "?username=" + playerUsername;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_3 = V_1;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_baseURL_20();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = L_5->get_playerUsername_14();
		String_t* L_7;
		L_7 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_4, _stringLiteralCDEBA5192C137E7688193AAC71C2B4DE462929A3, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		// UnityWebRequest taskIDRequest = UnityWebRequest.Get(finalURL);
		String_t* L_8 = V_2;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_9;
		L_9 = UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A(L_8, /*hidden argument*/NULL);
		__this->set_U3CtaskIDRequestU3E5__2_3(L_9);
		// taskIDRequest.SetRequestHeader("Authorization", clientToken);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_10 = __this->get_U3CtaskIDRequestU3E5__2_3();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_11 = V_1;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_clientToken_19();
		NullCheck(L_10);
		UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB(L_10, _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907, L_12, /*hidden argument*/NULL);
		// yield return taskIDRequest.SendWebRequest();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_13 = __this->get_U3CtaskIDRequestU3E5__2_3();
		NullCheck(L_13);
		UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * L_14;
		L_14 = UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50(L_13, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_14);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0084:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (taskIDRequest.isNetworkError || taskIDRequest.isHttpError)
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_15 = __this->get_U3CtaskIDRequestU3E5__2_3();
		NullCheck(L_15);
		bool L_16;
		L_16 = UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00a5;
		}
	}
	{
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_17 = __this->get_U3CtaskIDRequestU3E5__2_3();
		NullCheck(L_17);
		bool L_18;
		L_18 = UnityWebRequest_get_isHttpError_m23ADCBE1CF082A940EF6AA75A15051583228B616(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00c1;
		}
	}

IL_00a5:
	{
		// Debug.LogError("taskIDRequest: " + taskIDRequest.error);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_19 = __this->get_U3CtaskIDRequestU3E5__2_3();
		NullCheck(L_19);
		String_t* L_20;
		L_20 = UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4(L_19, /*hidden argument*/NULL);
		String_t* L_21;
		L_21 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral2293036C634A08D112127B21C69759506FF3E36E, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_21, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_00c1:
	{
		// JSONNode task = JSON.Parse(taskIDRequest.downloadHandler.text);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_22 = __this->get_U3CtaskIDRequestU3E5__2_3();
		NullCheck(L_22);
		DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * L_23;
		L_23 = UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24;
		L_24 = DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A(L_23, /*hidden argument*/NULL);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_25;
		L_25 = JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		// int id = int.Parse(task["id"]);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_26 = V_3;
		NullCheck(L_26);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_27;
		L_27 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_26, _stringLiteral996E5360F80E16B2189CC1E536C91CE68083F694);
		IL2CPP_RUNTIME_CLASS_INIT(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var);
		String_t* L_28;
		L_28 = JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF(L_27, /*hidden argument*/NULL);
		int32_t L_29;
		L_29 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_28, /*hidden argument*/NULL);
		__this->set_U3CidU3E5__3_4(L_29);
		// finalURL = baseURL + "GetTaskPoints" + "?taskID=" + task["taskId"];
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_30 = V_1;
		NullCheck(L_30);
		String_t* L_31 = L_30->get_baseURL_20();
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_32 = V_3;
		NullCheck(L_32);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_33;
		L_33 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_32, _stringLiteralC15F9B9EBEDDDB20A27F1D25C7E06F7F21F3F96D);
		String_t* L_34;
		L_34 = JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF(L_33, /*hidden argument*/NULL);
		String_t* L_35;
		L_35 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_31, _stringLiteralF1FFE25608DB132DAE3BB59C8C9FDD13CE5C0399, L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		// UnityWebRequest taskPointsRequest = UnityWebRequest.Get(finalURL);
		String_t* L_36 = V_2;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_37;
		L_37 = UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A(L_36, /*hidden argument*/NULL);
		__this->set_U3CtaskPointsRequestU3E5__4_5(L_37);
		// taskPointsRequest.SetRequestHeader("Authorization", clientToken);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_38 = __this->get_U3CtaskPointsRequestU3E5__4_5();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_39 = V_1;
		NullCheck(L_39);
		String_t* L_40 = L_39->get_clientToken_19();
		NullCheck(L_38);
		UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB(L_38, _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907, L_40, /*hidden argument*/NULL);
		// yield return taskPointsRequest.SendWebRequest();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_41 = __this->get_U3CtaskPointsRequestU3E5__4_5();
		NullCheck(L_41);
		UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * L_42;
		L_42 = UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50(L_41, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_42);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_014f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (taskPointsRequest.isNetworkError || taskPointsRequest.isHttpError)
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_43 = __this->get_U3CtaskPointsRequestU3E5__4_5();
		NullCheck(L_43);
		bool L_44;
		L_44 = UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0170;
		}
	}
	{
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_45 = __this->get_U3CtaskPointsRequestU3E5__4_5();
		NullCheck(L_45);
		bool L_46;
		L_46 = UnityWebRequest_get_isHttpError_m23ADCBE1CF082A940EF6AA75A15051583228B616(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_018c;
		}
	}

IL_0170:
	{
		// Debug.LogError("taskIDRequest: " + taskPointsRequest.error);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_47 = __this->get_U3CtaskPointsRequestU3E5__4_5();
		NullCheck(L_47);
		String_t* L_48;
		L_48 = UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4(L_47, /*hidden argument*/NULL);
		String_t* L_49;
		L_49 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral2293036C634A08D112127B21C69759506FF3E36E, L_48, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_49, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_018c:
	{
		// points = int.Parse(taskPointsRequest.downloadHandler.text);
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_50 = V_1;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_51 = __this->get_U3CtaskPointsRequestU3E5__4_5();
		NullCheck(L_51);
		DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * L_52;
		L_52 = UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_53;
		L_53 = DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A(L_52, /*hidden argument*/NULL);
		int32_t L_54;
		L_54 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_53, /*hidden argument*/NULL);
		NullCheck(L_50);
		L_50->set_points_16(L_54);
		// finalURL = baseURL + "AskForMultiChoiceTaskMainBody" + "?taskID=" + id;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_55 = V_1;
		NullCheck(L_55);
		String_t* L_56 = L_55->get_baseURL_20();
		int32_t* L_57 = __this->get_address_of_U3CidU3E5__3_4();
		String_t* L_58;
		L_58 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_57, /*hidden argument*/NULL);
		String_t* L_59;
		L_59 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_56, _stringLiteral5C70E315F7123B7B54B08D851EEFD2C450442E31, L_58, /*hidden argument*/NULL);
		V_2 = L_59;
		// UnityWebRequest taskDetailsRequest = UnityWebRequest.Get(finalURL);
		String_t* L_60 = V_2;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_61;
		L_61 = UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A(L_60, /*hidden argument*/NULL);
		__this->set_U3CtaskDetailsRequestU3E5__5_6(L_61);
		// taskDetailsRequest.SetRequestHeader("Authorization", clientToken);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_62 = __this->get_U3CtaskDetailsRequestU3E5__5_6();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_63 = V_1;
		NullCheck(L_63);
		String_t* L_64 = L_63->get_clientToken_19();
		NullCheck(L_62);
		UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB(L_62, _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907, L_64, /*hidden argument*/NULL);
		// yield return taskDetailsRequest.SendWebRequest();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_65 = __this->get_U3CtaskDetailsRequestU3E5__5_6();
		NullCheck(L_65);
		UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * L_66;
		L_66 = UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50(L_65, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_66);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_01ff:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (taskDetailsRequest.isNetworkError || taskDetailsRequest.isHttpError)
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_67 = __this->get_U3CtaskDetailsRequestU3E5__5_6();
		NullCheck(L_67);
		bool L_68;
		L_68 = UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03(L_67, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_0220;
		}
	}
	{
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_69 = __this->get_U3CtaskDetailsRequestU3E5__5_6();
		NullCheck(L_69);
		bool L_70;
		L_70 = UnityWebRequest_get_isHttpError_m23ADCBE1CF082A940EF6AA75A15051583228B616(L_69, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_023c;
		}
	}

IL_0220:
	{
		// Debug.LogError("taskDetailsRequest: " + taskDetailsRequest.error);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_71 = __this->get_U3CtaskDetailsRequestU3E5__5_6();
		NullCheck(L_71);
		String_t* L_72;
		L_72 = UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4(L_71, /*hidden argument*/NULL);
		String_t* L_73;
		L_73 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral9C504D43C27CCB8D70AE2159F1DCAF7433D4FBF0, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_73, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_023c:
	{
		// JSONNode taskDetails = JSON.Parse(taskDetailsRequest.downloadHandler.text);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_74 = __this->get_U3CtaskDetailsRequestU3E5__5_6();
		NullCheck(L_74);
		DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * L_75;
		L_75 = UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		String_t* L_76;
		L_76 = DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A(L_75, /*hidden argument*/NULL);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_77;
		L_77 = JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D(L_76, /*hidden argument*/NULL);
		V_4 = L_77;
		// string question = taskDetails["question"];
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_78 = V_4;
		NullCheck(L_78);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_79;
		L_79 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_78, _stringLiteral9669FB3E21BDE2C2BFC3E709CFA73EF5A97B3B2C);
		IL2CPP_RUNTIME_CLASS_INIT(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var);
		String_t* L_80;
		L_80 = JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF(L_79, /*hidden argument*/NULL);
		__this->set_U3CquestionU3E5__6_7(L_80);
		// taskID = taskDetails["taskId"];
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_81 = V_1;
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_82 = V_4;
		NullCheck(L_82);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_83;
		L_83 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_82, _stringLiteralC15F9B9EBEDDDB20A27F1D25C7E06F7F21F3F96D);
		int32_t L_84;
		L_84 = JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50(L_83, /*hidden argument*/NULL);
		NullCheck(L_81);
		L_81->set_taskID_15(L_84);
		// finalURL = baseURL + "AskForMultiChoiceTaskChoices" + "?taskID=" + id;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_85 = V_1;
		NullCheck(L_85);
		String_t* L_86 = L_85->get_baseURL_20();
		int32_t* L_87 = __this->get_address_of_U3CidU3E5__3_4();
		String_t* L_88;
		L_88 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_87, /*hidden argument*/NULL);
		String_t* L_89;
		L_89 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_86, _stringLiteral1D4C35F5A2200BC0FC64F8385F11A8183DCE826C, L_88, /*hidden argument*/NULL);
		V_2 = L_89;
		// UnityWebRequest taskChoicesRequest = UnityWebRequest.Get(finalURL);
		String_t* L_90 = V_2;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_91;
		L_91 = UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A(L_90, /*hidden argument*/NULL);
		__this->set_U3CtaskChoicesRequestU3E5__7_8(L_91);
		// taskChoicesRequest.SetRequestHeader("Authorization", clientToken);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_92 = __this->get_U3CtaskChoicesRequestU3E5__7_8();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_93 = V_1;
		NullCheck(L_93);
		String_t* L_94 = L_93->get_clientToken_19();
		NullCheck(L_92);
		UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB(L_92, _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907, L_94, /*hidden argument*/NULL);
		// yield return taskChoicesRequest.SendWebRequest();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_95 = __this->get_U3CtaskChoicesRequestU3E5__7_8();
		NullCheck(L_95);
		UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * L_96;
		L_96 = UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50(L_95, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_96);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_02d9:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (taskChoicesRequest.isNetworkError || taskChoicesRequest.isHttpError)
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_97 = __this->get_U3CtaskChoicesRequestU3E5__7_8();
		NullCheck(L_97);
		bool L_98;
		L_98 = UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03(L_97, /*hidden argument*/NULL);
		if (L_98)
		{
			goto IL_02fa;
		}
	}
	{
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_99 = __this->get_U3CtaskChoicesRequestU3E5__7_8();
		NullCheck(L_99);
		bool L_100;
		L_100 = UnityWebRequest_get_isHttpError_m23ADCBE1CF082A940EF6AA75A15051583228B616(L_99, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_0316;
		}
	}

IL_02fa:
	{
		// Debug.LogError("taskChoicesRequest: " + taskChoicesRequest.error);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_101 = __this->get_U3CtaskChoicesRequestU3E5__7_8();
		NullCheck(L_101);
		String_t* L_102;
		L_102 = UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4(L_101, /*hidden argument*/NULL);
		String_t* L_103;
		L_103 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralEB5328652C8433B5660F353A2C264BE20A9E2B2C, L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_103, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_0316:
	{
		// JSONNode taskChoices = JSON.Parse(taskChoicesRequest.downloadHandler.text);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_104 = __this->get_U3CtaskChoicesRequestU3E5__7_8();
		NullCheck(L_104);
		DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * L_105;
		L_105 = UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		String_t* L_106;
		L_106 = DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A(L_105, /*hidden argument*/NULL);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_107;
		L_107 = JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D(L_106, /*hidden argument*/NULL);
		// int i = 0;
		V_5 = 0;
		// int numberCorrectAnswer = 0;
		V_6 = 0;
		// numChoices = 0;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_108 = V_1;
		NullCheck(L_108);
		L_108->set_numChoices_12(0);
		// List<string> choicesList = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_109 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_109, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		V_7 = L_109;
		// foreach (JSONNode choice in taskChoices)
		NullCheck(L_107);
		Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8  L_110;
		L_110 = VirtualFuncInvoker0< Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8  >::Invoke(28 /* SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNode::GetEnumerator() */, L_107);
		V_8 = L_110;
		goto IL_03ba;
	}

IL_0348:
	{
		// foreach (JSONNode choice in taskChoices)
		KeyValuePair_2_t7342D2F6B67041D137B9F2482D6A59A321C76B46  L_111;
		L_111 = Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9((Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8 *)(&V_8), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_112;
		L_112 = JSONNode_op_Implicit_m757755AFE21B579A47662DAA9BD32620FC7BC7F2(L_111, /*hidden argument*/NULL);
		V_9 = L_112;
		// choicesList.Add(choice["answer"]);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_113 = V_7;
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_114 = V_9;
		NullCheck(L_114);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_115;
		L_115 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_114, _stringLiteralF15E72E03C607420BD2E3D65E07F0C50155C1C30);
		String_t* L_116;
		L_116 = JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF(L_115, /*hidden argument*/NULL);
		NullCheck(L_113);
		List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A(L_113, L_116, /*hidden argument*/List_1_Add_mD8C90346622A7F72A072D319F0E2AE851BB5885A_RuntimeMethod_var);
		// numChoices++;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_117 = V_1;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_118 = V_1;
		NullCheck(L_118);
		int32_t L_119 = L_118->get_numChoices_12();
		NullCheck(L_117);
		L_117->set_numChoices_12(((int32_t)il2cpp_codegen_add((int32_t)L_119, (int32_t)1)));
		// if (i == 0)
		int32_t L_120 = V_5;
		if (L_120)
		{
			goto IL_0397;
		}
	}
	{
		// selectedChoiceID = choice["id"];
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_121 = V_1;
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_122 = V_9;
		NullCheck(L_122);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_123;
		L_123 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_122, _stringLiteral996E5360F80E16B2189CC1E536C91CE68083F694);
		IL2CPP_RUNTIME_CLASS_INIT(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var);
		int32_t L_124;
		L_124 = JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50(L_123, /*hidden argument*/NULL);
		NullCheck(L_121);
		L_121->set_selectedChoiceID_18(L_124);
	}

IL_0397:
	{
		// if (choice["isItTheCorrectAnswer"] == true)
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_125 = V_9;
		NullCheck(L_125);
		JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 * L_126;
		L_126 = VirtualFuncInvoker1< JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043 *, String_t* >::Invoke(7 /* SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String) */, L_125, _stringLiteral67E5FF74526E1D92F88CA14F6FF0C12733893B44);
		bool L_127 = ((bool)1);
		RuntimeObject * L_128 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_127);
		IL2CPP_RUNTIME_CLASS_INIT(JSONNode_t323405B8D0CBFDE18F9CA898D127407E729FF043_il2cpp_TypeInfo_var);
		bool L_129;
		L_129 = JSONNode_op_Equality_mEB349D8644B8E3F87CD33A35457A00480EFB329A(L_126, L_128, /*hidden argument*/NULL);
		if (!L_129)
		{
			goto IL_03b4;
		}
	}
	{
		// numberCorrectAnswer = i;
		int32_t L_130 = V_5;
		V_6 = L_130;
	}

IL_03b4:
	{
		// i++;
		int32_t L_131 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_131, (int32_t)1));
	}

IL_03ba:
	{
		// foreach (JSONNode choice in taskChoices)
		bool L_132;
		L_132 = Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E((Enumerator_tF163ECD99FD283D3ED0FCFE2FC9B43AC0EAEE5E8 *)(&V_8), /*hidden argument*/NULL);
		if (L_132)
		{
			goto IL_0348;
		}
	}
	{
		// SetReportPanelSettings(choicesList, question, numberCorrectAnswer);
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_133 = V_1;
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_134 = V_7;
		String_t* L_135 = __this->get_U3CquestionU3E5__6_7();
		int32_t L_136 = V_6;
		NullCheck(L_133);
		AU_Task_APIController_SetReportPanelSettings_m1949E74AD77596BD8283CB0BC3AC1EE2DAFE1ACF(L_133, L_134, L_135, L_136, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetImposterCitationSentimentTaskU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4632ED6BADD8CCD5A193A46569A83F26077723CA (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetImposterCitationSentimentTaskU3Ed__19_System_Collections_IEnumerator_Reset_m7069DA4DD0A91724FC51133EB53DC3B8908AB7BA (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetImposterCitationSentimentTaskU3Ed__19_System_Collections_IEnumerator_Reset_m7069DA4DD0A91724FC51133EB53DC3B8908AB7BA_RuntimeMethod_var)));
	}
}
// System.Object AU_Task_APIController/<GetImposterCitationSentimentTask>d__19::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CGetImposterCitationSentimentTaskU3Ed__19_System_Collections_IEnumerator_get_Current_m37537E7F74909AC0AEA9CB4BD25F0CDA660E113C (U3CGetImposterCitationSentimentTaskU3Ed__19_t860E8882C2798B0B5F676C5C79E1EE504BEF470B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AU_Task_APIController/<SendTask>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendTaskU3Ed__23__ctor_m8DE32D4A24776E5FC14967A368DFF03EA4D84AE6 (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void AU_Task_APIController/<SendTask>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendTaskU3Ed__23_System_IDisposable_Dispose_m53DF0E93B71098F6CE84903AC183E364AD2299BE (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean AU_Task_APIController/<SendTask>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSendTaskU3Ed__23_MoveNext_m0EA6C85E7971F664B9B0D8564EDA5AE501402524 (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2B4DA93E5AFF4264BB03A9D00D4D58669A0E137D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral64058CC688A96A90239811EF06C9D20DB0499C3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB07D84E310F5267DA77D2245B52128D405C8FF6F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEB5328652C8433B5660F353A2C264BE20A9E2B2C);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_017f;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// eventsController.StartBotsFunction();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_4 = V_1;
		NullCheck(L_4);
		AU_Events_ManagerController_tBC5D340CD58CB8C6CB868607C3558CB30454A8F4 * L_5 = L_4->get_eventsController_8();
		NullCheck(L_5);
		AU_Events_ManagerController_StartBotsFunction_m16FA216A0E86CB53E68C745F17E2154D77DEF9C6(L_5, /*hidden argument*/NULL);
		// interactableControllers[currentInteractable].SetInteractableAsActive(false);
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_6 = V_1;
		NullCheck(L_6);
		AU_InteractableControllerU5BU5D_t9439BE24B1DECCCBD109B728134ADDEBD9892570* L_7 = L_6->get_interactableControllers_5();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_currentInteractable_11();
		NullCheck(L_7);
		int32_t L_10 = L_9;
		AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		AU_InteractableController_SetInteractableAsActive_m14B7EEFE6F3B356A150EE9D2594FED89D5D369AD(L_11, (bool)0, /*hidden argument*/NULL);
		// points = Mathf.RoundToInt(points - (attemptsNeeded * (points / (numChoices + 1))));
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_12 = V_1;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_points_16();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_attemptsNeeded_17();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_17 = V_1;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_points_16();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_19 = V_1;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_numChoices_12();
		int32_t L_21;
		L_21 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_16, (int32_t)((int32_t)((int32_t)L_18/(int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)))))))))), /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_points_16(L_21);
		// totalPoints += points;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_22 = V_1;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_totalPoints_13();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_25 = V_1;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_points_16();
		NullCheck(L_22);
		L_22->set_totalPoints_13(((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)L_26)));
		// pointsText.text = totalPoints + " Points";
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_27 = V_1;
		NullCheck(L_27);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_28 = L_27->get_pointsText_9();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_29 = V_1;
		NullCheck(L_29);
		int32_t* L_30 = L_29->get_address_of_totalPoints_13();
		String_t* L_31;
		L_31 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_30, /*hidden argument*/NULL);
		String_t* L_32;
		L_32 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_31, _stringLiteral2B4DA93E5AFF4264BB03A9D00D4D58669A0E137D, /*hidden argument*/NULL);
		NullCheck(L_28);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_28, L_32);
		// MultipleChoiceRequest multipleChoiceRequest = new MultipleChoiceRequest();
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_33 = (MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 *)il2cpp_codegen_object_new(MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290_il2cpp_TypeInfo_var);
		MultipleChoiceRequest__ctor_mEDE2D8308BB37F3210C7F4E4CDD9A78FA6BCDFFF(L_33, /*hidden argument*/NULL);
		// multipleChoiceRequest.Username = playerUsername;
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_34 = L_33;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_35 = V_1;
		NullCheck(L_35);
		String_t* L_36 = L_35->get_playerUsername_14();
		NullCheck(L_34);
		L_34->set_Username_0(L_36);
		// multipleChoiceRequest.TaskID = taskID;
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_37 = L_34;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_38 = V_1;
		NullCheck(L_38);
		int32_t L_39 = L_38->get_taskID_15();
		NullCheck(L_37);
		L_37->set_TaskID_1(L_39);
		// multipleChoiceRequest.Points = points;
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_40 = L_37;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_41 = V_1;
		NullCheck(L_41);
		int32_t L_42 = L_41->get_points_16();
		NullCheck(L_40);
		L_40->set_Points_3(L_42);
		// multipleChoiceRequest.SelectedChoiceID = selectedChoiceID;
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_43 = L_40;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_44 = V_1;
		NullCheck(L_44);
		int32_t L_45 = L_44->get_selectedChoiceID_18();
		NullCheck(L_43);
		L_43->set_SelectedChoiceID_2(L_45);
		// multipleChoiceRequest.AttemptsNeeded = attemptsNeeded;
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_46 = L_43;
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_47 = V_1;
		NullCheck(L_47);
		int32_t L_48 = L_47->get_attemptsNeeded_17();
		NullCheck(L_46);
		L_46->set_AttemptsNeeded_4(L_48);
		// multipleChoiceRequest.CorrectChoiceSelected = true;
		MultipleChoiceRequest_tCC6315A1A8E7AA4EFE60C66FF37F300CB5B7C290 * L_49 = L_46;
		NullCheck(L_49);
		L_49->set_CorrectChoiceSelected_5((bool)1);
		// string finalURL = baseURL + "MultipleChoiceCompleted";
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_50 = V_1;
		NullCheck(L_50);
		String_t* L_51 = L_50->get_baseURL_20();
		String_t* L_52;
		L_52 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_51, _stringLiteralB07D84E310F5267DA77D2245B52128D405C8FF6F, /*hidden argument*/NULL);
		V_2 = L_52;
		// string json = JsonUtility.ToJson(multipleChoiceRequest);
		String_t* L_53;
		L_53 = JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C(L_49, /*hidden argument*/NULL);
		V_3 = L_53;
		// var request = new UnityWebRequest(finalURL, "POST");
		String_t* L_54 = V_2;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_55 = (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E *)il2cpp_codegen_object_new(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_mC2ED369A4ACE53AFF2E70A38BE95EB48D68D4975(L_55, L_54, _stringLiteral14E338D17C42E552FA7AF42CDAE40CA1F0E8A04D, /*hidden argument*/NULL);
		__this->set_U3CrequestU3E5__2_3(L_55);
		// byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
		Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * L_56;
		L_56 = Encoding_get_UTF8_mC877FB3137BBD566AEE7B15F9BF61DC4EF8F5E5E(/*hidden argument*/NULL);
		String_t* L_57 = V_3;
		NullCheck(L_56);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_58;
		L_58 = VirtualFuncInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, String_t* >::Invoke(15 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_56, L_57);
		V_4 = L_58;
		// request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_59 = __this->get_U3CrequestU3E5__2_3();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_60 = V_4;
		UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669 * L_61 = (UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669 *)il2cpp_codegen_object_new(UploadHandlerRaw_t398466F5905D0829DE2807D531A2419DA8B61669_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_mB46261D7AA64B605D5CA8FF9027A4A32E57A7BD9(L_61, L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		UnityWebRequest_set_uploadHandler_m8D5DF24FBE7F8F0DCF27E11CE3C6CF4363DF23BA(L_59, L_61, /*hidden argument*/NULL);
		// request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_62 = __this->get_U3CrequestU3E5__2_3();
		DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D * L_63 = (DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m01FD35970E4B4FC45FC99A648408F53A8B164774(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		UnityWebRequest_set_downloadHandler_m7496D2C5F755BEB68651A4F33EA9BDA319D092C2(L_62, L_63, /*hidden argument*/NULL);
		// request.SetRequestHeader("Content-Type", "application/json");
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_64 = __this->get_U3CrequestU3E5__2_3();
		NullCheck(L_64);
		UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB(L_64, _stringLiteral5B58EBE31E594BF8FA4BEA3CD075473149322B18, _stringLiteral64058CC688A96A90239811EF06C9D20DB0499C3E, /*hidden argument*/NULL);
		// request.SetRequestHeader("Authorization", clientToken);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_65 = __this->get_U3CrequestU3E5__2_3();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_66 = V_1;
		NullCheck(L_66);
		String_t* L_67 = L_66->get_clientToken_19();
		NullCheck(L_65);
		UnityWebRequest_SetRequestHeader_m5ED4EFBACC106390DF5D81D19E4D4D9D59F13EFB(L_65, _stringLiteral9D5A3AE3D2B0B5E5AF5AB489000D9B88FA11E907, L_67, /*hidden argument*/NULL);
		// yield return request.SendWebRequest();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_68 = __this->get_U3CrequestU3E5__2_3();
		NullCheck(L_68);
		UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * L_69;
		L_69 = UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50(L_68, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_69);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_017f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (request.isNetworkError || request.isHttpError)
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_70 = __this->get_U3CrequestU3E5__2_3();
		NullCheck(L_70);
		bool L_71;
		L_71 = UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03(L_70, /*hidden argument*/NULL);
		if (L_71)
		{
			goto IL_01a0;
		}
	}
	{
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_72 = __this->get_U3CrequestU3E5__2_3();
		NullCheck(L_72);
		bool L_73;
		L_73 = UnityWebRequest_get_isHttpError_m23ADCBE1CF082A940EF6AA75A15051583228B616(L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_01bc;
		}
	}

IL_01a0:
	{
		// Debug.LogError("taskChoicesRequest: " + request.error);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_74 = __this->get_U3CrequestU3E5__2_3();
		NullCheck(L_74);
		String_t* L_75;
		L_75 = UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4(L_74, /*hidden argument*/NULL);
		String_t* L_76;
		L_76 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralEB5328652C8433B5660F353A2C264BE20A9E2B2C, L_75, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_76, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_01bc:
	{
		// SetNewTask();
		AU_Task_APIController_t75F3D277144B43988D3FFECD032C3DF14B8CF62C * L_77 = V_1;
		NullCheck(L_77);
		AU_Task_APIController_SetNewTask_m8E43365A9441987808C31DCBA4042F71D3134E49(L_77, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object AU_Task_APIController/<SendTask>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSendTaskU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DB392F95155BA19FC1CFBE271E4229B2F233BEA (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void AU_Task_APIController/<SendTask>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSendTaskU3Ed__23_System_Collections_IEnumerator_Reset_m9630B623DDBD3BFEE709FFD4E1B3F61382167D55 (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSendTaskU3Ed__23_System_Collections_IEnumerator_Reset_m9630B623DDBD3BFEE709FFD4E1B3F61382167D55_RuntimeMethod_var)));
	}
}
// System.Object AU_Task_APIController/<SendTask>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSendTaskU3Ed__23_System_Collections_IEnumerator_get_Current_m08B2C3114C6F99BF5DE83770A60ED97927A8C2E5 (U3CSendTaskU3Ed__23_t1DFE603732D74ED5D34FCA908F5D747B74E4397A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TimerController/<UpdateTimer>d__11::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateTimerU3Ed__11__ctor_m1AF49DBD7A9D3A6EFACE8ADAB4AFD02B3A3B0E98 (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TimerController/<UpdateTimer>d__11::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateTimerU3Ed__11_System_IDisposable_Dispose_mB421A4D615A83F4DFA958F113D182D6951B17CE6 (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TimerController/<UpdateTimer>d__11::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CUpdateTimerU3Ed__11_MoveNext_m6D00E0088F5A485D2056C3446031F16A57466243 (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral299E01A3C227A338CCCF7D17E88F26B036E2B8EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88B3600712D8F762131B3FE13B12305C2B95E0BD);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0081;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_0088;
	}

IL_0020:
	{
		// elapsedTime += Time.deltaTime;
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_4 = V_1;
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_5 = V_1;
		NullCheck(L_5);
		float L_6 = L_5->get_elapsedTime_7();
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_elapsedTime_7(((float)il2cpp_codegen_add((float)L_6, (float)L_7)));
		// timePlaying = TimeSpan.FromSeconds(elapsedTime);
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_8 = V_1;
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_9 = V_1;
		NullCheck(L_9);
		float L_10 = L_9->get_elapsedTime_7();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_il2cpp_TypeInfo_var);
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_11;
		L_11 = TimeSpan_FromSeconds_m4644EABECA69BC6C07AD649C5898A8E53F4FE7B0(((double)((double)L_10)), /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_timePlaying_5(L_11);
		// string timePlayingStr = "Time: " + timePlaying.ToString("mm':'ss'.'ff");
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_12 = V_1;
		NullCheck(L_12);
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * L_13 = L_12->get_address_of_timePlaying_5();
		String_t* L_14;
		L_14 = TimeSpan_ToString_m7EA7DE3511D395B11A477C384FFFC40B40D3BA9F((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)L_13, _stringLiteral88B3600712D8F762131B3FE13B12305C2B95E0BD, /*hidden argument*/NULL);
		String_t* L_15;
		L_15 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral299E01A3C227A338CCCF7D17E88F26B036E2B8EC, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		// timeCounter.text = timePlayingStr;
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_16 = V_1;
		NullCheck(L_16);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_17 = L_16->get_timeCounter_4();
		String_t* L_18 = V_2;
		NullCheck(L_17);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_17, L_18);
		// GenerateBotAndImposter();
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_19 = V_1;
		NullCheck(L_19);
		TimerController_GenerateBotAndImposter_mEBEF71D98829A65C13330DB1499566A993B4EA1B(L_19, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0081:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0088:
	{
		// while(timerGoing)
		TimerController_t59138E77F95F150F126AF88B63680B6723DEFBFA * L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->get_timerGoing_6();
		if (L_21)
		{
			goto IL_0020;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object TimerController/<UpdateTimer>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateTimerU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E74A12B15F140BB7DB382BA51EA4FC626377BE1 (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TimerController/<UpdateTimer>d__11::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateTimerU3Ed__11_System_Collections_IEnumerator_Reset_m3A48CBE0E25B23CA678E3A24B8D4E29A699B263E (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CUpdateTimerU3Ed__11_System_Collections_IEnumerator_Reset_m3A48CBE0E25B23CA678E3A24B8D4E29A699B263E_RuntimeMethod_var)));
	}
}
// System.Object TimerController/<UpdateTimer>d__11::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateTimerU3Ed__11_System_Collections_IEnumerator_get_Current_m95D36EC4A6C71CEE60870735879056C1D3D2E52F (U3CUpdateTimerU3Ed__11_t3482F7DF04DB933BFEC0C7284B8FFF8EC39D1BEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_OnClick; }
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_0 = __this->get_m_OnClick_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  Selectable_get_colors_m47C712DD0CFA000DAACD750853E81E981C90B7D9_inline (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, const RuntimeMethod* method)
{
	{
		// public ColorBlock        colors            { get { return m_Colors; } set { if (SetPropertyUtility.SetStruct(ref m_Colors, value))            OnSetProperty(); } }
		ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  L_0 = __this->get_m_Colors_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ColorBlock_set_normalColor_m32EB40A0BB6DD89B8816945EF43CFED8A1ED78B9_inline (ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// public Color normalColor       { get { return m_NormalColor; } set { m_NormalColor = value; } }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = ___value0;
		__this->set_m_NormalColor_0(L_0);
		// public Color normalColor       { get { return m_NormalColor; } set { m_NormalColor = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Imposter_set_Id_mB6C591DEF175F5270ADE9BAF9F241EAB046B050D_inline (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Id { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Imposter_set_Available_m219488AE60B148B7A5635AFCD25630259A4D91D4_inline (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool Available { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CAvailableU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Imposter_get_Available_mC21C8E2B2BB1A37CA7209A6EE8A1C3D016DDFEE1_inline (Imposter_t645C570AD9BC8A08ECB19861A18B11C6FE6C8CE7 * __this, const RuntimeMethod* method)
{
	{
		// public bool Available { get; set; }
		bool L_0 = __this->get_U3CAvailableU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AU_InteractableController_SetTaskToShow_m1A1ECAFB9D7479F16572921C0862C9308B57CEAC_inline (AU_InteractableController_tAC9F5FE2CC1098B7DFA413EE878ED02B2ECDACB9 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___taskPanelToShow0, const RuntimeMethod* method)
{
	{
		// taskPanel = taskPanelToShow;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___taskPanelToShow0;
		__this->set_taskPanel_4(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AU_CompassController_SetTarget_m333CA468882EF3AD43388DFC1FF8EFF5257179C1_inline (AU_CompassController_t923DC00C7A2DB413BC9C4E1E8093693154F171B4 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___newTarget0, const RuntimeMethod* method)
{
	{
		// target = newTarget;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___newTarget0;
		__this->set_target_6(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ((EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C91D0E84532DF10C654917487D82CB0AB27693B_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
