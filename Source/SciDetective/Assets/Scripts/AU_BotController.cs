using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class AU_BotController : MonoBehaviour
{
    // Components
    Rigidbody myRB;
    Transform myAvatar;
    Animator myAnim;

    // Role
    [SerializeField] bool isImposter;

    // Communication
    public string dialog;

    // Movement
    public float accelerationTime = 4f;
    public float maxSpeed = 5f;
    private Vector2 movement;
    private float timeLeft;

    private bool playerIsAnsweringTask = false;

    // Start is called before the first frame update
    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        myAvatar = transform.GetChild(0);
        myAnim = GetComponent<Animator>();
        RandomMovement();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsAnsweringTask)
        {
            myRB.velocity = Vector3.zero;
            myRB.angularVelocity = Vector3.zero;
        }
        else
        {
            timeLeft -= Time.deltaTime;

            if (timeLeft <= -1)
            {
                RandomMovement();
                timeLeft += accelerationTime;
            }

            if (movement.x != 0)
            {
                myAvatar.localScale = new Vector2(Mathf.Sign(movement.x), 1);
            }
        }
        myAnim.SetFloat("Speed", movement.magnitude);
    }

    private void FixedUpdate()
    {
        if(!playerIsAnsweringTask)
        {
            myRB.velocity = movement * maxSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            RandomMovement();
        }
    }

    private void OnTriggerExit(Collider other)
    {
    }


    private void RandomMovement()
    {
        movement = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
    }

    public void StartBots()
    {
        playerIsAnsweringTask = false;
    }

    public void StopBots()
    {
        playerIsAnsweringTask = true;
    }

}
