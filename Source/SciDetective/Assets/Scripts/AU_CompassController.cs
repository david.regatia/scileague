using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AU_CompassController : MonoBehaviour
{
    public GameObject compass;
    public GameObject player;
    public GameObject target;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = target.transform.position - player.transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        compass.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 100);
    }

    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
    }
}
