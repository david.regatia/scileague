using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;
using System;
using System.Text;

public class AU_Task_APIController : MonoBehaviour
{

    public GameObject[] interactableObjects;
    public AU_InteractableController[] interactableControllers;
    public GameObject multipleChoicePanel;
    public AU_ReportController reportController;
    public AU_Events_ManagerController eventsController;
    public Text pointsText;

    public AU_CompassController compassController;

    private int currentInteractable;
    private int numChoices = 0;
    public int totalPoints = 0;

    // Information to pass to API.
    private string playerUsername = "davidrferreira";
    private int taskID = -1;
    public int points = 0;
    public int attemptsNeeded = -1;
    public int selectedChoiceID = -1;
    private string clientToken;

    private readonly string baseURL = "https://reality.utad.net/scileague/api/gateway/";


    public void SetNewTask()
    {
        StartCoroutine(GetImposterCitationSentimentTask());
        SelectRandInteractable();
    }

    public void SetClientToken(string token)
    {
        // TODO: Randomly chooses a type of task to ask the API.
        // At this moment only the Sentiment Analysis task is implemented.
        clientToken = token;
        SetNewTask();
    }

    IEnumerator GetImposterCitationSentimentTask()
    {
        // First, we'll ask for the id of an available task. 

        string finalURL = baseURL + "AskForAvailableMultiChoiceTask" + "?username=" + playerUsername;

        UnityWebRequest taskIDRequest = UnityWebRequest.Get(finalURL);
        taskIDRequest.SetRequestHeader("Authorization", clientToken);

        yield return taskIDRequest.SendWebRequest();

        if (taskIDRequest.isNetworkError || taskIDRequest.isHttpError)
        {
            Debug.LogError("taskIDRequest: " + taskIDRequest.error);
            yield break;
        }

        JSONNode task = JSON.Parse(taskIDRequest.downloadHandler.text);

        int id = int.Parse(task["id"]);


        // Secondly, we'll ask for the task's points.

        finalURL = baseURL + "GetTaskPoints" + "?taskID=" + task["taskId"];

        UnityWebRequest taskPointsRequest = UnityWebRequest.Get(finalURL);
        taskPointsRequest.SetRequestHeader("Authorization", clientToken);

        yield return taskPointsRequest.SendWebRequest();

        if (taskPointsRequest.isNetworkError || taskPointsRequest.isHttpError)
        {
            Debug.LogError("taskIDRequest: " + taskPointsRequest.error);
            yield break;
        }

        points = int.Parse(taskPointsRequest.downloadHandler.text);


        // Thirdly, we'll ask for the question associated to that task id.

        finalURL = baseURL + "AskForMultiChoiceTaskMainBody" + "?taskID=" + id;

        UnityWebRequest taskDetailsRequest = UnityWebRequest.Get(finalURL);
        taskDetailsRequest.SetRequestHeader("Authorization", clientToken);

        yield return taskDetailsRequest.SendWebRequest();

        if (taskDetailsRequest.isNetworkError || taskDetailsRequest.isHttpError)
        {
            Debug.LogError("taskDetailsRequest: " + taskDetailsRequest.error);
            yield break;
        }

        JSONNode taskDetails = JSON.Parse(taskDetailsRequest.downloadHandler.text);

     
        string question = taskDetails["question"];
        taskID = taskDetails["taskId"];


        // Lastly, we'll ask for the choices associated with that task.

        finalURL = baseURL + "AskForMultiChoiceTaskChoices" + "?taskID=" + id;

        UnityWebRequest taskChoicesRequest = UnityWebRequest.Get(finalURL);
        taskChoicesRequest.SetRequestHeader("Authorization", clientToken);

        yield return taskChoicesRequest.SendWebRequest();

        if (taskChoicesRequest.isNetworkError || taskChoicesRequest.isHttpError)
        {
            Debug.LogError("taskChoicesRequest: " + taskChoicesRequest.error);
            yield break;
        }

        JSONNode taskChoices = JSON.Parse(taskChoicesRequest.downloadHandler.text);

        int i = 0;
        int numberCorrectAnswer = 0;
        numChoices = 0;
        List<string> choicesList = new List<string>();

        foreach (JSONNode choice in taskChoices)
        {
            choicesList.Add(choice["answer"]);
            numChoices++;

            if (i == 0)
            {
                selectedChoiceID = choice["id"];
            }

            if (choice["isItTheCorrectAnswer"] == true)
            {
                numberCorrectAnswer = i;              
            }

            i++;
        }

        SetReportPanelSettings(choicesList, question, numberCorrectAnswer);
    }

    public void SelectRandInteractable()
    {
        int selected;

        do
        {
            selected = UnityEngine.Random.Range(0, interactableControllers.Length);
        } while (selected == currentInteractable);

        currentInteractable = selected;
    }



    public void SetReportPanelSettings(List<string> choices, string question, int correctAnswer)
    {
        interactableControllers[currentInteractable].SetInteractableAsActive(true);
        interactableControllers[currentInteractable].SetTaskToShow(multipleChoicePanel);
        reportController.SubmitTaskInfo(choices, question, correctAnswer);
        compassController.SetTarget(interactableObjects[currentInteractable]);
    }


    public void TaskCompleted()
    {
        StartCoroutine(SendTask());
    }


    IEnumerator SendTask()
    {
        eventsController.StartBotsFunction();
        interactableControllers[currentInteractable].SetInteractableAsActive(false);


        // Compute points.
        points = Mathf.RoundToInt(points - (attemptsNeeded * (points / (numChoices + 1))));
        totalPoints += points;
        pointsText.text = totalPoints + " Points"; 

        MultipleChoiceRequest multipleChoiceRequest = new MultipleChoiceRequest();
        multipleChoiceRequest.Username = playerUsername;
        multipleChoiceRequest.TaskID = taskID;
        multipleChoiceRequest.Points = points;
        multipleChoiceRequest.SelectedChoiceID = selectedChoiceID;
        multipleChoiceRequest.AttemptsNeeded = attemptsNeeded;
        multipleChoiceRequest.CorrectChoiceSelected = true;

        string finalURL = baseURL + "MultipleChoiceCompleted";
        string json = JsonUtility.ToJson(multipleChoiceRequest);


        var request = new UnityWebRequest(finalURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", clientToken);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError("taskChoicesRequest: " + request.error);
            yield break;
        }

        SetNewTask();
    }
    
}

[Serializable]
public class MultipleChoiceRequest
{
    [SerializeField] public string Username;
    [SerializeField] public int TaskID;
    [SerializeField] public int SelectedChoiceID;
    [SerializeField] public int Points;
    [SerializeField] public int AttemptsNeeded;
    [SerializeField] public bool CorrectChoiceSelected;
}

