using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class AU_PlayerController : MonoBehaviour
{
    // Components
    Rigidbody myRB;
    Transform myAvatar;
    Animator myAnim;

    // Player movement.
    [SerializeField] InputAction WASD;
    Vector2 movementInput;
    [SerializeField] float movementSpeed;

    AU_BotController target;
    [SerializeField] Collider myCollider;

    bool isDead;

    private void Awake()
    {
    }

    private void OnEnable()
    {
        WASD.Enable();
    }

    private void OnDisable()
    {
        WASD.Disable();
    }


    // Start is called before the first frame update
    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        myAvatar = transform.GetChild(0);
        myAnim = GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    { 
        movementInput = WASD.ReadValue<Vector2>();
        
        if (movementInput.x != 0)
        {
            myAvatar.localScale = new Vector2(Mathf.Sign(movementInput.x), 1);
        }

        myAnim.SetFloat("Speed", movementInput.magnitude);
    }


    private void FixedUpdate()
    {
        myRB.velocity = movementInput * movementSpeed;
    }


    private void OnTriggerEnter(Collider other)
    {

        //if (other.tag == "Player")
        //{
        //    AU_BotController tempTarget = other.GetComponent<AU_BotController>();
        //    target = tempTarget;
        //    interrogateButton.SetActive(true);
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        //if (other.tag == "Player")
        //{
        //    AU_BotController tempTarget = other.GetComponent<AU_BotController>();
        //    tempTarget.StopSpeaking();
        //    interrogateButton.SetActive(false);
        //}
    }

    public void PlayerKilled()
    {
        isDead = true;
        myAnim.SetBool("isDead", isDead);
        myCollider.enabled = false;
        WASD.Disable();
    }
}
