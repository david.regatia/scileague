using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AU_Game_ManagerController : MonoBehaviour
{
    public Button startButton;
    public GameObject startPanel;
    public GameObject exitGamePanel;
    public GameObject gameOverPanel;
    public Text gameOverPointsTextPanel;
    public AU_Task_APIController TasksManagerController;

    private void Awake()
    {
        Time.timeScale = 0f;
        startPanel.SetActive(true);

        // Ask Android to pass Client Token to Unity to handle API requests.
        using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                obj_Activity.Call("getClientToken");
            }
        }
    }

    private void OnEnable()
    {
        startButton.onClick.AddListener(StartGame);
    }

    private void OnDisable()
    {
        startButton.onClick.RemoveListener(StartGame);
    }

    private void StartGame()
    {
        Time.timeScale = 1f;

        // Hides the panel.
        startPanel.SetActive(false);
        //TasksManagerController.SetNewTask();
    }

    public void OpenExitGamePanel()
    {
        exitGamePanel.SetActive(true);
    }

    public void CloseExitGamePanel()
    {
        exitGamePanel.SetActive(false);
    }

    public void OpenGameOverPanel()
    {
        gameOverPointsTextPanel.text = "You received " + TasksManagerController.totalPoints + " points.";
        gameOverPanel.SetActive(true);
    }

    public void ContinueButtonPressedGameOver()
    {
        using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                obj_Activity.Call("exitGame");
            }
        }
    }

    public void ExitGame()
    {
        CloseExitGamePanel();
        OpenGameOverPanel();  
    }


    public void PlayerKilled()
    {
        OpenGameOverPanel();
    }
}
