using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    //public static TimerController instance;
    public Text timeCounter;
    private TimeSpan timePlaying;
    private bool timerGoing;
    private float elapsedTime;
    public GameObject[] bots;
    public GameObject[] imposters;
    private int alreadySpawn = 1;
    private bool spawned = false;

    void Start()
    {
        timeCounter.text = "Time: 00:00.00";
        timerGoing = false;
        BeginTimer();
    }

    public void BeginTimer()
    {
        timerGoing = true;
        elapsedTime = 0f;

        StartCoroutine(UpdateTimer());
    }

    public void EndTimer()
    {
        timerGoing = false;
    }


    private IEnumerator UpdateTimer()
    {
        while(timerGoing)
        {
            elapsedTime += Time.deltaTime;
            timePlaying = TimeSpan.FromSeconds(elapsedTime);
            string timePlayingStr = "Time: " + timePlaying.ToString("mm':'ss'.'ff");
            timeCounter.text = timePlayingStr;

            GenerateBotAndImposter();

            yield return null;
        }
    }

    public int GetTime()
    {
        return timePlaying.Seconds + timePlaying.Minutes * 60;
    }

    public void GenerateBotAndImposter()
    {
        bool accepted = false;
        int numRandom = 0;


        if (timePlaying.Seconds == 30 || timePlaying.Seconds == 59)
        {
            if (alreadySpawn != imposters.Length && spawned == false)
            {
                do
                {
                    numRandom = UnityEngine.Random.Range(0, imposters.Length - 1);

                    Debug.Log("\nRandom: " + numRandom);


                    if (!imposters[numRandom].activeSelf)
                    {
                        Debug.Log("\nSpawn: " + numRandom);
                        accepted = true;
                        spawned = true;
                    }

                } while (accepted == false);

                alreadySpawn++;
                imposters[numRandom].SetActive(true);
                bots[numRandom].SetActive(true);
            }
        }
        else
        {
            spawned = false;
        }




        //if (timePlaying.Seconds == 30)
        //{
        //    if (alreadySpawn == false)
        //    {
        //        Debug.Log("\nWill Spawn");

        //        do
        //        {
        //            numRandom = UnityEngine.Random.Range(0, imposters.Length);

        //            if (!imposters[numRandom].activeSelf)
        //            {
        //                Debug.Log("\nSpawn: " + numRandom);
        //                accepted = true;
        //            }

        //        } while (accepted == false);

        //        imposters[numRandom].SetActive(true);
        //        bots[numRandom].SetActive(true);
        //        alreadySpawn = true;
        //    }
        //}
        //else
        //{
        //    alreadySpawn = false;
        //}
    }
}
