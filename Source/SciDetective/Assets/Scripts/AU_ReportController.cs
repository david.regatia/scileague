using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;

public class AU_ReportController : MonoBehaviour
{
    public AU_Task_APIController taskAPIController;
    public AU_Events_ManagerController eventsController;


    [SerializeField] public GameObject prefabButton;
    [SerializeField] public RectTransform ParentPanel;
    [SerializeField] public Text descriptionText;
    [SerializeField] public Text questionText;
    public GameObject GamePanel;
    public Image crewmate;
    private List<string> colors = new List<string>() { "#66ae0f", "#628386", "#672b20", "#3e93fc", "#8142d4", "#4825d9", "#ce8d09" };
    private int selectedCrewmate = 0;
    private List<Button> buttons = new List<Button>();
    public AU_Events_ManagerController eventsManagerController;
    public int numRightOption = 0;
    public List<string> citationsText = new List<string>();

    int selectedOption = -1;
    int numAttempts = -1;
    bool correctChoiceSelected = false;

    public class Imposter
    {
        public int Id { get; set; }
        public bool Available { get; set; }
    }

    public List<Imposter> availableCrewmates = new List<Imposter>();


    // Start is called before the first frame update
    void Start()
    {
        Color myColor = new Color();
        ColorUtility.TryParseHtmlString(colors[0], out myColor);

        crewmate.color = myColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SubmitTaskInfo(List<string> citations, string question, int correctAnswerNumber)
    {
        buttons = new List<Button>();
        numRightOption = correctAnswerNumber;
        descriptionText.text = citations[0];
        questionText.text = question;
        citationsText = citations;
        colors = new List<string>() { "#66ae0f", "#628386", "#672b20", "#3e93fc", "#8142d4", "#4825d9", "#ce8d09" };
        availableCrewmates = new List<Imposter>();
        numAttempts = 0;

        for (int i = 0; i < citations.Count; i++)
        {
            GameObject goButton = (GameObject)Instantiate(prefabButton);
            goButton.transform.SetParent(ParentPanel, false);
            goButton.transform.localScale = new Vector3(1, 1, 1);

            Button tempButton = goButton.GetComponent<Button>();
            int tempInt = i;
            buttons.Add(tempButton);

            Color myColor = new Color();
            ColorUtility.TryParseHtmlString(colors[i], out myColor);

            ColorBlock cb = tempButton.colors;
            cb.normalColor = myColor;
            tempButton.colors = cb;

            tempButton.onClick.AddListener(() => ButtonClicked(tempInt));

            Imposter imposter = new Imposter();
            imposter.Id = i;
            imposter.Available = true;

            availableCrewmates.Add(imposter);
        }
    }



    public void ChangeCrewmateLeft()
    {
        bool found = false;
        int nextCrewmate = 0;

        do
        {
            found = false;
            nextCrewmate = selectedCrewmate - 1;

            if (nextCrewmate == -1)
            {
                nextCrewmate = availableCrewmates.Count - 1;
            }

            if (availableCrewmates[nextCrewmate].Available)
            {
                found = true;
            }

            selectedCrewmate = nextCrewmate;

        } while (found == false);

        ChangeImposterUI(nextCrewmate);
    }


    public void ChangeCrewmateRight()
    {
        bool found = false;
        int nextCrewmate = 0;

        do
        {
            found = false;
            nextCrewmate = selectedCrewmate + 1;

            if (nextCrewmate == availableCrewmates.Count)
            {
                nextCrewmate = 0;
            }

            if (availableCrewmates[nextCrewmate].Available)
            {
                found = true;
            }

            selectedCrewmate = nextCrewmate;

        } while (found == false);

        ChangeImposterUI(nextCrewmate);
    }

    private void ChangeImposterUI(int nextCrewmate)
    {
        Color myColor = new Color();
        ColorUtility.TryParseHtmlString(colors[nextCrewmate], out myColor);
        descriptionText.text = citationsText[nextCrewmate];

        crewmate.color = myColor;
    }


    void ButtonClicked(int numButton)
    {
        if (numButton != numRightOption)
        {
            numAttempts++;

            availableCrewmates[numButton].Available = false;
            buttons[numButton].gameObject.SetActive(false);
            eventsController.WrongChoiceSelectedFunction();
        }
        else
        {

            foreach (Transform child in ParentPanel)
                Destroy(child.gameObject);

            SetVisibilityPanel(false);

            selectedOption = numButton;
            correctChoiceSelected = true;

            taskAPIController.attemptsNeeded = numAttempts;
            taskAPIController.selectedChoiceID += numButton;

            // Throws event announcing that the task was completed.
            eventsManagerController.TaskCompletedFunction();
        }
    }


    public void SetVisibilityPanel(bool active)
    {
        GamePanel.SetActive(active);
    }
}
