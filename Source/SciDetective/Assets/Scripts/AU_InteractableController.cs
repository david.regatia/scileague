using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AU_InteractableController : MonoBehaviour
{
    [SerializeField] public GameObject taskPanel;
    [SerializeField] public GameObject highlight;
    public AU_Events_ManagerController eventsController;

    private bool interactableIsActive = false;

    [SerializeField] public GameObject activeMiniGame;

    private void OnEnable()
    {
        highlight = transform.GetChild(0).gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Opens panel when player enters interactable's collider.
        if (other.tag == "Player" && interactableIsActive)
        {
            taskPanel.SetActive(true);
            eventsController.StopBotsFunction();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Closes panel when player enters interactable's collider.
        if (other.tag == "Player" && interactableIsActive == true)
        {
            taskPanel.SetActive(false);
        }
    }

    public void SetInteractableAsActive(bool active)
    {
        interactableIsActive = active;
        highlight.SetActive(active);
    }

    public void SetTaskToShow(GameObject taskPanelToShow)
    {
        taskPanel = taskPanelToShow;
    }

}
