using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AU_ImposterController : MonoBehaviour
{

    public AU_Events_ManagerController eventsManagerController;

    // Components
    Rigidbody myRB;
    Transform myAvatar;
    Animator myAnim;

    // Movement
    public float accelerationTime = 4f;
    public float maxSpeed = 5f;
    private Vector2 movement;
    private float timeLeft;
    private bool playerIsDead = false;
    public Quaternion originalRotationValue;


    // Following
    public Transform _player;
    public bool followingPlayer = false;
    private Transform _transform;
    private float _moveSpeed = 3f;

    private bool playerIsAnsweringTask = false;


    private void Awake()
    {
        _transform = transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        myAvatar = transform.GetChild(0);
        myAnim = GetComponent<Animator>();
        originalRotationValue = transform.rotation;
        RandomMovement();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsAnsweringTask)
        {
            // If player is answering a task,
            // All bots' movement will be stopped.

            myRB.velocity = Vector3.zero;
            myRB.angularVelocity = Vector3.zero;
        }
        else if (Vector3.Distance(_transform.position, _player.position) < 1f)
        {
            // If imposter is very near of the player's position,
            // It is going to kill the player.

            KillPlayer(_player.GetComponent<Collider>());
            return;
        }
        else if (followingPlayer)
        {
            // If player is in the area,
            // The imposter is going to follow him.

            _moveSpeed = 2f;
            _transform.LookAt(_player.position);
            _transform.Rotate(new Vector3(0, -90, 0), Space.Self);
            _transform.Translate(new Vector3(_moveSpeed * Time.deltaTime, 0, 0));
            myAnim.SetFloat("Speed", movement.magnitude);
        }
        else
        {
            // Random movement if player is not near.

            RandomMovement();
        }
    }

    private void FixedUpdate()
    {
        if (!followingPlayer && !playerIsAnsweringTask)
        {
            myRB.velocity = movement * maxSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            myRB.velocity = Vector3.zero;
            myRB.angularVelocity = Vector3.zero;
            followingPlayer = true;
        }

        if (other.tag == "Wall")
        {
            RandomMovement();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _transform.rotation = Quaternion.Slerp(transform.rotation, originalRotationValue, Time.time * 2f);
            followingPlayer = false;
        }
    }


    private void RandomMovement()
    {
        timeLeft -= Time.deltaTime;

        if (timeLeft <= -1)
        {
            movement = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            timeLeft += accelerationTime;
        }

        if (movement.x != 0)
        {
            myAvatar.localScale = new Vector2(Mathf.Sign(movement.x), 1);
        }

        myAnim.SetFloat("Speed", movement.magnitude);
    }

    private void KillPlayer(Collider other)
    {
        myAvatar.position = other.transform.position;

        // Throws event indicating that the player was killed.
        eventsManagerController.PlayerKilledFunction();
        playerIsDead = true;

        RandomMovement();
    }

    public void StartBots()
    {
        playerIsAnsweringTask = false;

    }

    public void StopBots()
    {
        playerIsAnsweringTask = true;
    }

    public void IncreaseSpeed()
    {
        _moveSpeed += 0.15f;
    }
}
