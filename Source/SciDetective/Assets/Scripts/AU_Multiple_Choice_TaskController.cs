using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AU_Multiple_Choice_TaskController : MonoBehaviour
{
    [SerializeField] GameObject GamePanel;
    [SerializeField] Text QuestionText;
    [SerializeField] Text BodyText;
    [SerializeField] Button[] Buttons;

    public TimerController timerController;
    public GameObject timeObject;

    public AU_Events_ManagerController eventsManagerController;

    void Start()
    {
        timerController = timerController.GetComponent<TimerController>();
        GamePanel.SetActive(false);
    }

    public void SetTaskDataInUI(string questionText, string bodyText, List<string> choices)
    {
        // Updates UI components.

        QuestionText.text = questionText;
        BodyText.text = bodyText;

        int i = 0;


        foreach (var choiceText in choices)
        {
            Buttons[i].GetComponentInChildren<Text>().text = choiceText;
            i++;
        }
    }

    public void ButtonPressed(int button)
    {
        // TODO: Verify how much time has spent between the appearance of the task and the answer's selection. 

        // TODO: Verify if the answer given agrees with existing statics of previous answers. Give points if yes. 

        // Close menu.
        SetVisibilityPanel(false);

        // Throws event announcing that the task was completed.
        eventsManagerController.TaskCompletedFunction();
    }

    public void SetVisibilityPanel(bool active)
    {
        GamePanel.SetActive(active);
    }

}
