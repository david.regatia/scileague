using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AU_Events_ManagerController : MonoBehaviour
{
    public UnityEvent TaskCompleted;
    public UnityEvent PlayerKilled;
    public UnityEvent StopBots;
    public UnityEvent StartBots;
    public UnityEvent WrongChoiceSelected;

    public void TaskCompletedFunction()
    {
        TaskCompleted.Invoke();
    }

    public void PlayerKilledFunction()
    {
        PlayerKilled.Invoke();
    }

    public void StopBotsFunction()
    {
        StopBots.Invoke();
    }

    public void StartBotsFunction()
    {
        StartBots.Invoke();
    }

    public void WrongChoiceSelectedFunction()
    {
        WrongChoiceSelected.Invoke();
    }
}
