﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x00000002 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m68FCA7088A6690FB632145068B7E8B373A8DB4ED (void);
// 0x00000003 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m25F2BA097890C323C4B283AE4ED5995BB103F021 (void);
// 0x00000004 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mE4DA966D936ACB13C37763CDCE5EC2AF5729D205 (void);
// 0x00000005 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m29B408C6CAB5AA50B520C24A19563CE2C8E0F9DE (void);
// 0x00000006 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m9B82BB86D4FD6E7CC92BFB773E9459329816CE9C (void);
// 0x00000007 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_m831EF2FF049F37126F9915D07AD0408400B1C72A (void);
// 0x00000008 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m7AD24AA531038210738C18D8A25190B3BA76FD2D (void);
// 0x00000009 System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m3189BF7AD2A6D5D70797D383EDCF74ACEBBBCE5C (void);
// 0x0000000A System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mAA1EB2A7163F1F1CECB249CC01967F58C792D10F (void);
// 0x0000000B System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mAEAD2EF66A8E96031237FB0C24A3EB7678D9A67C (void);
// 0x0000000C System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_mA9E1B086C9708E0DBA88B8D46C12145F350DD103 (void);
// 0x0000000D System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m1B11D601CC7BCB5FCBF86ABD789B500240A63A67 (void);
// 0x0000000E System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mFC432CA1A043D3EE786300135BCEDB8F60F004D7 (void);
// 0x0000000F System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m2F25D4A17A00109F0FC4E333DD0992D8F8B3E3E9 (void);
// 0x00000010 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m929B6EBB35EA276CF12E8A90FFCA2A8B330342A0 (void);
// 0x00000011 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mDD2480A1E9A657DD80FACB63418B7D3EB928FC8C (void);
// 0x00000012 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m31E333F3FF2EA38F6E41E269B55CA1573F6BB85F (void);
// 0x00000013 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mF7243F4578DB464584758182E06AB93679B3783F (void);
// 0x00000014 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m7241A96E1AE22CAE87DE961567091BBFE1F5A360 (void);
// 0x00000015 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mF567CA468968F37D6A0BC5E8A30D5F2C4A0F8EB5 (void);
// 0x00000016 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m93AC18CC13651ADE3DB09B101FCB8E0B489B299F (void);
// 0x00000017 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_mF66414308628678DEA3A73F7B6841D82D99020CE (void);
// 0x00000018 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m6C2211311DCA4723345DF3832693184CB3E4554D (void);
// 0x00000019 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_mC000EFD570BBC997597C47F38395D1B780063CCC (void);
// 0x0000001A System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x0000001B SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x0000001C System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m90FBEF6424C5C7C4A0DD9489C0638F6905803DA5 (void);
// 0x0000001D SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_m4D9FD5091E68ADC0A6A621818E27CD2CD1DDE7C0 (void);
// 0x0000001E SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_mC225711E45284FEC77A340E3D7686B2F667EBE6A (void);
// 0x0000001F System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_mDB5FD20F057B328E4A8D2A6E0344D712EA5222CD (void);
// 0x00000020 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mC5255237373D9BFBB462959043D6A230D5E52642 (void);
// 0x00000021 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m3F0B6AC95E43D28183E6FF0E6680E8AEBFF5614D (void);
// 0x00000022 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m19378B69037D4A783749882479D66937E3942CE2 (void);
// 0x00000023 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_mA5E740EE81C49B0A2B3653DD2D91EAC4D0DA38B2 (void);
// 0x00000024 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m7389EDD5E4CAFCBF1B5FD2BC91E492736E0ECA65 (void);
// 0x00000025 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m468F905B830A77EDC94036AECAE06CCB2062D2F0 (void);
// 0x00000026 System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mAF476F53D7ED6FAE388588E1C1DD2B4EF980F5EE (void);
// 0x00000027 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_mD6FAE94576DA50BCAF8E3EEF81CAB62C89966D3F (void);
// 0x00000028 SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m96C974C159160E812077411341FFA273B3BD4216 (void);
// 0x00000029 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_mC6392F9282360F9ABD3AC734B18BF94C1FB7F107 (void);
// 0x0000002A System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF (void);
// 0x0000002B SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_mDB9E40DDE6449122804576F1F4FC2D1BD9FE9721 (void);
// 0x0000002C System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m96884B0F42B9282CF3B94DF70EA790591CE5D70A (void);
// 0x0000002D SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m15B5C6A8F83AEFCA8E2422039495626A6B5BA9AD (void);
// 0x0000002E System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m1C2AE3220662C5E56E16142EFD9391B7C50A318C (void);
// 0x0000002F SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m04F10331C13E2A8FC82650B6B15E1187B045F3A7 (void);
// 0x00000030 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50 (void);
// 0x00000031 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_m04174CC463B2FAAA3893DA0E1BC5F0EB0280C3B5 (void);
// 0x00000032 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m56F05EC7453355A68B50E4933297958F8D60701C (void);
// 0x00000033 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m757755AFE21B579A47662DAA9BD32620FC7BC7F2 (void);
// 0x00000034 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mEB349D8644B8E3F87CD33A35457A00480EFB329A (void);
// 0x00000035 System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m22851EA8F1ED30D4A1B977CD92AFEEB246DD098A (void);
// 0x00000036 System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mBFF7A822B266FD96F6A6B54433B3F9A00CE06AA0 (void);
// 0x00000037 System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mA0F3A3B708D8EAABFB5AAF1A3FE8EB86A5CC90BA (void);
// 0x00000038 System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m0ACB6810C49FC1E2246978F2E5C66F7344422458 (void);
// 0x00000039 System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m71E3F923CC34FB0961ACD615E516B61AB60BBE23 (void);
// 0x0000003A System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_m78C5E1232F0AA824915F86ACA6114BD584BB1F71 (void);
// 0x0000003B SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m69AEF21E17F84854BF203846254C8B943E9FEF21 (void);
// 0x0000003C System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_m1CBF4F8B6E1DECA2CC6252F7DE0BEF1D11A18A91 (void);
// 0x0000003D System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mD5545294D80A8A7EC2E166FDDF8C71A4EC3F09D0 (void);
// 0x0000003E System.Boolean SimpleJSON.JSONNode/Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mFA3BB4B7F43B1005535936519FD3A42F5B23981E (void);
// 0x0000003F System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m057901299D85978F34E86C2B99997599C904516F (void);
// 0x00000040 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m1DBE7DBD0628D4C87A31D88096548AF2917CC576 (void);
// 0x00000041 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::get_Current()
extern void Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9 (void);
// 0x00000042 System.Boolean SimpleJSON.JSONNode/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E (void);
// 0x00000043 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m9E8A702BF30F549C55B441B43EBBF823D1F8C3AF (void);
// 0x00000044 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_mD13470952E4791B2DC48D9A64AD1735248EC0AF2 (void);
// 0x00000045 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void ValueEnumerator__ctor_mCB3648E562A32EEA21FD3F4DB8C20160ED03722E (void);
// 0x00000046 SimpleJSON.JSONNode SimpleJSON.JSONNode/ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mF69BBD9003C127D62E50E3A2AB4179EA3863B710 (void);
// 0x00000047 System.Boolean SimpleJSON.JSONNode/ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mFBBE4BD2BD4BEDC52A09992FFB2BE22E61571C4B (void);
// 0x00000048 SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode/ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_mB4E3F0A6AC7AA6140B861775571A15036BECB254 (void);
// 0x00000049 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m13B6087FA46C7C03A3471C28B3FBC22DD4171FC7 (void);
// 0x0000004A System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mD48489EDADC6546248A26244922162520AD16C3B (void);
// 0x0000004B System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void KeyEnumerator__ctor_m0747B3AC824951C3C8B818496CEDD6D88B9E7E8A (void);
// 0x0000004C SimpleJSON.JSONNode SimpleJSON.JSONNode/KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m1E30F213CEB7C4567654D28C37E2841B4A81FAE0 (void);
// 0x0000004D System.Boolean SimpleJSON.JSONNode/KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m99B9BF2D4062F287B8D3A55C2C691537CAFE6F98 (void);
// 0x0000004E SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode/KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_m0243DD23679D98AE2F30AD67FFFD4E27A16AB170 (void);
// 0x0000004F System.Void SimpleJSON.JSONNode/LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m3B100A94EC3DF14B4A94AE12D32050F84E72F65B (void);
// 0x00000050 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m414843B6351637AAAF627AAD81880DE4BFD61353 (void);
// 0x00000051 System.Object SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m7F2D3716F0A2DAB0E37902FCC792A31BF9063758 (void);
// 0x00000052 System.Boolean SimpleJSON.JSONNode/LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mC600DAAECE627F2900FA1975C41E3D95F79E30F5 (void);
// 0x00000053 System.Void SimpleJSON.JSONNode/LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_m1E091AD169C66B9DA526BA37FFC6DC662ECE3272 (void);
// 0x00000054 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode/LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m8EE24F869680079C3280C231400F84085414E93E (void);
// 0x00000055 System.Void SimpleJSON.JSONNode/LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m5412BB4216090D65D10EE8E9E3D3006CAC45C43C (void);
// 0x00000056 System.Collections.IEnumerator SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m74AF45D293634ECACC82EE9CB8987984DB4E6968 (void);
// 0x00000057 System.Void SimpleJSON.JSONNode/<get_Children>d__39::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__39__ctor_m63F0D06333FBB49218C0B5014FECFBC5175BD0A5 (void);
// 0x00000058 System.Void SimpleJSON.JSONNode/<get_Children>d__39::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE120FE95B41F93A499A4D5DC467888E726B2F1E3 (void);
// 0x00000059 System.Boolean SimpleJSON.JSONNode/<get_Children>d__39::MoveNext()
extern void U3Cget_ChildrenU3Ed__39_MoveNext_m79E16CADBE6A9ADCE0E05EC107BBE0DCFA4FC44D (void);
// 0x0000005A SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5092BD25A20414E5F6EB32077296D953F0D581B6 (void);
// 0x0000005B System.Void SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m27A1C27A483D81AA0440A12DD10FE9AD9E3A6ACC (void);
// 0x0000005C System.Object SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_mAD0C2CEA294864931CA571CB5D7EFBCD0C670BE6 (void);
// 0x0000005D System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA647F5A5EEA340473000B2E0E7F4B82337322ED6 (void);
// 0x0000005E System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_m3B6F1A44882D66A956E6B435D261558891C87CE5 (void);
// 0x0000005F System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__41__ctor_m0C4C7559A4F8423F94519BECED7E7461F0C6C59C (void);
// 0x00000060 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mE9B558A92556F9BC9B1DAE775781708631B23773 (void);
// 0x00000061 System.Boolean SimpleJSON.JSONNode/<get_DeepChildren>d__41::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__41_MoveNext_mF6F808F7064BD1D12BE84B35B6DB06C667F5B202 (void);
// 0x00000062 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_mFBED011F823B343DF2B3F5C760644DD0C9F476BE (void);
// 0x00000063 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m2C4BE4DFF37F16458DDF888AED1FA19576F422CC (void);
// 0x00000064 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD96920D0B8440B39B9A5FB193E20F50C798CE42A (void);
// 0x00000065 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mC12AE3C207E5BE2C29D113F7524B3DF55C709B66 (void);
// 0x00000066 System.Object SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m8BB67D7F47460E168E96A9A294806D0A725D8B19 (void);
// 0x00000067 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBB66C6FD838719E9C3F687CE7E9E37C206DE6460 (void);
// 0x00000068 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m8F9A41F8EC3D989146B598EBFB8AF6F63FF9F014 (void);
// 0x00000069 System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mC00FE5764DBE022CB2A6BF50824C467CF01FCB01 (void);
// 0x0000006A System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_mCBDA88B1041B9DDFB22CF1F95D2C9AF533C11231 (void);
// 0x0000006B SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_mBA7D2708AFD83F55024FEE237CB4CC2B53ECFDDA (void);
// 0x0000006C System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m194C15DF5096C82E67284D79ECC9D52A783A5B21 (void);
// 0x0000006D SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m0B2C42AB0E7CD1C885EE73FFBC8A67964C58B18D (void);
// 0x0000006E SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mB91E086B47603F6EB9E27B95FCAD0E696B2D6F35 (void);
// 0x0000006F System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mF4A3E324D160BF0AD1427B88A2B47F700EB69696 (void);
// 0x00000070 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m411ED75FE2A99BEA55FE44AB9C844D971A4BC815 (void);
// 0x00000071 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mF61DBA24AB5A0323BDBA8F38F1C4504FC7A7D3E6 (void);
// 0x00000072 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m9468CAC9D288419DE8A7CA235B32EBC9FBF558B7 (void);
// 0x00000073 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_mFDEF8511E0C68436BDFEF07162C4F0AF4EAA2970 (void);
// 0x00000074 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mD3958262883D23A7E689E01AEF74DF589AD323FE (void);
// 0x00000075 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m7142B83C03C15E06352847A651143A30DDD53E28 (void);
// 0x00000076 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_mFF20142C5D77D438F218F14C2C7241E9083B15D8 (void);
// 0x00000077 System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m80DDB88D53978AE9F8BC8CB95D98F10B92E5D64B (void);
// 0x00000078 System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m5F3CE693516DF2278031527B3BEB830D430F78B1 (void);
// 0x00000079 System.Void SimpleJSON.JSONArray/<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_mFDAB07A9A4574778F9BE79CB24F5D25268081AEC (void);
// 0x0000007A System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m27D90B6AD18FAE9F1C3491048B45920A3221EC50 (void);
// 0x0000007B System.Boolean SimpleJSON.JSONArray/<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m71D465B34326827965E057A2D08CE6A07D4FB0B5 (void);
// 0x0000007C System.Void SimpleJSON.JSONArray/<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mE203663469D7E7DCC70B97ABC4E7DF17CE3AFD14 (void);
// 0x0000007D SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m31F5F2527DFDC39F0317D8236755CA263E0A6051 (void);
// 0x0000007E System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mEFE1CB55600EC3E065CBE2DA326B800C5A6B3DF7 (void);
// 0x0000007F System.Object SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m078E049917A635A9534AD2BBC91AE3F7C507F558 (void);
// 0x00000080 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF48300F880C2258127A0D59E244B611816290D87 (void);
// 0x00000081 System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m02F5A03E66FDD6D8FBDD6731827D6490F4A04B29 (void);
// 0x00000082 System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_m359D85BE012171E4E002285F20BC429E1A6BFA98 (void);
// 0x00000083 System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_m0AEACDF3C151A715C9F63679BDFAC03171D47810 (void);
// 0x00000084 SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m511E4A135B46CF2AFFB7509BBFBF6ABAF93603E7 (void);
// 0x00000085 System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m8DA1E1498537E1E1C66EB54EF30C3C60C6C0D3CB (void);
// 0x00000086 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m1AF735552661D6EEA2C54C3F190230835E407236 (void);
// 0x00000087 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_m1A6BE59A0A8BAB7FE28A6C0BC87FA4BE32D4018B (void);
// 0x00000088 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mB3A6227B0FEFE0A516EE7E2F694042FE168156EB (void);
// 0x00000089 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m76D60CDACB0991A590F72FF75D1828EA2B39F39F (void);
// 0x0000008A System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m3CB015E02CB932589E57C00F6C3E27322293B3A0 (void);
// 0x0000008B System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m19CC7B95F788DFF0CE6917F06CFDFE0062FBDB96 (void);
// 0x0000008C System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m0268BD87069B407DE0F601028CFBE6A52F22D594 (void);
// 0x0000008D SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_mE20DCDECF7FF984A389891EF2F08DC2088E7D8C6 (void);
// 0x0000008E SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_mD648238FD174CC258C46E70A36BDFDBE3CC53EE8 (void);
// 0x0000008F SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m0F1ECF30C2B6C5607AFD8C72E37E87FE5D572EDB (void);
// 0x00000090 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_mD36A022F70752842908E542DF8A88E827A43153C (void);
// 0x00000091 System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_mC9B657B6950EAD6BFADD62F0EFCFACC8E1E25DB4 (void);
// 0x00000092 System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_mDC880B59ED4D826E15F67A626326822D1CD1F52C (void);
// 0x00000093 System.Void SimpleJSON.JSONObject/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mE55EB0815ED94FB7DA4223EB2B1D83A792D49444 (void);
// 0x00000094 System.Boolean SimpleJSON.JSONObject/<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m32C80D793500A7AD9747C4B9BEF7E5902322DC8D (void);
// 0x00000095 System.Void SimpleJSON.JSONObject/<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_mAAFF58023C08EF32D6720ED98EAAEBDDEC41A5D3 (void);
// 0x00000096 System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m181A10417CF3D7FB11597D6BC8980F1041538D6C (void);
// 0x00000097 System.Boolean SimpleJSON.JSONObject/<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m73899F4BB42E92BEEDCC9E6DE93498B3E5FA12E8 (void);
// 0x00000098 System.Void SimpleJSON.JSONObject/<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m5675C62761DE690A974233757AFF7ACA2F6588C5 (void);
// 0x00000099 SimpleJSON.JSONNode SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mEE728FF36A84F0406C9961667F0CF83A1D076EF8 (void);
// 0x0000009A System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m1878F871DF3F1DECBC29D1EA12F302E7C7D913C0 (void);
// 0x0000009B System.Object SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_mEDC188741AB55EAA55CD475E71338FF617C4DB81 (void);
// 0x0000009C System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F30C3A17067EAEBE1F620E688233DDA9E058C00 (void);
// 0x0000009D System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m0435461165875637BB8E9DDD819E8C18BC824AA0 (void);
// 0x0000009E SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m7E09C62F10964D4D27854A83E0860A5AC985AFE3 (void);
// 0x0000009F System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_m5BF6795E2EC537F95E28F159B6760962D9F8CA4E (void);
// 0x000000A0 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m69464F52BB0DA089F154DCB55F5ED7302310CB9D (void);
// 0x000000A1 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m4731F37BD04F956DABFB67BA0C421211C6C97F59 (void);
// 0x000000A2 System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m2670EB54A9F28DF3F09476CA0AEF5A74F83EF45B (void);
// 0x000000A3 System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m7E8C037643D81531C83ED7103B3B2E2847454981 (void);
// 0x000000A4 System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mD04D1D9ECF6524AFE0BFFA950C5E28C6CCA58A81 (void);
// 0x000000A5 System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_mBF62E77E4EA113962F09D5EF5AAB519F0C5DBE96 (void);
// 0x000000A6 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m0D03E5BEB56AD43BDEFC55A105FB80C808C22017 (void);
// 0x000000A7 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m6B46BC7A9FAB14E69076796A4EC0EA692AE868F8 (void);
// 0x000000A8 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_m4EA2B0AAFDABFB616DC3F2F5BF2432E5FBC7DE1C (void);
// 0x000000A9 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m401DDA65F1DA380E495054994C9AABEAAD9E1DA3 (void);
// 0x000000AA System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_mF47E99756A3D325CF16BE8BFD6FF7148DFCAB81D (void);
// 0x000000AB System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_mA2DD9A59619C9FD69ED20EAD8EE931E4506589A1 (void);
// 0x000000AC System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m71742FA87FBA9CEC1B3CD7797BB12D940AA8C3D5 (void);
// 0x000000AD System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m01EF626FFB8BCB29DDDB4AB4B6A6FD4973C0395E (void);
// 0x000000AE System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_m540460B21D5ED37F9529F1734B24B6E868D432B8 (void);
// 0x000000AF System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_mAAF7F464DDCB064C83D2898FE50CCE90F77E4808 (void);
// 0x000000B0 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m918A4406F272535FBDEC2E67DA558DF0B3AB2264 (void);
// 0x000000B1 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mFAC1B74A6BD2705D6B2952D74DA6DF6D38C4F268 (void);
// 0x000000B2 System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mA8EBF84AAB284A4FF0E308F193B06A812E038A3F (void);
// 0x000000B3 System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_m886C693586191D3BE953E1E1CC7D5548B1E737D3 (void);
// 0x000000B4 SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mB9CA91991F00F4B74A0B8506DF446B77AF7872AB (void);
// 0x000000B5 System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m4C847D82501287231AD2E08CBB6CADA94D980599 (void);
// 0x000000B6 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mB707A772B8651C727BABE7657335A6EFCFD2D407 (void);
// 0x000000B7 System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_m7EDACE20FE016532F6714D1311EC851635DD4C53 (void);
// 0x000000B8 System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mE4AC78CF051938BCFBD45858D58A260709A210BC (void);
// 0x000000B9 System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_m2BB0201AE8B6118A20FCF2727F3F8D235505A552 (void);
// 0x000000BA System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m3D364B4168376B1F5312DCF0F4B771C278BF014F (void);
// 0x000000BB System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mBFA987A0D1492AFBC458BB89C88E7EC4AA2BE007 (void);
// 0x000000BC System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m9AAE9CA0B6044181BAF839B3CB43F9EB05DB2F8A (void);
// 0x000000BD System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m421FFE2D4A595460C5B0AA59C83E6EF5DECCC4E0 (void);
// 0x000000BE System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_mEEF0FFBE41AB3814546ABA212A53AA57517D84B1 (void);
// 0x000000BF System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m59F19469C64DA65D9B03D4BE6FBCBAAE2762C05E (void);
// 0x000000C0 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_mE2E06026E04958D2795026FBF38A47FBE14A7AEF (void);
// 0x000000C1 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m774EAA6C8365C47B27BDB3FAD8C28686A1105033 (void);
// 0x000000C2 SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m8CC0AC532BEF769781F75F62AC52009E154FB105 (void);
// 0x000000C3 System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m3B07A0182B1672877098FDC068A5ABA1AE336099 (void);
// 0x000000C4 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m4654CE7479238B4E8CB6219F9D859CBD7784AB18 (void);
// 0x000000C5 System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m553A353F6BB70066CDFC90AE29595B789CE3C0ED (void);
// 0x000000C6 System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mBBFC09F30AC48C4DF47BF6FCA1F271A6C6AD814A (void);
// 0x000000C7 System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m21B6CD1890428C491DBDC195ABD86C7A9B2CB520 (void);
// 0x000000C8 System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m9C680F040C87C7617AC04D1CCEC4DF04B3A062B1 (void);
// 0x000000C9 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m281EA55980AE7E71755B38A70914EDB521651CDE (void);
// 0x000000CA System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m11ACC117024154A6425EC0A92F92BE0850AB9AC8 (void);
// 0x000000CB System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_mA85B44E1E280A51A0E09A296D80CDF5735E8E4A6 (void);
// 0x000000CC System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_mFBF013316CC0BC72EEF846918C4C18C8EDD2C75A (void);
// 0x000000CD SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m3A6D074EB93F58B7070B64703D0ABCF7F75B1954 (void);
// 0x000000CE SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m1E8AD0743755E20E6E32B8B53643063DC5F3C13B (void);
// 0x000000CF System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m0B79861AA63A30F62B32C3A2F3A87F152C1015F6 (void);
// 0x000000D0 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_m6C390D7570282C672EB980117A97087DE5E763DF (void);
// 0x000000D1 System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_mC0B81E1660108B5070C659C1E3D6E8E715F5BA90 (void);
// 0x000000D2 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m5679E58AD4E2A877E1B0DFDEE1966C91F22F6873 (void);
// 0x000000D3 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mE8980B3490249E8442E0BBA0FFAF7A25579659FB (void);
// 0x000000D4 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m1E8CE199AA17884522F27832C69277BB00BABF07 (void);
// 0x000000D5 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m0D0E3BF1D133B3E0DA46F6A127350770F4837A91 (void);
// 0x000000D6 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m6AA4B279C70E27ED1B18AF98E92783CD97EA0889 (void);
// 0x000000D7 System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_mA541314CB35828119821DF9D32C06C02E103D097 (void);
// 0x000000D8 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mCB25438D736BAEC78588A1C203AE79D31064898C (void);
// 0x000000D9 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m02BA9367F2E9AB8C9BCD5669C6BA9DE076178218 (void);
// 0x000000DA System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m14E80E99AF8A7CCD7EB9991FFABF3DB01980894F (void);
// 0x000000DB System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m20B43AF6AED7123FA9CA49F2019A4B214F523DF9 (void);
// 0x000000DC System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m42A06E76316F1243A40AD6FD3C664575D8988758 (void);
// 0x000000DD System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_mD3942EF6D8F7D250F63D8358B626E3233E4E6D9A (void);
// 0x000000DE System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m89017807E84A7B04CD9B072858E4B2780928CECF (void);
// 0x000000DF System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m74C138C110E58F0F84C3E63844C9D10694D1BF9F (void);
// 0x000000E0 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_mBD1C53F2A1C5BEA7D0F16FC4427B361325D1DFF8 (void);
// 0x000000E1 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m490FE6CA5F4FB2BF477934745D8D184779BC746D (void);
// 0x000000E2 System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mF27B6C9FC704065CDB2B178AED35069212EDDFCC (void);
// 0x000000E3 System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m58268C31926C3B396083BF888747BC1F5E4BCBCB (void);
// 0x000000E4 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m7774E52E019503D6042D0AB8021466B5E617DFBC (void);
// 0x000000E5 SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m258758F521BE31F4BA01AD8E3D769761D320CFCC (void);
// 0x000000E6 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m95D334FA623EB56E6BD461C32EA5CD864662F05B (void);
// 0x000000E7 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D (void);
static Il2CppMethodPointer s_methodPointers[231] = 
{
	NULL,
	JSONNode_get_Item_m68FCA7088A6690FB632145068B7E8B373A8DB4ED,
	JSONNode_set_Item_m25F2BA097890C323C4B283AE4ED5995BB103F021,
	JSONNode_get_Item_mE4DA966D936ACB13C37763CDCE5EC2AF5729D205,
	JSONNode_set_Item_m29B408C6CAB5AA50B520C24A19563CE2C8E0F9DE,
	JSONNode_get_Value_m9B82BB86D4FD6E7CC92BFB773E9459329816CE9C,
	JSONNode_set_Value_m831EF2FF049F37126F9915D07AD0408400B1C72A,
	JSONNode_get_Count_m7AD24AA531038210738C18D8A25190B3BA76FD2D,
	JSONNode_get_IsNumber_m3189BF7AD2A6D5D70797D383EDCF74ACEBBBCE5C,
	JSONNode_get_IsString_mAA1EB2A7163F1F1CECB249CC01967F58C792D10F,
	JSONNode_get_IsBoolean_mAEAD2EF66A8E96031237FB0C24A3EB7678D9A67C,
	JSONNode_get_IsNull_mA9E1B086C9708E0DBA88B8D46C12145F350DD103,
	JSONNode_get_IsArray_m1B11D601CC7BCB5FCBF86ABD789B500240A63A67,
	JSONNode_get_IsObject_mFC432CA1A043D3EE786300135BCEDB8F60F004D7,
	JSONNode_get_Inline_m2F25D4A17A00109F0FC4E333DD0992D8F8B3E3E9,
	JSONNode_set_Inline_m929B6EBB35EA276CF12E8A90FFCA2A8B330342A0,
	JSONNode_Add_mDD2480A1E9A657DD80FACB63418B7D3EB928FC8C,
	JSONNode_Add_m31E333F3FF2EA38F6E41E269B55CA1573F6BB85F,
	JSONNode_Remove_mF7243F4578DB464584758182E06AB93679B3783F,
	JSONNode_Remove_m7241A96E1AE22CAE87DE961567091BBFE1F5A360,
	JSONNode_Remove_mF567CA468968F37D6A0BC5E8A30D5F2C4A0F8EB5,
	JSONNode_get_Children_m93AC18CC13651ADE3DB09B101FCB8E0B489B299F,
	JSONNode_get_DeepChildren_mF66414308628678DEA3A73F7B6841D82D99020CE,
	JSONNode_ToString_m6C2211311DCA4723345DF3832693184CB3E4554D,
	JSONNode_ToString_mC000EFD570BBC997597C47F38395D1B780063CCC,
	NULL,
	NULL,
	JSONNode_get_Linq_m90FBEF6424C5C7C4A0DD9489C0638F6905803DA5,
	JSONNode_get_Keys_m4D9FD5091E68ADC0A6A621818E27CD2CD1DDE7C0,
	JSONNode_get_Values_mC225711E45284FEC77A340E3D7686B2F667EBE6A,
	JSONNode_get_AsDouble_mDB5FD20F057B328E4A8D2A6E0344D712EA5222CD,
	JSONNode_set_AsDouble_mC5255237373D9BFBB462959043D6A230D5E52642,
	JSONNode_get_AsInt_m3F0B6AC95E43D28183E6FF0E6680E8AEBFF5614D,
	JSONNode_set_AsInt_m19378B69037D4A783749882479D66937E3942CE2,
	JSONNode_get_AsFloat_mA5E740EE81C49B0A2B3653DD2D91EAC4D0DA38B2,
	JSONNode_set_AsFloat_m7389EDD5E4CAFCBF1B5FD2BC91E492736E0ECA65,
	JSONNode_get_AsBool_m468F905B830A77EDC94036AECAE06CCB2062D2F0,
	JSONNode_set_AsBool_mAF476F53D7ED6FAE388588E1C1DD2B4EF980F5EE,
	JSONNode_get_AsArray_mD6FAE94576DA50BCAF8E3EEF81CAB62C89966D3F,
	JSONNode_get_AsObject_m96C974C159160E812077411341FFA273B3BD4216,
	JSONNode_op_Implicit_mC6392F9282360F9ABD3AC734B18BF94C1FB7F107,
	JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF,
	JSONNode_op_Implicit_mDB9E40DDE6449122804576F1F4FC2D1BD9FE9721,
	JSONNode_op_Implicit_m96884B0F42B9282CF3B94DF70EA790591CE5D70A,
	JSONNode_op_Implicit_m15B5C6A8F83AEFCA8E2422039495626A6B5BA9AD,
	JSONNode_op_Implicit_m1C2AE3220662C5E56E16142EFD9391B7C50A318C,
	JSONNode_op_Implicit_m04F10331C13E2A8FC82650B6B15E1187B045F3A7,
	JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50,
	JSONNode_op_Implicit_m04174CC463B2FAAA3893DA0E1BC5F0EB0280C3B5,
	JSONNode_op_Implicit_m56F05EC7453355A68B50E4933297958F8D60701C,
	JSONNode_op_Implicit_m757755AFE21B579A47662DAA9BD32620FC7BC7F2,
	JSONNode_op_Equality_mEB349D8644B8E3F87CD33A35457A00480EFB329A,
	JSONNode_op_Inequality_m22851EA8F1ED30D4A1B977CD92AFEEB246DD098A,
	JSONNode_Equals_mBFF7A822B266FD96F6A6B54433B3F9A00CE06AA0,
	JSONNode_GetHashCode_mA0F3A3B708D8EAABFB5AAF1A3FE8EB86A5CC90BA,
	JSONNode_get_EscapeBuilder_m0ACB6810C49FC1E2246978F2E5C66F7344422458,
	JSONNode_Escape_m71E3F923CC34FB0961ACD615E516B61AB60BBE23,
	JSONNode_ParseElement_m78C5E1232F0AA824915F86ACA6114BD584BB1F71,
	JSONNode_Parse_m69AEF21E17F84854BF203846254C8B943E9FEF21,
	JSONNode__ctor_m1CBF4F8B6E1DECA2CC6252F7DE0BEF1D11A18A91,
	JSONNode__cctor_mD5545294D80A8A7EC2E166FDDF8C71A4EC3F09D0,
	Enumerator_get_IsValid_mFA3BB4B7F43B1005535936519FD3A42F5B23981E,
	Enumerator__ctor_m057901299D85978F34E86C2B99997599C904516F,
	Enumerator__ctor_m1DBE7DBD0628D4C87A31D88096548AF2917CC576,
	Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9,
	Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E,
	ValueEnumerator__ctor_m9E8A702BF30F549C55B441B43EBBF823D1F8C3AF,
	ValueEnumerator__ctor_mD13470952E4791B2DC48D9A64AD1735248EC0AF2,
	ValueEnumerator__ctor_mCB3648E562A32EEA21FD3F4DB8C20160ED03722E,
	ValueEnumerator_get_Current_mF69BBD9003C127D62E50E3A2AB4179EA3863B710,
	ValueEnumerator_MoveNext_mFBBE4BD2BD4BEDC52A09992FFB2BE22E61571C4B,
	ValueEnumerator_GetEnumerator_mB4E3F0A6AC7AA6140B861775571A15036BECB254,
	KeyEnumerator__ctor_m13B6087FA46C7C03A3471C28B3FBC22DD4171FC7,
	KeyEnumerator__ctor_mD48489EDADC6546248A26244922162520AD16C3B,
	KeyEnumerator__ctor_m0747B3AC824951C3C8B818496CEDD6D88B9E7E8A,
	KeyEnumerator_get_Current_m1E30F213CEB7C4567654D28C37E2841B4A81FAE0,
	KeyEnumerator_MoveNext_m99B9BF2D4062F287B8D3A55C2C691537CAFE6F98,
	KeyEnumerator_GetEnumerator_m0243DD23679D98AE2F30AD67FFFD4E27A16AB170,
	LinqEnumerator__ctor_m3B100A94EC3DF14B4A94AE12D32050F84E72F65B,
	LinqEnumerator_get_Current_m414843B6351637AAAF627AAD81880DE4BFD61353,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m7F2D3716F0A2DAB0E37902FCC792A31BF9063758,
	LinqEnumerator_MoveNext_mC600DAAECE627F2900FA1975C41E3D95F79E30F5,
	LinqEnumerator_Dispose_m1E091AD169C66B9DA526BA37FFC6DC662ECE3272,
	LinqEnumerator_GetEnumerator_m8EE24F869680079C3280C231400F84085414E93E,
	LinqEnumerator_Reset_m5412BB4216090D65D10EE8E9E3D3006CAC45C43C,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m74AF45D293634ECACC82EE9CB8987984DB4E6968,
	U3Cget_ChildrenU3Ed__39__ctor_m63F0D06333FBB49218C0B5014FECFBC5175BD0A5,
	U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE120FE95B41F93A499A4D5DC467888E726B2F1E3,
	U3Cget_ChildrenU3Ed__39_MoveNext_m79E16CADBE6A9ADCE0E05EC107BBE0DCFA4FC44D,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5092BD25A20414E5F6EB32077296D953F0D581B6,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m27A1C27A483D81AA0440A12DD10FE9AD9E3A6ACC,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_mAD0C2CEA294864931CA571CB5D7EFBCD0C670BE6,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA647F5A5EEA340473000B2E0E7F4B82337322ED6,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_m3B6F1A44882D66A956E6B435D261558891C87CE5,
	U3Cget_DeepChildrenU3Ed__41__ctor_m0C4C7559A4F8423F94519BECED7E7461F0C6C59C,
	U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mE9B558A92556F9BC9B1DAE775781708631B23773,
	U3Cget_DeepChildrenU3Ed__41_MoveNext_mF6F808F7064BD1D12BE84B35B6DB06C667F5B202,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_mFBED011F823B343DF2B3F5C760644DD0C9F476BE,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m2C4BE4DFF37F16458DDF888AED1FA19576F422CC,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD96920D0B8440B39B9A5FB193E20F50C798CE42A,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mC12AE3C207E5BE2C29D113F7524B3DF55C709B66,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m8BB67D7F47460E168E96A9A294806D0A725D8B19,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBB66C6FD838719E9C3F687CE7E9E37C206DE6460,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m8F9A41F8EC3D989146B598EBFB8AF6F63FF9F014,
	JSONArray_get_Inline_mC00FE5764DBE022CB2A6BF50824C467CF01FCB01,
	JSONArray_set_Inline_mCBDA88B1041B9DDFB22CF1F95D2C9AF533C11231,
	JSONArray_get_Tag_mBA7D2708AFD83F55024FEE237CB4CC2B53ECFDDA,
	JSONArray_get_IsArray_m194C15DF5096C82E67284D79ECC9D52A783A5B21,
	JSONArray_GetEnumerator_m0B2C42AB0E7CD1C885EE73FFBC8A67964C58B18D,
	JSONArray_get_Item_mB91E086B47603F6EB9E27B95FCAD0E696B2D6F35,
	JSONArray_set_Item_mF4A3E324D160BF0AD1427B88A2B47F700EB69696,
	JSONArray_get_Item_m411ED75FE2A99BEA55FE44AB9C844D971A4BC815,
	JSONArray_set_Item_mF61DBA24AB5A0323BDBA8F38F1C4504FC7A7D3E6,
	JSONArray_get_Count_m9468CAC9D288419DE8A7CA235B32EBC9FBF558B7,
	JSONArray_Add_mFDEF8511E0C68436BDFEF07162C4F0AF4EAA2970,
	JSONArray_Remove_mD3958262883D23A7E689E01AEF74DF589AD323FE,
	JSONArray_Remove_m7142B83C03C15E06352847A651143A30DDD53E28,
	JSONArray_get_Children_mFF20142C5D77D438F218F14C2C7241E9083B15D8,
	JSONArray_WriteToStringBuilder_m80DDB88D53978AE9F8BC8CB95D98F10B92E5D64B,
	JSONArray__ctor_m5F3CE693516DF2278031527B3BEB830D430F78B1,
	U3Cget_ChildrenU3Ed__22__ctor_mFDAB07A9A4574778F9BE79CB24F5D25268081AEC,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m27D90B6AD18FAE9F1C3491048B45920A3221EC50,
	U3Cget_ChildrenU3Ed__22_MoveNext_m71D465B34326827965E057A2D08CE6A07D4FB0B5,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mE203663469D7E7DCC70B97ABC4E7DF17CE3AFD14,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m31F5F2527DFDC39F0317D8236755CA263E0A6051,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mEFE1CB55600EC3E065CBE2DA326B800C5A6B3DF7,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m078E049917A635A9534AD2BBC91AE3F7C507F558,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF48300F880C2258127A0D59E244B611816290D87,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m02F5A03E66FDD6D8FBDD6731827D6490F4A04B29,
	JSONObject_get_Inline_m359D85BE012171E4E002285F20BC429E1A6BFA98,
	JSONObject_set_Inline_m0AEACDF3C151A715C9F63679BDFAC03171D47810,
	JSONObject_get_Tag_m511E4A135B46CF2AFFB7509BBFBF6ABAF93603E7,
	JSONObject_get_IsObject_m8DA1E1498537E1E1C66EB54EF30C3C60C6C0D3CB,
	JSONObject_GetEnumerator_m1AF735552661D6EEA2C54C3F190230835E407236,
	JSONObject_get_Item_m1A6BE59A0A8BAB7FE28A6C0BC87FA4BE32D4018B,
	JSONObject_set_Item_mB3A6227B0FEFE0A516EE7E2F694042FE168156EB,
	JSONObject_get_Item_m76D60CDACB0991A590F72FF75D1828EA2B39F39F,
	JSONObject_set_Item_m3CB015E02CB932589E57C00F6C3E27322293B3A0,
	JSONObject_get_Count_m19CC7B95F788DFF0CE6917F06CFDFE0062FBDB96,
	JSONObject_Add_m0268BD87069B407DE0F601028CFBE6A52F22D594,
	JSONObject_Remove_mE20DCDECF7FF984A389891EF2F08DC2088E7D8C6,
	JSONObject_Remove_mD648238FD174CC258C46E70A36BDFDBE3CC53EE8,
	JSONObject_Remove_m0F1ECF30C2B6C5607AFD8C72E37E87FE5D572EDB,
	JSONObject_get_Children_mD36A022F70752842908E542DF8A88E827A43153C,
	JSONObject_WriteToStringBuilder_mC9B657B6950EAD6BFADD62F0EFCFACC8E1E25DB4,
	JSONObject__ctor_mDC880B59ED4D826E15F67A626326822D1CD1F52C,
	U3CU3Ec__DisplayClass21_0__ctor_mE55EB0815ED94FB7DA4223EB2B1D83A792D49444,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m32C80D793500A7AD9747C4B9BEF7E5902322DC8D,
	U3Cget_ChildrenU3Ed__23__ctor_mAAFF58023C08EF32D6720ED98EAAEBDDEC41A5D3,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m181A10417CF3D7FB11597D6BC8980F1041538D6C,
	U3Cget_ChildrenU3Ed__23_MoveNext_m73899F4BB42E92BEEDCC9E6DE93498B3E5FA12E8,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m5675C62761DE690A974233757AFF7ACA2F6588C5,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mEE728FF36A84F0406C9961667F0CF83A1D076EF8,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m1878F871DF3F1DECBC29D1EA12F302E7C7D913C0,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_mEDC188741AB55EAA55CD475E71338FF617C4DB81,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F30C3A17067EAEBE1F620E688233DDA9E058C00,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m0435461165875637BB8E9DDD819E8C18BC824AA0,
	JSONString_get_Tag_m7E09C62F10964D4D27854A83E0860A5AC985AFE3,
	JSONString_get_IsString_m5BF6795E2EC537F95E28F159B6760962D9F8CA4E,
	JSONString_GetEnumerator_m69464F52BB0DA089F154DCB55F5ED7302310CB9D,
	JSONString_get_Value_m4731F37BD04F956DABFB67BA0C421211C6C97F59,
	JSONString_set_Value_m2670EB54A9F28DF3F09476CA0AEF5A74F83EF45B,
	JSONString__ctor_m7E8C037643D81531C83ED7103B3B2E2847454981,
	JSONString_WriteToStringBuilder_mD04D1D9ECF6524AFE0BFFA950C5E28C6CCA58A81,
	JSONString_Equals_mBF62E77E4EA113962F09D5EF5AAB519F0C5DBE96,
	JSONString_GetHashCode_m0D03E5BEB56AD43BDEFC55A105FB80C808C22017,
	JSONNumber_get_Tag_m6B46BC7A9FAB14E69076796A4EC0EA692AE868F8,
	JSONNumber_get_IsNumber_m4EA2B0AAFDABFB616DC3F2F5BF2432E5FBC7DE1C,
	JSONNumber_GetEnumerator_m401DDA65F1DA380E495054994C9AABEAAD9E1DA3,
	JSONNumber_get_Value_mF47E99756A3D325CF16BE8BFD6FF7148DFCAB81D,
	JSONNumber_set_Value_mA2DD9A59619C9FD69ED20EAD8EE931E4506589A1,
	JSONNumber_get_AsDouble_m71742FA87FBA9CEC1B3CD7797BB12D940AA8C3D5,
	JSONNumber_set_AsDouble_m01EF626FFB8BCB29DDDB4AB4B6A6FD4973C0395E,
	JSONNumber__ctor_m540460B21D5ED37F9529F1734B24B6E868D432B8,
	JSONNumber__ctor_mAAF7F464DDCB064C83D2898FE50CCE90F77E4808,
	JSONNumber_WriteToStringBuilder_m918A4406F272535FBDEC2E67DA558DF0B3AB2264,
	JSONNumber_IsNumeric_mFAC1B74A6BD2705D6B2952D74DA6DF6D38C4F268,
	JSONNumber_Equals_mA8EBF84AAB284A4FF0E308F193B06A812E038A3F,
	JSONNumber_GetHashCode_m886C693586191D3BE953E1E1CC7D5548B1E737D3,
	JSONBool_get_Tag_mB9CA91991F00F4B74A0B8506DF446B77AF7872AB,
	JSONBool_get_IsBoolean_m4C847D82501287231AD2E08CBB6CADA94D980599,
	JSONBool_GetEnumerator_mB707A772B8651C727BABE7657335A6EFCFD2D407,
	JSONBool_get_Value_m7EDACE20FE016532F6714D1311EC851635DD4C53,
	JSONBool_set_Value_mE4AC78CF051938BCFBD45858D58A260709A210BC,
	JSONBool_get_AsBool_m2BB0201AE8B6118A20FCF2727F3F8D235505A552,
	JSONBool_set_AsBool_m3D364B4168376B1F5312DCF0F4B771C278BF014F,
	JSONBool__ctor_mBFA987A0D1492AFBC458BB89C88E7EC4AA2BE007,
	JSONBool__ctor_m9AAE9CA0B6044181BAF839B3CB43F9EB05DB2F8A,
	JSONBool_WriteToStringBuilder_m421FFE2D4A595460C5B0AA59C83E6EF5DECCC4E0,
	JSONBool_Equals_mEEF0FFBE41AB3814546ABA212A53AA57517D84B1,
	JSONBool_GetHashCode_m59F19469C64DA65D9B03D4BE6FBCBAAE2762C05E,
	JSONNull_CreateOrGet_mE2E06026E04958D2795026FBF38A47FBE14A7AEF,
	JSONNull__ctor_m774EAA6C8365C47B27BDB3FAD8C28686A1105033,
	JSONNull_get_Tag_m8CC0AC532BEF769781F75F62AC52009E154FB105,
	JSONNull_get_IsNull_m3B07A0182B1672877098FDC068A5ABA1AE336099,
	JSONNull_GetEnumerator_m4654CE7479238B4E8CB6219F9D859CBD7784AB18,
	JSONNull_get_Value_m553A353F6BB70066CDFC90AE29595B789CE3C0ED,
	JSONNull_set_Value_mBBFC09F30AC48C4DF47BF6FCA1F271A6C6AD814A,
	JSONNull_get_AsBool_m21B6CD1890428C491DBDC195ABD86C7A9B2CB520,
	JSONNull_set_AsBool_m9C680F040C87C7617AC04D1CCEC4DF04B3A062B1,
	JSONNull_Equals_m281EA55980AE7E71755B38A70914EDB521651CDE,
	JSONNull_GetHashCode_m11ACC117024154A6425EC0A92F92BE0850AB9AC8,
	JSONNull_WriteToStringBuilder_mA85B44E1E280A51A0E09A296D80CDF5735E8E4A6,
	JSONNull__cctor_mFBF013316CC0BC72EEF846918C4C18C8EDD2C75A,
	JSONLazyCreator_get_Tag_m3A6D074EB93F58B7070B64703D0ABCF7F75B1954,
	JSONLazyCreator_GetEnumerator_m1E8AD0743755E20E6E32B8B53643063DC5F3C13B,
	JSONLazyCreator__ctor_m0B79861AA63A30F62B32C3A2F3A87F152C1015F6,
	JSONLazyCreator__ctor_m6C390D7570282C672EB980117A97087DE5E763DF,
	JSONLazyCreator_Set_mC0B81E1660108B5070C659C1E3D6E8E715F5BA90,
	JSONLazyCreator_get_Item_m5679E58AD4E2A877E1B0DFDEE1966C91F22F6873,
	JSONLazyCreator_set_Item_mE8980B3490249E8442E0BBA0FFAF7A25579659FB,
	JSONLazyCreator_get_Item_m1E8CE199AA17884522F27832C69277BB00BABF07,
	JSONLazyCreator_set_Item_m0D0E3BF1D133B3E0DA46F6A127350770F4837A91,
	JSONLazyCreator_Add_m6AA4B279C70E27ED1B18AF98E92783CD97EA0889,
	JSONLazyCreator_Add_mA541314CB35828119821DF9D32C06C02E103D097,
	JSONLazyCreator_op_Equality_mCB25438D736BAEC78588A1C203AE79D31064898C,
	JSONLazyCreator_op_Inequality_m02BA9367F2E9AB8C9BCD5669C6BA9DE076178218,
	JSONLazyCreator_Equals_m14E80E99AF8A7CCD7EB9991FFABF3DB01980894F,
	JSONLazyCreator_GetHashCode_m20B43AF6AED7123FA9CA49F2019A4B214F523DF9,
	JSONLazyCreator_get_AsInt_m42A06E76316F1243A40AD6FD3C664575D8988758,
	JSONLazyCreator_set_AsInt_mD3942EF6D8F7D250F63D8358B626E3233E4E6D9A,
	JSONLazyCreator_get_AsFloat_m89017807E84A7B04CD9B072858E4B2780928CECF,
	JSONLazyCreator_set_AsFloat_m74C138C110E58F0F84C3E63844C9D10694D1BF9F,
	JSONLazyCreator_get_AsDouble_mBD1C53F2A1C5BEA7D0F16FC4427B361325D1DFF8,
	JSONLazyCreator_set_AsDouble_m490FE6CA5F4FB2BF477934745D8D184779BC746D,
	JSONLazyCreator_get_AsBool_mF27B6C9FC704065CDB2B178AED35069212EDDFCC,
	JSONLazyCreator_set_AsBool_m58268C31926C3B396083BF888747BC1F5E4BCBCB,
	JSONLazyCreator_get_AsArray_m7774E52E019503D6042D0AB8021466B5E617DFBC,
	JSONLazyCreator_get_AsObject_m258758F521BE31F4BA01AD8E3D769761D320CFCC,
	JSONLazyCreator_WriteToStringBuilder_m95D334FA623EB56E6BD461C32EA5CD864662F05B,
	JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D,
};
extern void Enumerator_get_IsValid_mFA3BB4B7F43B1005535936519FD3A42F5B23981E_AdjustorThunk (void);
extern void Enumerator__ctor_m057901299D85978F34E86C2B99997599C904516F_AdjustorThunk (void);
extern void Enumerator__ctor_m1DBE7DBD0628D4C87A31D88096548AF2917CC576_AdjustorThunk (void);
extern void Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9_AdjustorThunk (void);
extern void Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m9E8A702BF30F549C55B441B43EBBF823D1F8C3AF_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mD13470952E4791B2DC48D9A64AD1735248EC0AF2_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mCB3648E562A32EEA21FD3F4DB8C20160ED03722E_AdjustorThunk (void);
extern void ValueEnumerator_get_Current_mF69BBD9003C127D62E50E3A2AB4179EA3863B710_AdjustorThunk (void);
extern void ValueEnumerator_MoveNext_mFBBE4BD2BD4BEDC52A09992FFB2BE22E61571C4B_AdjustorThunk (void);
extern void ValueEnumerator_GetEnumerator_mB4E3F0A6AC7AA6140B861775571A15036BECB254_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m13B6087FA46C7C03A3471C28B3FBC22DD4171FC7_AdjustorThunk (void);
extern void KeyEnumerator__ctor_mD48489EDADC6546248A26244922162520AD16C3B_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m0747B3AC824951C3C8B818496CEDD6D88B9E7E8A_AdjustorThunk (void);
extern void KeyEnumerator_get_Current_m1E30F213CEB7C4567654D28C37E2841B4A81FAE0_AdjustorThunk (void);
extern void KeyEnumerator_MoveNext_m99B9BF2D4062F287B8D3A55C2C691537CAFE6F98_AdjustorThunk (void);
extern void KeyEnumerator_GetEnumerator_m0243DD23679D98AE2F30AD67FFFD4E27A16AB170_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[17] = 
{
	{ 0x0600003E, Enumerator_get_IsValid_mFA3BB4B7F43B1005535936519FD3A42F5B23981E_AdjustorThunk },
	{ 0x0600003F, Enumerator__ctor_m057901299D85978F34E86C2B99997599C904516F_AdjustorThunk },
	{ 0x06000040, Enumerator__ctor_m1DBE7DBD0628D4C87A31D88096548AF2917CC576_AdjustorThunk },
	{ 0x06000041, Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9_AdjustorThunk },
	{ 0x06000042, Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E_AdjustorThunk },
	{ 0x06000043, ValueEnumerator__ctor_m9E8A702BF30F549C55B441B43EBBF823D1F8C3AF_AdjustorThunk },
	{ 0x06000044, ValueEnumerator__ctor_mD13470952E4791B2DC48D9A64AD1735248EC0AF2_AdjustorThunk },
	{ 0x06000045, ValueEnumerator__ctor_mCB3648E562A32EEA21FD3F4DB8C20160ED03722E_AdjustorThunk },
	{ 0x06000046, ValueEnumerator_get_Current_mF69BBD9003C127D62E50E3A2AB4179EA3863B710_AdjustorThunk },
	{ 0x06000047, ValueEnumerator_MoveNext_mFBBE4BD2BD4BEDC52A09992FFB2BE22E61571C4B_AdjustorThunk },
	{ 0x06000048, ValueEnumerator_GetEnumerator_mB4E3F0A6AC7AA6140B861775571A15036BECB254_AdjustorThunk },
	{ 0x06000049, KeyEnumerator__ctor_m13B6087FA46C7C03A3471C28B3FBC22DD4171FC7_AdjustorThunk },
	{ 0x0600004A, KeyEnumerator__ctor_mD48489EDADC6546248A26244922162520AD16C3B_AdjustorThunk },
	{ 0x0600004B, KeyEnumerator__ctor_m0747B3AC824951C3C8B818496CEDD6D88B9E7E8A_AdjustorThunk },
	{ 0x0600004C, KeyEnumerator_get_Current_m1E30F213CEB7C4567654D28C37E2841B4A81FAE0_AdjustorThunk },
	{ 0x0600004D, KeyEnumerator_MoveNext_m99B9BF2D4062F287B8D3A55C2C691537CAFE6F98_AdjustorThunk },
	{ 0x0600004E, KeyEnumerator_GetEnumerator_m0243DD23679D98AE2F30AD67FFFD4E27A16AB170_AdjustorThunk },
};
static const int32_t s_InvokerIndices[231] = 
{
	2106,
	1277,
	840,
	1283,
	947,
	2121,
	1721,
	2106,
	2145,
	2145,
	2145,
	2145,
	2145,
	2145,
	2145,
	1741,
	947,
	1721,
	1283,
	1277,
	1283,
	2121,
	2121,
	2121,
	1277,
	309,
	2218,
	2121,
	2219,
	2220,
	2082,
	1685,
	2106,
	1708,
	2148,
	1744,
	2145,
	1741,
	2121,
	2121,
	3425,
	3425,
	3417,
	3338,
	3431,
	3477,
	3421,
	3371,
	3430,
	3460,
	3397,
	3129,
	3129,
	1440,
	2106,
	3575,
	3425,
	2673,
	3425,
	2173,
	3598,
	2145,
	1614,
	1615,
	2028,
	2145,
	1614,
	1615,
	1805,
	2121,
	2145,
	2220,
	1614,
	1615,
	1805,
	2121,
	2145,
	2219,
	1721,
	2028,
	2121,
	2145,
	2173,
	2121,
	2173,
	2121,
	1708,
	2173,
	2145,
	2121,
	2173,
	2121,
	2121,
	2121,
	1708,
	2173,
	2145,
	2173,
	2173,
	2121,
	2173,
	2121,
	2121,
	2121,
	2145,
	1741,
	2106,
	2145,
	2218,
	1277,
	840,
	1283,
	947,
	2106,
	947,
	1277,
	1283,
	2121,
	309,
	2173,
	1708,
	2173,
	2145,
	2173,
	2121,
	2173,
	2121,
	2121,
	2121,
	2145,
	1741,
	2106,
	2145,
	2218,
	1283,
	947,
	1277,
	840,
	2106,
	947,
	1283,
	1277,
	1283,
	2121,
	309,
	2173,
	2173,
	1339,
	1708,
	2173,
	2145,
	2173,
	2121,
	2173,
	2121,
	2121,
	2121,
	2106,
	2145,
	2218,
	2121,
	1721,
	1721,
	309,
	1440,
	2106,
	2106,
	2145,
	2218,
	2121,
	1721,
	2082,
	1685,
	1685,
	1721,
	309,
	3460,
	1440,
	2106,
	2106,
	2145,
	2218,
	2121,
	1721,
	2145,
	1741,
	1741,
	1721,
	309,
	1440,
	2106,
	3575,
	2173,
	2106,
	2145,
	2218,
	2121,
	1721,
	2145,
	1741,
	1440,
	2106,
	309,
	3598,
	2106,
	2218,
	1721,
	947,
	1721,
	1277,
	840,
	1283,
	947,
	1721,
	947,
	3129,
	3129,
	1440,
	2106,
	2106,
	1708,
	2148,
	1744,
	2082,
	1685,
	2145,
	1741,
	2121,
	2121,
	309,
	3425,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	231,
	s_methodPointers,
	17,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
