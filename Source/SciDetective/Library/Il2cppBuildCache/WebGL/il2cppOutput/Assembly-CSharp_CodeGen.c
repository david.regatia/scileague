﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AU_BotController::Start()
extern void AU_BotController_Start_m7796F437384EB35DC57043F13889BC9FA814B014 (void);
// 0x00000002 System.Void AU_BotController::Update()
extern void AU_BotController_Update_m81555C66C1CFF34117EEA2EE95A0A97C806DF437 (void);
// 0x00000003 System.Void AU_BotController::FixedUpdate()
extern void AU_BotController_FixedUpdate_m5AC5393A439659B745A73CED676F18DBC0962F38 (void);
// 0x00000004 System.Void AU_BotController::OnTriggerEnter(UnityEngine.Collider)
extern void AU_BotController_OnTriggerEnter_m5E17B2B114D06A85A27662651B3106ADBD7DA590 (void);
// 0x00000005 System.Void AU_BotController::OnTriggerExit(UnityEngine.Collider)
extern void AU_BotController_OnTriggerExit_m6E8BC8ED7F435ABA288A109F06C3817ADC938596 (void);
// 0x00000006 System.Void AU_BotController::RandomMovement()
extern void AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED (void);
// 0x00000007 System.Void AU_BotController::SetRole(System.Boolean)
extern void AU_BotController_SetRole_m56D4A94572E60D92E09F6D4DE7CBB28862165FBD (void);
// 0x00000008 System.Void AU_BotController::Speak()
extern void AU_BotController_Speak_m376A6A1824ACF99D6ADEEEFD0ED68B0966C2DA86 (void);
// 0x00000009 System.Void AU_BotController::StopSpeaking()
extern void AU_BotController_StopSpeaking_m08A100330610E85F68D98287A9E4CCA2700F5DD2 (void);
// 0x0000000A System.Void AU_BotController::.ctor()
extern void AU_BotController__ctor_mF59665130179C15CA492F8C274911D9106201AB1 (void);
// 0x0000000B System.Void AU_Game_ManagerController::Awake()
extern void AU_Game_ManagerController_Awake_m6215ABB8E34964976ED4523911102A892D419C6E (void);
// 0x0000000C System.Void AU_Game_ManagerController::OnEnable()
extern void AU_Game_ManagerController_OnEnable_m3B1F4AA5EF833EF601CD95DD89F8DA614EAAC770 (void);
// 0x0000000D System.Void AU_Game_ManagerController::OnDisable()
extern void AU_Game_ManagerController_OnDisable_m83DA97F63DCCC218848A719EC5D88D6626E845DD (void);
// 0x0000000E System.Void AU_Game_ManagerController::Start()
extern void AU_Game_ManagerController_Start_m2CE92E47166F41B2434FEFEE831DEEBC40D47184 (void);
// 0x0000000F System.Void AU_Game_ManagerController::StartGame()
extern void AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9 (void);
// 0x00000010 System.Void AU_Game_ManagerController::OpenLeaveGamePanel()
extern void AU_Game_ManagerController_OpenLeaveGamePanel_mB15F51F31738BBD2F7C3D7F818B05D0670F569E7 (void);
// 0x00000011 System.Void AU_Game_ManagerController::CloseLeaveGamePanel()
extern void AU_Game_ManagerController_CloseLeaveGamePanel_mC5510552A6CF1A4DB8AA676A30F7F52AF5BB4FB6 (void);
// 0x00000012 System.Void AU_Game_ManagerController::.ctor()
extern void AU_Game_ManagerController__ctor_m89B9D1CD615856A679C922B15619DD0D755D57B9 (void);
// 0x00000013 System.Void AU_InteractableController::OnEnable()
extern void AU_InteractableController_OnEnable_mC1C1E50C6218B02CA515D38DB1FEFB271266D9FF (void);
// 0x00000014 System.Void AU_InteractableController::OnTriggerEnter(UnityEngine.Collider)
extern void AU_InteractableController_OnTriggerEnter_m72F7FEA6A0317E3A5B55DAA4F06422695886F46D (void);
// 0x00000015 System.Void AU_InteractableController::OnTriggerExit(UnityEngine.Collider)
extern void AU_InteractableController_OnTriggerExit_m916CC2265525EE04FD3B1ABFFE8B6DDDDD1EF965 (void);
// 0x00000016 System.Void AU_InteractableController::ReportImposter()
extern void AU_InteractableController_ReportImposter_mF459D0161F39FBEAF55DDC78BD06645EF0B70F12 (void);
// 0x00000017 System.Void AU_InteractableController::.ctor()
extern void AU_InteractableController__ctor_m43F69DA379F9F73BA48B8851F6BF24C63E8C8E2E (void);
// 0x00000018 System.Void AU_PlayerController::Awake()
extern void AU_PlayerController_Awake_m345BCE0A4729CA5A2185D769C8D16E21617487D4 (void);
// 0x00000019 System.Void AU_PlayerController::OnEnable()
extern void AU_PlayerController_OnEnable_m00A3E46DE1B22F49331AD274EE622B6298062219 (void);
// 0x0000001A System.Void AU_PlayerController::OnDisable()
extern void AU_PlayerController_OnDisable_m0FF4AC76EDFE4B8C46315750B657E87C770A735F (void);
// 0x0000001B System.Void AU_PlayerController::Start()
extern void AU_PlayerController_Start_mB8DDD548ED35BA81A4117BC2215EBC1C08F54E85 (void);
// 0x0000001C System.Void AU_PlayerController::Update()
extern void AU_PlayerController_Update_m65FE80358152F56FCF82812EF29FC5D2B828848B (void);
// 0x0000001D System.Void AU_PlayerController::FixedUpdate()
extern void AU_PlayerController_FixedUpdate_m942D4DD16DE26C5411BE7ABDCDDE527C7A16E0AE (void);
// 0x0000001E System.Void AU_PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void AU_PlayerController_OnTriggerEnter_mA4B0B165E1B69920872BAEB569266563D7A755B5 (void);
// 0x0000001F System.Void AU_PlayerController::OnTriggerExit(UnityEngine.Collider)
extern void AU_PlayerController_OnTriggerExit_m32B48AD467169CB1CE911B346C1C33D06E65312E (void);
// 0x00000020 System.Void AU_PlayerController::InterrogateTarget(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void AU_PlayerController_InterrogateTarget_mDE8380697F779C63157CB22C9558AD09006244EF (void);
// 0x00000021 System.Void AU_PlayerController::.ctor()
extern void AU_PlayerController__ctor_mA6753EFBA4B72FB22758D458BD6BC90F95363895 (void);
// 0x00000022 System.Void AU_Report_ImposterController::Start()
extern void AU_Report_ImposterController_Start_m00734A7FA72823C437A9BDD48BF1B5399BC5FD67 (void);
// 0x00000023 System.Void AU_Report_ImposterController::OnEnable()
extern void AU_Report_ImposterController_OnEnable_m6A45D381FD55311D4B17002C71F39EE2F900370D (void);
// 0x00000024 System.Void AU_Report_ImposterController::ButtonOrder(System.Int32)
extern void AU_Report_ImposterController_ButtonOrder_m952289E671516CC2964D3CA792224304FC5DA1A8 (void);
// 0x00000025 System.Void AU_Report_ImposterController::ButtonOrderPanelClose()
extern void AU_Report_ImposterController_ButtonOrderPanelClose_m3E4E512F15C991E94E84DCE6723697B4084DA04A (void);
// 0x00000026 System.Void AU_Report_ImposterController::ButtonOrderPanelOpen()
extern void AU_Report_ImposterController_ButtonOrderPanelOpen_m18A623EE7B87062A8E5A0ADAEAEA1994F53821BE (void);
// 0x00000027 System.Void AU_Report_ImposterController::.ctor()
extern void AU_Report_ImposterController__ctor_m1AFC57DA9BF94C986F34ED64B0563DF398C4A029 (void);
// 0x00000028 System.Void AU_Task_APIController::Start()
extern void AU_Task_APIController_Start_m97C5E825C29CC1836D7C1609F3B36FA7FEF84EC7 (void);
// 0x00000029 System.Void AU_Task_APIController::RandomTask()
extern void AU_Task_APIController_RandomTask_m2708721CEFB9DBD5A22F3D2CF4139C3ABD764D40 (void);
// 0x0000002A System.Collections.IEnumerator AU_Task_APIController::GetTask()
extern void AU_Task_APIController_GetTask_mB1DFCB6D28903A0D56428DAA5D7569183FE7893F (void);
// 0x0000002B System.Void AU_Task_APIController::.ctor()
extern void AU_Task_APIController__ctor_m7A1DD4955F1F40620E05964D4A634E3B0EDDFFC4 (void);
// 0x0000002C System.Void AU_Task_APIController/<GetTask>d__11::.ctor(System.Int32)
extern void U3CGetTaskU3Ed__11__ctor_m6D939B0FDD1D79BAB1329FAD29DEFE4957376BA0 (void);
// 0x0000002D System.Void AU_Task_APIController/<GetTask>d__11::System.IDisposable.Dispose()
extern void U3CGetTaskU3Ed__11_System_IDisposable_Dispose_m557C06BF7C73221B1F502DCF1BEACA29E0DD7D2D (void);
// 0x0000002E System.Boolean AU_Task_APIController/<GetTask>d__11::MoveNext()
extern void U3CGetTaskU3Ed__11_MoveNext_m7EA306344F8976986B822EBF0ED92B85FC7E1C2F (void);
// 0x0000002F System.Object AU_Task_APIController/<GetTask>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetTaskU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E075F8AEC9BA63AF6BBC5A5455FBEA6A804A7C (void);
// 0x00000030 System.Void AU_Task_APIController/<GetTask>d__11::System.Collections.IEnumerator.Reset()
extern void U3CGetTaskU3Ed__11_System_Collections_IEnumerator_Reset_mCC1F160DE4BFFAF858DC9BE454D1FDD9FFCF2F7E (void);
// 0x00000031 System.Object AU_Task_APIController/<GetTask>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CGetTaskU3Ed__11_System_Collections_IEnumerator_get_Current_m62D64F79E49AFE84E3A787EE6D4508B3C0093300 (void);
// 0x00000032 System.Void TimerController::Start()
extern void TimerController_Start_m192651C0602F061E20482E7080024B13E4DE330F (void);
// 0x00000033 System.Void TimerController::BeginTimer()
extern void TimerController_BeginTimer_mAA181BB489DE0D7B29773360AC850C0208BF3202 (void);
// 0x00000034 System.Void TimerController::EndTimer()
extern void TimerController_EndTimer_mE6EAC849759F442EE09DED70216DFFD7FFDE5538 (void);
// 0x00000035 System.Collections.IEnumerator TimerController::UpdateTimer()
extern void TimerController_UpdateTimer_m09B308F03DCC34431BA5C4231CC62E1983914F49 (void);
// 0x00000036 System.Void TimerController::Update()
extern void TimerController_Update_mFB0929F437BB7887D460B7CEA453AFE346994AD6 (void);
// 0x00000037 System.Void TimerController::.ctor()
extern void TimerController__ctor_m1C44AEBF40C4F225ED192C258B42CB3BE16DCDAE (void);
// 0x00000038 System.Void TimerController/<UpdateTimer>d__7::.ctor(System.Int32)
extern void U3CUpdateTimerU3Ed__7__ctor_mA0B52C60B7819A6F7E5987BB7DE53D6E2D043D87 (void);
// 0x00000039 System.Void TimerController/<UpdateTimer>d__7::System.IDisposable.Dispose()
extern void U3CUpdateTimerU3Ed__7_System_IDisposable_Dispose_m29F987C41828A15EC77E33CE934699BC1E19A592 (void);
// 0x0000003A System.Boolean TimerController/<UpdateTimer>d__7::MoveNext()
extern void U3CUpdateTimerU3Ed__7_MoveNext_mF17E7DEA1A0F28ADD4B232830841FFE089343769 (void);
// 0x0000003B System.Object TimerController/<UpdateTimer>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateTimerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22547D0096F650C81D097E1EE42F520524068BDE (void);
// 0x0000003C System.Void TimerController/<UpdateTimer>d__7::System.Collections.IEnumerator.Reset()
extern void U3CUpdateTimerU3Ed__7_System_Collections_IEnumerator_Reset_m1C3E36C661C7EBBFE43CC87ECDAA0551A9213CE8 (void);
// 0x0000003D System.Object TimerController/<UpdateTimer>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateTimerU3Ed__7_System_Collections_IEnumerator_get_Current_mEFCD3D72C3BFF0BBE84959E3C2B2007EFC19051F (void);
static Il2CppMethodPointer s_methodPointers[61] = 
{
	AU_BotController_Start_m7796F437384EB35DC57043F13889BC9FA814B014,
	AU_BotController_Update_m81555C66C1CFF34117EEA2EE95A0A97C806DF437,
	AU_BotController_FixedUpdate_m5AC5393A439659B745A73CED676F18DBC0962F38,
	AU_BotController_OnTriggerEnter_m5E17B2B114D06A85A27662651B3106ADBD7DA590,
	AU_BotController_OnTriggerExit_m6E8BC8ED7F435ABA288A109F06C3817ADC938596,
	AU_BotController_RandomMovement_m1968E8F5F792EA78C3E76269255D3B37802C72ED,
	AU_BotController_SetRole_m56D4A94572E60D92E09F6D4DE7CBB28862165FBD,
	AU_BotController_Speak_m376A6A1824ACF99D6ADEEEFD0ED68B0966C2DA86,
	AU_BotController_StopSpeaking_m08A100330610E85F68D98287A9E4CCA2700F5DD2,
	AU_BotController__ctor_mF59665130179C15CA492F8C274911D9106201AB1,
	AU_Game_ManagerController_Awake_m6215ABB8E34964976ED4523911102A892D419C6E,
	AU_Game_ManagerController_OnEnable_m3B1F4AA5EF833EF601CD95DD89F8DA614EAAC770,
	AU_Game_ManagerController_OnDisable_m83DA97F63DCCC218848A719EC5D88D6626E845DD,
	AU_Game_ManagerController_Start_m2CE92E47166F41B2434FEFEE831DEEBC40D47184,
	AU_Game_ManagerController_StartGame_mF3D2D699563098F6912E0840CC504C8ED89409B9,
	AU_Game_ManagerController_OpenLeaveGamePanel_mB15F51F31738BBD2F7C3D7F818B05D0670F569E7,
	AU_Game_ManagerController_CloseLeaveGamePanel_mC5510552A6CF1A4DB8AA676A30F7F52AF5BB4FB6,
	AU_Game_ManagerController__ctor_m89B9D1CD615856A679C922B15619DD0D755D57B9,
	AU_InteractableController_OnEnable_mC1C1E50C6218B02CA515D38DB1FEFB271266D9FF,
	AU_InteractableController_OnTriggerEnter_m72F7FEA6A0317E3A5B55DAA4F06422695886F46D,
	AU_InteractableController_OnTriggerExit_m916CC2265525EE04FD3B1ABFFE8B6DDDDD1EF965,
	AU_InteractableController_ReportImposter_mF459D0161F39FBEAF55DDC78BD06645EF0B70F12,
	AU_InteractableController__ctor_m43F69DA379F9F73BA48B8851F6BF24C63E8C8E2E,
	AU_PlayerController_Awake_m345BCE0A4729CA5A2185D769C8D16E21617487D4,
	AU_PlayerController_OnEnable_m00A3E46DE1B22F49331AD274EE622B6298062219,
	AU_PlayerController_OnDisable_m0FF4AC76EDFE4B8C46315750B657E87C770A735F,
	AU_PlayerController_Start_mB8DDD548ED35BA81A4117BC2215EBC1C08F54E85,
	AU_PlayerController_Update_m65FE80358152F56FCF82812EF29FC5D2B828848B,
	AU_PlayerController_FixedUpdate_m942D4DD16DE26C5411BE7ABDCDDE527C7A16E0AE,
	AU_PlayerController_OnTriggerEnter_mA4B0B165E1B69920872BAEB569266563D7A755B5,
	AU_PlayerController_OnTriggerExit_m32B48AD467169CB1CE911B346C1C33D06E65312E,
	AU_PlayerController_InterrogateTarget_mDE8380697F779C63157CB22C9558AD09006244EF,
	AU_PlayerController__ctor_mA6753EFBA4B72FB22758D458BD6BC90F95363895,
	AU_Report_ImposterController_Start_m00734A7FA72823C437A9BDD48BF1B5399BC5FD67,
	AU_Report_ImposterController_OnEnable_m6A45D381FD55311D4B17002C71F39EE2F900370D,
	AU_Report_ImposterController_ButtonOrder_m952289E671516CC2964D3CA792224304FC5DA1A8,
	AU_Report_ImposterController_ButtonOrderPanelClose_m3E4E512F15C991E94E84DCE6723697B4084DA04A,
	AU_Report_ImposterController_ButtonOrderPanelOpen_m18A623EE7B87062A8E5A0ADAEAEA1994F53821BE,
	AU_Report_ImposterController__ctor_m1AFC57DA9BF94C986F34ED64B0563DF398C4A029,
	AU_Task_APIController_Start_m97C5E825C29CC1836D7C1609F3B36FA7FEF84EC7,
	AU_Task_APIController_RandomTask_m2708721CEFB9DBD5A22F3D2CF4139C3ABD764D40,
	AU_Task_APIController_GetTask_mB1DFCB6D28903A0D56428DAA5D7569183FE7893F,
	AU_Task_APIController__ctor_m7A1DD4955F1F40620E05964D4A634E3B0EDDFFC4,
	U3CGetTaskU3Ed__11__ctor_m6D939B0FDD1D79BAB1329FAD29DEFE4957376BA0,
	U3CGetTaskU3Ed__11_System_IDisposable_Dispose_m557C06BF7C73221B1F502DCF1BEACA29E0DD7D2D,
	U3CGetTaskU3Ed__11_MoveNext_m7EA306344F8976986B822EBF0ED92B85FC7E1C2F,
	U3CGetTaskU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E075F8AEC9BA63AF6BBC5A5455FBEA6A804A7C,
	U3CGetTaskU3Ed__11_System_Collections_IEnumerator_Reset_mCC1F160DE4BFFAF858DC9BE454D1FDD9FFCF2F7E,
	U3CGetTaskU3Ed__11_System_Collections_IEnumerator_get_Current_m62D64F79E49AFE84E3A787EE6D4508B3C0093300,
	TimerController_Start_m192651C0602F061E20482E7080024B13E4DE330F,
	TimerController_BeginTimer_mAA181BB489DE0D7B29773360AC850C0208BF3202,
	TimerController_EndTimer_mE6EAC849759F442EE09DED70216DFFD7FFDE5538,
	TimerController_UpdateTimer_m09B308F03DCC34431BA5C4231CC62E1983914F49,
	TimerController_Update_mFB0929F437BB7887D460B7CEA453AFE346994AD6,
	TimerController__ctor_m1C44AEBF40C4F225ED192C258B42CB3BE16DCDAE,
	U3CUpdateTimerU3Ed__7__ctor_mA0B52C60B7819A6F7E5987BB7DE53D6E2D043D87,
	U3CUpdateTimerU3Ed__7_System_IDisposable_Dispose_m29F987C41828A15EC77E33CE934699BC1E19A592,
	U3CUpdateTimerU3Ed__7_MoveNext_mF17E7DEA1A0F28ADD4B232830841FFE089343769,
	U3CUpdateTimerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22547D0096F650C81D097E1EE42F520524068BDE,
	U3CUpdateTimerU3Ed__7_System_Collections_IEnumerator_Reset_m1C3E36C661C7EBBFE43CC87ECDAA0551A9213CE8,
	U3CUpdateTimerU3Ed__7_System_Collections_IEnumerator_get_Current_mEFCD3D72C3BFF0BBE84959E3C2B2007EFC19051F,
};
static const int32_t s_InvokerIndices[61] = 
{
	1990,
	1990,
	1990,
	1615,
	1615,
	1990,
	1633,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1615,
	1615,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1990,
	1615,
	1615,
	1661,
	1990,
	1990,
	1990,
	1602,
	1990,
	1990,
	1990,
	1990,
	1990,
	1951,
	1990,
	1602,
	1990,
	1972,
	1951,
	1990,
	1951,
	1990,
	1990,
	1990,
	1951,
	1990,
	1990,
	1602,
	1990,
	1972,
	1951,
	1990,
	1951,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	61,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
