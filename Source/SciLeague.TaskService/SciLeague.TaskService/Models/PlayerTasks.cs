﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TaskService.Models
{
    public class PlayerTasks
    {
        public int Id { get; set; }
        public string CreationDate { get; set; }
        public string Username { get; set; }
        public int TaskId { get; set; }
    }
}
