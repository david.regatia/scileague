﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TaskService.Models
{
    public class ContributionMultiChoiceTask
    {
        public int Id { get; set; }
        public int PlayerTasksID { get; set; }
        public bool CorrectChoiceAnswered { get; set; }
        public int AttempsNeeded { get; set; }
        public int PointsConquered { get; set; }
        public int SelectedChoiceID { get; set; }
    }
}
