﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TaskService.Models
{
    public class Task
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public string CreationDate { get; set; }
        public string Difficulty { get; set; }
        public string CreatorUsername { get; set; }
        public int Points { get; set; }
    }
}
