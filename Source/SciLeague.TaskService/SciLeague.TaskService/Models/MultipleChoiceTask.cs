﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TaskService.Models
{
    public class MultipleChoiceTask
    {
        [Key]
        public int Id { get; set; }
        public string Question { get; set; }
        public string Body { get; set; }
        public bool DoWeKnowCorrectAnswer { get; set; }

        [ForeignKey("Task")]
        public int TaskId { get; set; }
        public Task Task { get; set; }

    }
}
