﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TaskService.Models
{
    public class Choice
    {
        [Key]
        public int Id { get; set; }
        public string Answer { get; set; }
        public bool IsItTheCorrectAnswer { get; set; }

        [ForeignKey("MultipleChoiceTask")]
        public int MultipleChoiceTaskId { get; set; }
        public MultipleChoiceTask MultipleChoiceTask { get; set; }

    }
}
