﻿using Microsoft.EntityFrameworkCore;
using SciLeague.TaskService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TaskService.Data
{
    public class SciLeagueTaskContext : DbContext
    {

        public SciLeagueTaskContext(DbContextOptions<SciLeagueTaskContext> options) : base(options)
        {

        }


        public virtual DbSet<Models.Task> Task { get; set; }
        public virtual DbSet<MultipleChoiceTask> MultipleChoiceTask { get; set; }
        public virtual DbSet<Choice> Choice { get; set; }
        public DbSet<SciLeague.TaskService.Models.ContributionMultiChoiceTask> ContributionMultiChoiceTask { get; set; }
        public DbSet<SciLeague.TaskService.Models.PlayerTasks> PlayerTasks { get; set; }

    }
}
