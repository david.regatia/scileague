﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TaskService.Data;
using SciLeague.TaskService.Models;
using Task = SciLeague.TaskService.Models.Task;

namespace SciLeague.TaskService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MultipleChoiceTasksController : ControllerBase
    {
        private readonly SciLeagueTaskContext _context;

        public MultipleChoiceTasksController(SciLeagueTaskContext context)
        {
            _context = context;
        }



        // GET: api/MultipleChoiceTasks/5
        [HttpGet("GetMultipleChoiceTask")]
        public async Task<ActionResult<MultipleChoiceTask>> GetMultipleChoiceTask(string id)
        {
            var multipleChoiceTask = await _context.MultipleChoiceTask.FindAsync(int.Parse(id));

            if (multipleChoiceTask == null)
            {
                return NotFound();
            }

            return multipleChoiceTask;
        }


        // GET: api/MultipleChoiceTasks
        [HttpGet("GetMultipleChoiceTasks")]
        public async Task<ActionResult<IEnumerable<MultipleChoiceTask>>> GetMultipleChoiceTasks(int taskId)
        {
            return await _context.MultipleChoiceTask.Where(x => x.TaskId == taskId).Include(x => x.Task).ToListAsync();
        }


        [HttpPost("CreateMultipleChoiceTask")]
        public async Task<ActionResult<MultipleChoiceTask>> CreateMultipleChoiceTask([FromBody] MultipleChoiceTask multipleChoiceTask)
        {
            Task task = new Task();
            if ((task = _context.Task.Where(x => x.Id == multipleChoiceTask.TaskId).FirstOrDefault()) == null)
            {
                return BadRequest("Doesn't exist any task with the specified ID.");
            }

            MultipleChoiceTask newChoiceTask = new MultipleChoiceTask();

            newChoiceTask.Body = multipleChoiceTask.Body;
            newChoiceTask.DoWeKnowCorrectAnswer = multipleChoiceTask.DoWeKnowCorrectAnswer;
            newChoiceTask.Question = multipleChoiceTask.Question;
            newChoiceTask.Task = task;
            newChoiceTask.TaskId = task.Id;

            _context.MultipleChoiceTask.Add(multipleChoiceTask);
            await _context.SaveChangesAsync();

            return Ok(multipleChoiceTask);
        }



        [HttpDelete("DeleteMultipleChoiceTask")]
        public async Task<IActionResult> DeleteMultipleChoiceTask(int id)
        {
            var multipleChoiceTask = await _context.MultipleChoiceTask.FindAsync(id);
            if (multipleChoiceTask == null)
            {
                return NotFound();
            }

            _context.MultipleChoiceTask.Remove(multipleChoiceTask);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet("DoesMultipleChoiceTaskExist")]
        public bool DoesMultipleChoiceTaskExist(int id)
        {
            return _context.MultipleChoiceTask.Any(e => e.Id == id);
        }


        [HttpGet("GetAvailableTaskForUser")]
        public async Task<IActionResult> GetAvailableTaskForUser(string username)
        {
            bool taskFound = false;
            MultipleChoiceTask task = new MultipleChoiceTask();
            PlayerTasks playerTask = new PlayerTasks();

            // Find a task that hasn't been played by the user.
            foreach (var taskIterator in _context.MultipleChoiceTask.ToList())
            {
                playerTask = await _context.PlayerTasks.Where(x => x.TaskId == taskIterator.TaskId && x.Username == username).FirstOrDefaultAsync();

                if (playerTask == null)
                {
                    taskFound = true;
                    task = taskIterator;
                    break;
                }
            }

            if (!taskFound)
            {
                return BadRequest("There is no available task for the user.");
            }

            return Ok(task);
        }
    }
}
