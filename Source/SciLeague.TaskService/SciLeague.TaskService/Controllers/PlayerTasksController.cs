﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TaskService.Data;
using SciLeague.TaskService.Models;

namespace SciLeague.TaskService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerTasksController : ControllerBase
    {
        private readonly SciLeagueTaskContext _context;

        public PlayerTasksController(SciLeagueTaskContext context)
        {
            _context = context;
        }

        // GET: api/PlayerTasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerTasks>>> GetPlayerTasks()
        {
            return await _context.PlayerTasks.ToListAsync();
        }

        // GET: api/PlayerTasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlayerTasks>> GetPlayerTasks(int id)
        {
            var playerTasks = await _context.PlayerTasks.FindAsync(id);

            if (playerTasks == null)
            {
                return NotFound();
            }

            return playerTasks;
        }

        // PUT: api/PlayerTasks/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayerTasks(int id, PlayerTasks playerTasks)
        {
            if (id != playerTasks.Id)
            {
                return BadRequest();
            }

            _context.Entry(playerTasks).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerTasksExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        // POST: api/PlayerTasks
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("PostPlayerTasks")]
        public async Task<ActionResult<PlayerTasks>> PostPlayerTasks(PlayerTasks playerTasksBody)
        {
            PlayerTasks playerTasks = new PlayerTasks();

            DateTime date = DateTime.Now.AddDays(-6);

            //playerTasks.CreationDate = DateTime.Now.ToString();
            playerTasks.CreationDate = date.ToString();
            playerTasks.Username = playerTasksBody.Username;
            playerTasks.TaskId = playerTasksBody.TaskId;

            _context.PlayerTasks.Add(playerTasks);
            await _context.SaveChangesAsync();

            return Ok(playerTasks.Id);
        }



        // DELETE: api/PlayerTasks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlayerTasks(int id)
        {
            var playerTasks = await _context.PlayerTasks.FindAsync(id);
            if (playerTasks == null)
            {
                return NotFound();
            }

            _context.PlayerTasks.Remove(playerTasks);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PlayerTasksExists(int id)
        {
            return _context.PlayerTasks.Any(e => e.Id == id);
        }



        [HttpGet("GetNumberOfConsecutivePlayDays")]
        public async Task<ActionResult> GetNumberOfConsecutivePlayDays(string username)
        {
            DateTime lastDate = DateTime.Now;
            int consecutiveDays = 0;
            DateTime date = DateTime.Now;

            // Get all submitted tasks from user.
            List<PlayerTasks> playerTasks = new List<PlayerTasks>();
            playerTasks = await _context.PlayerTasks.Where(x => x.Username == username).OrderByDescending(x => x.Id).ToListAsync();


            if (playerTasks == null)
            {
                // If there is not a single submitted task.
                // We return zero.
                return Ok(0);
            }

            foreach(var playerTask in playerTasks)
            {
                // If there are submitted tasks.
                // We are going to verify how many consecutive days the user has played.
                // Or how many consecutive days the user has not played. 

                date = DateTime.Parse(playerTask.CreationDate);

                if (date.Date == lastDate.Date)
                {
                    if (consecutiveDays == 0)
                    {
                        lastDate = date;
                        consecutiveDays++;
                    }
                }
                if (date.Date.AddDays(1) == lastDate.Date)
                {
                    lastDate = date;
                    consecutiveDays++;
                }
                else
                {
                    if (consecutiveDays != 0)
                    {
                        consecutiveDays = (DateTime.Now.Date - lastDate.Date).Days;
                        consecutiveDays++;
                        break;
                    }
                    else
                    {
                        consecutiveDays = (DateTime.Now.Date - date.Date).Days;
                        consecutiveDays = consecutiveDays * (-1);
                        break;
                    }
                }
            }

            return Ok(consecutiveDays);
        }
    }
}
