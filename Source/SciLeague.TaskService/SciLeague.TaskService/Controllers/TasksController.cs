﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TaskService.Data;
using SciLeague.TaskService.Models;

namespace SciLeague.TaskService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly SciLeagueTaskContext _context;

        public TasksController(SciLeagueTaskContext context)
        {
            _context = context;
        }



        [HttpGet("GetTask")]
        public async Task<ActionResult<Models.Task>> GetTask(int id)
        {
            var task = await _context.Task.FindAsync(id);

            if (task == null)
            {
                return NotFound();
            }

            return task;
        }

        [HttpGet("GetTaskPoints")]
        public async Task<ActionResult> GetTaskPoints(int taskID)
        {
            var task = await _context.Task.Where(x => x.Id == taskID).FirstOrDefaultAsync();

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task.Points.ToString());
        }


        [HttpPost("PostTask")]
        public async Task<ActionResult<Models.Task>> PostTask([FromBody] Models.Task task)
        {
            Models.Task newTask = new Models.Task();

            newTask.CreationDate = DateTime.Now.ToString();
            newTask.Difficulty = task.Difficulty;
            newTask.Points = task.Points;
            newTask.Type = task.Type;
            newTask.CreatorUsername = task.CreatorUsername;
            

            _context.Task.Add(task);
            await _context.SaveChangesAsync();

            return Ok(newTask);
        }



        [HttpDelete("DeleteTask")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            var task = await _context.Task.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }

            _context.Task.Remove(task);
            await _context.SaveChangesAsync();

            return NoContent();
        }


        [HttpGet("DoesTaskExist")]
        public bool DoesTaskExist(int id)
        {
            return _context.Task.Any(e => e.Id == id);
        }

    }
}
