﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TaskService.Data;
using SciLeague.TaskService.Models;

namespace SciLeague.TaskService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContributionMultiChoiceTasksController : ControllerBase
    {
        private readonly SciLeagueTaskContext _context;

        public ContributionMultiChoiceTasksController(SciLeagueTaskContext context)
        {
            _context = context;
        }

        // GET: api/ContributionMultiChoiceTasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContributionMultiChoiceTask>>> GetContributionMultiChoiceTask()
        {
            return await _context.ContributionMultiChoiceTask.ToListAsync();
        }

        // GET: api/ContributionMultiChoiceTasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ContributionMultiChoiceTask>> GetContributionMultiChoiceTask(int id)
        {
            var contributionMultiChoiceTask = await _context.ContributionMultiChoiceTask.FindAsync(id);

            if (contributionMultiChoiceTask == null)
            {
                return NotFound();
            }

            return contributionMultiChoiceTask;
        }

        // PUT: api/ContributionMultiChoiceTasks/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContributionMultiChoiceTask(int id, ContributionMultiChoiceTask contributionMultiChoiceTask)
        {
            if (id != contributionMultiChoiceTask.Id)
            {
                return BadRequest();
            }

            _context.Entry(contributionMultiChoiceTask).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContributionMultiChoiceTaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ContributionMultiChoiceTasks
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("PostContributionMultiChoiceTask")]
        public async Task<ActionResult<ContributionMultiChoiceTask>> PostContributionMultiChoiceTask(ContributionMultiChoiceTask contributionMultiChoiceTask)
        {
            _context.ContributionMultiChoiceTask.Add(contributionMultiChoiceTask);
            await _context.SaveChangesAsync();

            return Ok(contributionMultiChoiceTask);
        }

        // DELETE: api/ContributionMultiChoiceTasks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContributionMultiChoiceTask(int id)
        {
            var contributionMultiChoiceTask = await _context.ContributionMultiChoiceTask.FindAsync(id);
            if (contributionMultiChoiceTask == null)
            {
                return NotFound();
            }

            _context.ContributionMultiChoiceTask.Remove(contributionMultiChoiceTask);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ContributionMultiChoiceTaskExists(int id)
        {
            return _context.ContributionMultiChoiceTask.Any(e => e.Id == id);
        }
    }
}
