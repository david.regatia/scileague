﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TaskService.Data;
using SciLeague.TaskService.Models;

namespace SciLeague.TaskService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChoicesController : ControllerBase
    {
        private readonly SciLeagueTaskContext _context;

        public ChoicesController(SciLeagueTaskContext context)
        {
            _context = context;
        }

        // GET: api/Choices/5
        [HttpGet("GetChoice")]
        public async Task<ActionResult<Choice>> GetChoice(int id)
        {
            var choice = await _context.Choice.FindAsync(id);

            if (choice == null)
            {
                return NotFound();
            }

            return choice;
        }


        // GET: api/Choices
        [HttpGet("GetChoices")]
        public async Task<ActionResult<IEnumerable<Choice>>> GetChoices(int? id)
        {
            if (id == null)
            {
                return await _context.Choice.ToListAsync();
            }

            return await _context.Choice.Where(x => x.MultipleChoiceTaskId == id).Include(x => x.MultipleChoiceTask).ToListAsync();
        }


        [HttpPost("CreateChoice")]
        public async Task<ActionResult<Choice>> CreateChoice([FromBody] Choice choice)
        {
            Choice newChoice = new Choice();

            newChoice.Answer = choice.Answer;
            newChoice.IsItTheCorrectAnswer = choice.IsItTheCorrectAnswer;
            newChoice.MultipleChoiceTaskId = choice.MultipleChoiceTaskId;

            _context.Choice.Add(newChoice);
            await _context.SaveChangesAsync();

            return Ok(newChoice);
        }

        [HttpDelete("DeleteChoice")]
        public async Task<IActionResult> DeleteChoice(int id)
        {
            var choice = await _context.Choice.FindAsync(id);
            if (choice == null)
            {
                return NotFound();
            }

            _context.Choice.Remove(choice);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpPost("DoesChoiceExist")]
        public bool DoesChoiceExist(int id)
        {
            return _context.Choice.Any(e => e.Id == id);
        }
    }
}
