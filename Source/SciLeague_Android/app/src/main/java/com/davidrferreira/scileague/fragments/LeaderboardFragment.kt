package com.davidrferreira.scileague.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.davidrferreira.scileague.activities.InsideAppActivity
import com.davidrferreira.scileague.adapters.LeaderboardAdapter
import com.davidrferreira.scileague.databinding.FragmentLeaderboardBinding
import com.davidrferreira.scileague.models.Profile
import com.davidrferreira.scileague.models.ProfileUpperCase
import com.davidrferreira.scileague.models.UserPoints
import com.davidrferreira.scileague.networking.NetworkingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LeaderboardFragment : Fragment() {

    private lateinit var binding: FragmentLeaderboardBinding;
    private lateinit var leaderboardAdapter: LeaderboardAdapter;
    private val networking = NetworkingService()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = FragmentLeaderboardBinding.inflate(layoutInflater, container, false)

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            leaderboardAdapter = LeaderboardAdapter()
            adapter = leaderboardAdapter
        }



        return binding.root
    }

    override fun onResume() {
        super.onResume()
        getMyDetails()
        getLeaderboardTopPlayers(5)
    }


    private fun getMyDetails() {
        val token = (activity as InsideAppActivity).getSharedPreferencesClientToken()

        if (token != null) {
            var response = networking.apiService.getMyProfile(token)
            response.enqueue(object: Callback<ProfileUpperCase> {

                override fun onResponse(call: Call<ProfileUpperCase>, response: Response<ProfileUpperCase>) {
                    if (response.isSuccessful) {
                        binding.yourNameTextView.text = response.body()!!.username
                        binding.yourPointsTextView.text = response.body()!!.monthlyPoints.toString() + " Points"
                        binding.yourPositionTextView.text = response.body()!!.position.toString()
                    }
                    else {
                        Toast.makeText(context,
                                "It was not possible to get the profile details.", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ProfileUpperCase>, t: Throwable) {
                    Toast.makeText(context,
                            "It was not possible to get the profile details.", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private fun getLeaderboardTopPlayers(limit: Int) {
        var response = networking.apiService.getMonthlyTopPlayers(5)
        response.enqueue(object: Callback<List<Profile>> {
            override fun onResponse(call: Call<List<Profile>>, response: Response<List<Profile>>) {
                if (response.isSuccessful) {
                    binding.recyclerView.layoutManager = LinearLayoutManager(context)
                    leaderboardAdapter.submitPlayers(response.body()!!)
                }
                else {
                    Toast.makeText(context,
                            "It was not possible to get the leaderboard details.", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<List<Profile>>, t: Throwable) {
                Toast.makeText(context,
                        "It was not possible to get the leaderboard details.", Toast.LENGTH_SHORT).show()
            }
        })
    }
}