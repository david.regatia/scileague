package com.davidrferreira.scileague.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserPoints (
    @SerializedName("id")
    val id: Int,
    @SerializedName("username")
    val username: String,
    @SerializedName("totalPoints")
    val totalPoints: Int,
    @SerializedName("monthlyPoints")
    val monthlyPoints: Int
    ) : Serializable

