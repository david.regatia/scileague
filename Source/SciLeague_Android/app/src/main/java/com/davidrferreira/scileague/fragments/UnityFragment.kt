package com.davidrferreira.scileague.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.davidrferreira.scileague.fragments.HomeFragment
import com.davidrferreira.scileague.activities.InsideAppActivity
import com.davidrferreira.scileague.databinding.ActivityInsideAppBinding
import com.unity3d.player.UnityPlayer
import com.davidrferreira.scileague.databinding.FragmentUnityBinding
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import android.content.DialogInterface
import kotlin.system.exitProcess


class UnityFragment : Fragment() {

    protected var mUnityPlayer : UnityPlayer? = null
    var frameLayoutForUnity: FrameLayout? = null;
    private lateinit var binding: FragmentUnityBinding;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentUnityBinding.inflate(layoutInflater, container, false)

        mUnityPlayer = UnityPlayer(activity)
        frameLayoutForUnity = binding.frameLayoutForUnity as FrameLayout
        frameLayoutForUnity!!.addView(
            mUnityPlayer!!.view,
            FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT
        )

        mUnityPlayer!!.requestFocus()
        mUnityPlayer!!.windowFocusChanged(true)

        return binding.root;
    }

    override fun onDestroy() {
        super.onDestroy()
        mUnityPlayer!!.quit()
    }

    override fun onPause() {
        super.onPause()
        mUnityPlayer!!.pause()
    }

    override fun onResume() {
        super.onResume()
        mUnityPlayer!!.resume()
    }

}
