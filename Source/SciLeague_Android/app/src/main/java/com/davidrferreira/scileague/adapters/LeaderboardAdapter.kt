package com.davidrferreira.scileague.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.davidrferreira.scileague.R
import com.davidrferreira.scileague.databinding.LeaderboardItemListBinding
import com.davidrferreira.scileague.models.Profile
import com.davidrferreira.scileague.models.UserPoints

class LeaderboardAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var players :  ArrayList<Profile> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.leaderboard_item_list, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ViewHolder -> {
                holder.bindView(players[position], position)
            }
        }
    }

    override fun getItemCount(): Int {
        return players.size
    }

    fun submitPlayers(playerList: List<Profile>) {
        players = ArrayList(playerList)
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bindView(player : Profile, place: Int) {
            val binding = LeaderboardItemListBinding.bind(itemView)

            println("Player $player")

            val position = binding.positionTextView
            val username = binding.usernameTextView
            val points = binding.pointsTextView

            position.text = "#$place"
            points.text = player.monthlyPoints.toString()
            username.text = player.username
        }
    }
}