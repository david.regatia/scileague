package com.davidrferreira.scileague.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserBadge (
    @SerializedName("id")
    val id: Int,
    @SerializedName("creationDate")
    val creationDate: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("badgeId")
    val badgeId: Int,
    @SerializedName("isActive")
    val isActive: Boolean
    ) : Serializable

