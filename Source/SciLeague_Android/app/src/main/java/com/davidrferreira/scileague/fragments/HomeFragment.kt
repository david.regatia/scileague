package com.davidrferreira.scileague.fragments


import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.davidrferreira.scileague.activities.InsideAppActivity
import com.davidrferreira.scileague.databinding.FragmentHomeBinding
import com.davidrferreira.scileague.fragments.UnityFragment
import com.davidrferreira.scileague.models.Profile
import com.davidrferreira.scileague.models.ProfileUpperCase
import com.davidrferreira.scileague.models.UserPoints
import com.davidrferreira.scileague.networking.NetworkingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val networking = NetworkingService()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentHomeBinding.inflate(layoutInflater, container, false);

        binding.buttonGameSciDetective.setOnClickListener {
            launchUnityFragment()
        }

        return binding.root;
    }

    override fun onResume() {
        super.onResume()
        getHomeDetails()
    }

    private fun launchUnityFragment() {
        val unityFragment = UnityFragment();

        (activity as InsideAppActivity).binding.bottomNavigationView.visibility = View.GONE
        (activity as InsideAppActivity).changeOrientation(true)
        (activity as InsideAppActivity).setCurrentFragment(unityFragment, "unity")
    }


    private fun getHomeDetails() {
        getProfileDetails()
    }

    private fun  getProfileDetails() {

        val token = (activity as InsideAppActivity).getSharedPreferencesClientToken()
        if (token != null) {
            var response = networking.apiService.getMyProfile(token = token)

            response.enqueue(object: Callback<ProfileUpperCase> {

                override fun onResponse(call: Call<ProfileUpperCase>, response: Response<ProfileUpperCase>) {
                    if (response.isSuccessful) {
                        binding.nameTextView.text = "Hi, " + response.body()!!.firstName
                        binding.numberPointsTextView.text = response.body()!!.monthlyPoints.toString()
                        binding.rankPositionTextView.text = response.body()!!.position.toString()
                    }
                    else {
                        Toast.makeText(context,
                                "It was not possible to get the profile details.", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ProfileUpperCase>, t: Throwable) {
                    Toast.makeText(context,
                            "It was not possible to get the profile details.", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
}