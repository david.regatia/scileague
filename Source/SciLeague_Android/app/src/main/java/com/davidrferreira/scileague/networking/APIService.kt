package com.davidrferreira.scileague.networking

import com.davidrferreira.scileague.models.*
import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @POST("InternalLogin")
    fun login(
            @Body
            requestBody: LoginRequest
    ): Call<LoginResponse>

    @POST("SignUp")
    fun createAccount(
            @Body
            requestBody: Account
    ): Call<Account>

    @POST("EditProfile")
    fun createProfile(
            @Body
            requestBody: Profile
    ): Call<Profile>

    @GET("GetLeaderboardPlayers")
    fun getMonthlyTopPlayers(
         @Query("limit")
         limit: Int
    ): Call<List<Profile>>

    @GET("GetLeaderboardPosition")
    fun getLeaderboardPosition(
            @Header("Authorization")
            token: String
    ): Call<Int>

    @GET("GetMyProfile")
    fun getMyProfile(
            @Header("Authorization")
            token: String
    ): Call<ProfileUpperCase>

    @POST("VerifyBadgeStatus")
    fun verifyBadgeStatus(
            @Header("Authorization")
            token: String
    ): Call<UserBadge>

    @GET("GetAllActiveBadgesFromUser")
    fun getAllActiveBadgesFromUser(
            @Header("Authorization")
            token: String
    ): Call<List<Badge>>

//    @GET("accounts/getEmailLogged")
//    fun getEmailLogged(
//            @Header("AUTHORIZATION")
//            value: String
//    ): Call<String>
//
//    @GET("profiles/getMyProfile")
//    fun getMyProfile(
//            @Header("AUTHORIZATION")
//            value: String
//    ): Call<Profile>


}