package com.davidrferreira.scileague.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Badge (
        @SerializedName("id")
        val id: Int,
        @SerializedName("level")
        val level: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("imageName")
        val imageName: String
        ) : Serializable