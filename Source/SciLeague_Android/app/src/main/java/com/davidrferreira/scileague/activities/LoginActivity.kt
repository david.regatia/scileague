package com.davidrferreira.scileague.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.davidrferreira.scileague.databinding.ActivityLoginBinding
import com.davidrferreira.scileague.models.Badge
import com.davidrferreira.scileague.models.LoginRequest
import com.davidrferreira.scileague.models.LoginResponse
import com.davidrferreira.scileague.models.UserBadge
import com.davidrferreira.scileague.networking.NetworkingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding;
    var loginDone : Boolean = false;
    private val networking = NetworkingService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (loginDone) {
            val intent = Intent(this, InsideAppActivity::class.java);
            startActivity(intent);
        }

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loginSignUpButton.setOnClickListener {
            navigateToSignUpActivity();
        }

        binding.loginLoginButton.setOnClickListener {
            internalLogin()
            verifyBadgeStatus()
        }

        //saveSharedPreferencesPlayingGame(false);
        //saveSharedPreferencesCount(0);
    }

    override fun onStart() {
        super.onStart();

        if (loginDone) {
            val intent = Intent(this, InsideAppActivity::class.java);
            startActivity(intent);
        }
    }

    override fun onResume() {
        super.onResume()
    }


    private fun navigateToSignUpActivity() {
        val intent = Intent(this, SignUpActivity::class.java);
        startActivity(intent);
    }

    private fun internalLogin() {


        val username = binding.loginUsernameText.text.toString()
        val password = binding.loginPasswordText.text.toString()

        val login = LoginRequest(username, password)
        var response = networking.apiService.login(login)
        response.enqueue(object: Callback<LoginResponse> {

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.isSuccessful) {
                    saveSharedPreferencesClientToken(response.body()!!.token)
                    navigateToInsideAppActivity()
                }
                else {
                    Toast.makeText(applicationContext,
                            "Incorrect username or password.", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Toast.makeText(applicationContext,
                        "The service is currently unavailable.", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun verifyBadgeStatus() {
        var response = networking.apiService.verifyBadgeStatus("davidrferreira")
        response.enqueue(object: Callback<UserBadge> {

            override fun onResponse(call: Call<UserBadge>, response: Response<UserBadge>) {
                if (response.isSuccessful) {
                    // TODO: Display new badge.
                }
                else {
                    // TODO: Error Message.
                }
            }

            override fun onFailure(call: Call<UserBadge>, t: Throwable) {
                // TODO: Error Message.
            }
        })
    }


    private fun navigateToInsideAppActivity() {
        loginDone = true
        val intent = Intent(this, InsideAppActivity::class.java)
        startActivity(intent)
    }

    private fun saveSharedPreferencesClientToken(token : String) {
        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("token", "Bearer $token");
        editor.apply()
    }


//    private fun saveSharedPreferencesPlayingGame(playingGame : Boolean) {
//        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//        editor.putBoolean("playingGame", playingGame);
//        editor.apply()
//    }
//
//    private fun saveSharedPreferencesCount(count : Int) {
//        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//        editor.putInt("count", count);
//        editor.apply()
//    }
}

