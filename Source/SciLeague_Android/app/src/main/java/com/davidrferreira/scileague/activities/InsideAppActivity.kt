package com.davidrferreira.scileague.activities

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.davidrferreira.scileague.R
import com.davidrferreira.scileague.databinding.ActivityInsideAppBinding
import com.davidrferreira.scileague.fragments.*
import com.unity3d.player.UnityPlayer.UnitySendMessage


class InsideAppActivity : AppCompatActivity() {

    lateinit var binding: ActivityInsideAppBinding
    private var homeFragment = HomeFragment()
    private var leaderboardFragment = LeaderboardFragment()
    private var badgeFragment = BadgeFragment()
    private var settingsFragment = SettingsFragment()
    private var count = 0
    var fragmentID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInsideAppBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set navigation.
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.homeFragment -> {
                    fragmentID = -1
                    setCurrentFragment(homeFragment, "home")
                }
                R.id.leaderboardFragment -> {
                    setCurrentFragment(leaderboardFragment, "leaderboard")
                }
                R.id.badgeFragment -> {
                    setCurrentFragment(badgeFragment, "badge")
                }
//                R.id.settingsFragment -> {
//                    setCurrentFragment(settingsFragment, "settings")
//                }
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()

        println("********************** TOKEN: " +  getSharedPreferencesClientToken())

        if (fragmentID == 0) {
            fragmentID = -1
            setCurrentFragment(homeFragment, "home")
        }
        // If the process was killed after the user report the imposter
        // we need to present a popup with the points won.
        // Since Android kills the process when there is a rotation
        // There are going to be two processes killed (that's why the comparison with == 2).
//        count = getSharedPreferencesCount()
//        count++
//        saveSharedPreferencesCount(count)
//
//        if (getSharedPreferencesPlayingGame() && getSharedPreferencesCount() == 2) {
//            saveSharedPreferencesPlayingGame(false)
//            val taskCompletedFragment = TaskCompletedFragment()
//            setCurrentFragment(taskCompletedFragment, "taskCompleted")
//            count = 0
//        }
//        if (!getSharedPreferencesPlayingGame())
//            count = 0
//
//        saveSharedPreferencesCount(count)
    }


    fun setCurrentFragment(fragment: Fragment, tag: String) =
        supportFragmentManager.beginTransaction().apply {
            replace(binding.customFragment.id, fragment, tag)
            commit()
        }

    fun changeOrientation(changeToLandscape: Boolean) {
        requestedOrientation = if (changeToLandscape) {
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        } else {
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

//    fun saveSharedPreferencesPlayingGame(playingGame: Boolean) {
//        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//        editor.putBoolean("playingGame", playingGame);
//        editor.apply()
//    }
//
//    private fun saveSharedPreferencesCount(count: Int) {
//        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//        editor.putInt("count", count);
//        editor.apply()
//    }
//
//    private fun saveSharedPreferencesPoints(taskPoints: Int) {
//        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
//        val editor = sharedPreferences.edit()
//
//        var totalPoints = getSharedPreferencesTotalPoints() + taskPoints
//
//        editor.putInt("taskPoints", taskPoints)
//        editor.putInt("totalPoints", totalPoints)
//        editor.apply()
//    }

    public fun saveSharedPreferencesClientToken(token: String) {
        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("token", "Bearer $token");
        editor.apply()
    }

    public fun getSharedPreferencesClientToken() : String? {
        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
        return sharedPreferences.getString("token", "")
    }

    private fun getSharedPreferencesPlayingGame() : Boolean {
        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean("playingGame", false)
    }
//
//    private fun getSharedPreferencesCount() : Int {
//        val sharedPreferences = getSharedPreferences("Info", Context.MODE_PRIVATE)
//        return sharedPreferences.getInt("count", -1)
//    }

    fun navigateToLoginScreen() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("logout", true)
        startActivity(intent)
        finishAffinity()
    }


    private fun exitGame() {
        // This method is called by the unity fragment.
        // Don't delete it.
        //saveSharedPreferencesPlayingGame(false)
        finish()
    }

    private fun getClientToken() {
        // This method is called by the unity fragment.
        // Don't delete it.
        var token = getSharedPreferencesClientToken()
        UnitySendMessage("TasksManager", "SetClientToken", token.toString())
    }
}