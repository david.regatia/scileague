package com.davidrferreira.scileague.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProfileUpperCase  (
        @SerializedName("Id")
        val id: Int,
        @SerializedName("Username")
        val username: String,
        @SerializedName("FirstName")
        val firstName: String,
        @SerializedName("LastName")
        val lastName: String,
        @SerializedName("Birthday")
        val birthday: String,
        @SerializedName("Country")
        val country: String,
        @SerializedName("Role")
        val role: String,
        @SerializedName("MonthlyPoints")
        val monthlyPoints: Int,
        @SerializedName("TotalPoints")
        val totalPoints: Int,
        @SerializedName("Position")
        val position: Int
) : Serializable