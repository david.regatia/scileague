package com.davidrferreira.scileague.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LoginResponse (
        @SerializedName("username")
        val username: String,
        @SerializedName("firstName")
        val firstName: String,
        @SerializedName("lastName")
        val lastName: String,
        @SerializedName("token")
        val token: String
) : Serializable