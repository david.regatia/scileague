package com.davidrferreira.scileague.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.davidrferreira.scileague.R
import com.davidrferreira.scileague.activities.InsideAppActivity
import com.davidrferreira.scileague.databinding.FragmentLeaderboardBinding
import com.davidrferreira.scileague.databinding.FragmentTaskCompletedBinding


class TaskCompletedFragment : Fragment() {

    private lateinit var binding: FragmentTaskCompletedBinding;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = FragmentTaskCompletedBinding.inflate(layoutInflater, container, false)

        binding.confirmButton.setOnClickListener {
            confirmButtonTriggered()
        }

        val points : Int = 0;
        binding.pointsReceivedTextView.text = "You have received 0 points."

        if (points != 0) {
            binding.loseTextView.visibility = View.INVISIBLE
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart();
    }

    override fun onResume() {
        super.onResume();
    }

    private fun confirmButtonTriggered() {
        activity?.onBackPressed();
    }

}