package com.davidrferreira.scileague.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.davidrferreira.scileague.R

class MainActivity : AppCompatActivity() {

    var login : Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (login) {
            val intent = Intent(this, LoginActivity::class.java);
        } else {
            val intent = Intent(this, InsideAppActivity::class.java);
        }

        startActivity(intent);
    }
}