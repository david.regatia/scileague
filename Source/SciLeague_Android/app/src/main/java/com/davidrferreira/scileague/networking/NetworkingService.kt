package com.davidrferreira.scileague.networking

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class NetworkingService {

    val apiService: APIService = Retrofit.Builder()
            .baseUrl("https://reality.utad.net/scileague/api/gateway/")
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(APIService::class.java)
}
