package com.davidrferreira.scileague.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.davidrferreira.scileague.databinding.ActivitySignUpBinding
import com.davidrferreira.scileague.models.Account
import com.davidrferreira.scileague.models.UserBadge
import com.davidrferreira.scileague.networking.NetworkingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding;
    private val networking = NetworkingService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignUpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.signUpSignUpButton.setOnClickListener{
            signUp()
        }

        binding.signUpLoginButton.setOnClickListener{
            navigateToLoginActivity()
        }
    }

    private fun signUp() {

        val username = binding.signUpUsernameText.text.toString()
        val email = binding.emailText.text.toString()
        val password = binding.signUpPasswordText.text.toString()

        val account = Account(username, password, email)

        var response = networking.apiService.createAccount(account)
        response.enqueue(object: Callback<Account> {

            override fun onResponse(call: Call<Account>, response: Response<Account>) {
                if (response.isSuccessful) {
                    navigateToProfileActivity(response.body()!!.username)
                }
                else {
                    Toast.makeText(applicationContext,
                            "It was not possible to submit account details.", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Account>, t: Throwable) {
                Toast.makeText(applicationContext,
                        "The service is currently unavailable.", Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun navigateToProfileActivity(username: String) {
        val intent = Intent(this, EditProfileActivity::class.java)
        intent.putExtra("username", username)
        startActivity(intent)
    }

    private fun navigateToLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}