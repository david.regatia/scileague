package com.davidrferreira.scileague.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Profile  (
        @SerializedName("id")
        val id: Int,
        @SerializedName("username")
        val username: String,
        @SerializedName("firstName")
        val firstName: String,
        @SerializedName("lastName")
        val lastName: String,
        @SerializedName("birthday")
        val birthday: String,
        @SerializedName("country")
        val country: String,
        @SerializedName("role")
        val role: String,
        @SerializedName("monthlyPoints")
        val monthlyPoints: Int,
        @SerializedName("totalPoints")
        val totalPoints: Int,
        @SerializedName("position")
        val position: Int
) : Serializable