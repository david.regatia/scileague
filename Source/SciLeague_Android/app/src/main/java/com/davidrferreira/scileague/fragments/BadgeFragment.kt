package com.davidrferreira.scileague.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.davidrferreira.scileague.R
import com.davidrferreira.scileague.activities.InsideAppActivity
import com.davidrferreira.scileague.databinding.FragmentBadgeBinding
import com.davidrferreira.scileague.databinding.FragmentHomeBinding
import com.davidrferreira.scileague.models.Badge
import com.davidrferreira.scileague.models.Profile
import com.davidrferreira.scileague.models.UserPoints
import com.davidrferreira.scileague.networking.NetworkingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BadgeFragment : Fragment() {

    private lateinit var binding: FragmentBadgeBinding
    private val networking = NetworkingService()
    private var players :  ArrayList<Badge> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = FragmentBadgeBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        getMyBadges()
    }

    private fun getMyBadges() {

        val token = (activity as InsideAppActivity).getSharedPreferencesClientToken()

        if (token != null) {

            var response = networking.apiService.getAllActiveBadgesFromUser(token)
            response.enqueue(object: Callback<List<Badge>> {

                override fun onResponse(call: Call<List<Badge>>, response: Response<List<Badge>>) {

                    if (response.isSuccessful) {
                        players = ArrayList(response.body()!!)
                        displayBadges()
                    }
                    else {
                        Toast.makeText(context,
                                "It was not possible to get the badges details.", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<List<Badge>>, t: Throwable) {
                    Toast.makeText(context,
                            "It was not possible to get the badges details.", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private fun displayBadges() {
        binding.badge1.setImageResource(R.drawable.badge1)
        binding.badge2.setImageResource(R.drawable.badge2)
        binding.badge3.setImageResource(R.drawable.badge3)

        for (player in players) {
            when(player.level) {
                0 -> binding.badge1.setImageResource(R.drawable.badge1)
                1 -> binding.badge2.setImageResource(R.drawable.badge2)
                2 -> binding.badge3.setImageResource(R.drawable.badge3)
                3 -> binding.badge4.setImageResource(R.drawable.badge4)
                4 -> binding.badge5.setImageResource(R.drawable.badge5)
                else -> {
                    print("Badge not implemented in the app.");
                }
            }
        }
    }

}