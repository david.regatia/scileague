package com.davidrferreira.scileague.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.davidrferreira.scileague.R
import com.davidrferreira.scileague.databinding.ActivityEditProfileBinding
import com.davidrferreira.scileague.databinding.ActivitySignUpBinding
import com.davidrferreira.scileague.models.Account
import com.davidrferreira.scileague.models.Profile
import com.davidrferreira.scileague.networking.NetworkingService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditProfileBinding
    private val networking = NetworkingService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.editProfileConfirmButton.setOnClickListener {
            confirmProfileDetails()
        }
    }


    private fun confirmProfileDetails() {

        val firstName = binding.editProfileFirstNameText.text.toString()
        val lastName = binding.editProfileLastNameText.text.toString()
        val birthday = binding.editProfileBirthdayText.text.toString()
        val country = binding.editProfileCountryText.text.toString()
        val username= intent.getStringExtra("username")
        val profile = Profile(0, username!!, firstName, lastName, birthday, country, "player", 0, 0, 0)

        var response = networking.apiService.createProfile(profile)
        response.enqueue(object: Callback<Profile> {

            override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                if (response.isSuccessful) {
                    navigateToLoginActivity()
                }
                else {
                    Toast.makeText(applicationContext,
                            "It was not possible to submit profile details.", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Profile>, t: Throwable) {
                Toast.makeText(applicationContext,
                        "The service is currently unavailable.", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun navigateToLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}