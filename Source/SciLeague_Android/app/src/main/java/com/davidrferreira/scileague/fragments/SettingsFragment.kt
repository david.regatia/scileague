package com.davidrferreira.scileague.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.davidrferreira.scileague.R
import com.davidrferreira.scileague.activities.InsideAppActivity
import com.davidrferreira.scileague.activities.LoginActivity
import com.davidrferreira.scileague.adapters.LeaderboardAdapter
import com.davidrferreira.scileague.databinding.FragmentLeaderboardBinding
import com.davidrferreira.scileague.databinding.FragmentSettingsBinding


class SettingsFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding;


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = FragmentSettingsBinding.inflate(layoutInflater, container, false)

        binding.logoutButton.setOnClickListener {
            logout()
        }

        return binding.root
    }


    private fun logout() {
        (activity as InsideAppActivity).saveSharedPreferencesClientToken("")
        (activity as InsideAppActivity).fragmentID = 0
        (activity as InsideAppActivity).navigateToLoginScreen()
    }


}