﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TeamService.Models
{
    public class UserTeam
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public int TeamId { get; set; }
        public bool RequestAnswered { get; set; }
        public bool RequestAccepted { get; set; }
        public bool ActiveMember { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
