﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TeamService.Data;
using SciLeague.TeamService.Models;

namespace SciLeague.TeamService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserTeamsController : ControllerBase
    {
        private readonly SciLeagueTeamContext _context;

        public UserTeamsController(SciLeagueTeamContext context)
        {
            _context = context;
        }


        [HttpPost("CreateInvitation")]
        public async Task<ActionResult> CreateInvitation(int teamId, string invitedUsername)
        {
            UserTeam userTeam = new UserTeam();

            userTeam = await _context.UserTeam.SingleOrDefaultAsync(x => x.Id == teamId && x.Username == invitedUsername && x.RequestAnswered == true);

            if (userTeam != null)
            {
                return NotFound("There is already an unanswered invitation sent from this team to the specified user.");
            }

            userTeam.TeamId = teamId;
            userTeam.Username = invitedUsername;
            userTeam.RequestAnswered = false;
            userTeam.RequestAccepted = false;
            userTeam.ActiveMember = false;

            _context.UserTeam.Add(userTeam);
            _context.SaveChanges();

            return Ok(userTeam);
        }


        [HttpPost("KickTeamMember")]
        public async Task<ActionResult> KickTeamMember(int teamId, string username)
        {
            UserTeam userTeam = new UserTeam();

            userTeam = await _context.UserTeam.SingleOrDefaultAsync(x => x.Id == teamId && x.Username == username && x.ActiveMember == true);

            if (userTeam == null)
            {
                return NotFound("Not found any user with the specified username in the specified team.");
            }

            userTeam.ActiveMember = false;

            _context.SaveChanges();

            return Ok(userTeam);
        }



        [HttpPost("LeaveTeam")]
        public async Task<ActionResult> LeaveTeam(string username)
        {
            UserTeam userTeam = new UserTeam();

            userTeam = await _context.UserTeam.FirstOrDefaultAsync(x => x.Username == username && x.ActiveMember == true);

            if (userTeam == null)
            {
                return NotFound("Not found any user with the specified username in a team.");
            }

            userTeam.ActiveMember = false;

            _context.SaveChanges();

            return Ok(userTeam);
        }



        [HttpGet("GetTeamElements")]
        public async Task<ActionResult<IEnumerable<UserTeam>>> GetTeamElements(int teamId)
        {
            List<UserTeam> teamElements = new List<UserTeam>();

            teamElements = await _context.UserTeam.Where(x => x.TeamId == teamId && x.RequestAccepted == true && x.ActiveMember == true).ToListAsync();

            if (teamElements == null)
            {
                return NotFound();
            }

            return Ok(teamElements);
        }

        [HttpGet("GetTeamsWithLessThan5Members")]
        public async Task<ActionResult<IEnumerable<UserTeam>>> GetTeamsWithLessThan5Members()
        {
            List<int> teamIds = new List<int>();
            List<int> teamsNotFull = new List<int>();

            teamIds = await _context.UserTeam.Select(x => x.TeamId).ToListAsync();

            if (teamIds == null)
            {
                return NotFound();
            }

            foreach (var team in teamIds)
            {
                if (TeamIsNotFull(team))
                {
                    teamsNotFull.Add(team);
                }
            }

            return Ok(teamsNotFull);
        }


        [HttpGet("VerifyIfTeamIsNotFull")]
        public bool TeamIsNotFull(int teamId)
        {
            if (_context.UserTeam.Where(x => x.TeamId == teamId && x.ActiveMember == true).Count() < 5)
            {
                return true;
            }

            return false;
        }



        [HttpGet("GetTeamElements")]
        public async Task<ActionResult> IsUserOnATeam(string username)
        {
            UserTeam userTeam = new UserTeam();

            userTeam = await _context.UserTeam.SingleOrDefaultAsync(x => x.Username == username);

            if (userTeam == null)
            {
                return null;
            }

            return Ok(userTeam.TeamId);
        }


        [HttpPost("AnswerInvitation")]
        public async Task<ActionResult> AnswerInvitation(bool acceptInvitation, int invitationId)
        {
            UserTeam userTeam = new UserTeam();

            userTeam = await _context.UserTeam.SingleOrDefaultAsync(x => x.Id == invitationId);

            if (userTeam == null)
            {
                return NotFound("Invitation with specified id not found.");
            }

            userTeam.RequestAnswered = true;
            userTeam.RequestAccepted = acceptInvitation;
            userTeam.ActiveMember = acceptInvitation;

            _context.SaveChanges();

            return Ok(userTeam);
        }
    }
}
