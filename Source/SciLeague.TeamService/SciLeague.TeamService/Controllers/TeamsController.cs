﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.TeamService.Data;
using SciLeague.TeamService.Models;

namespace SciLeague.TeamService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly SciLeagueTeamContext _context;

        public TeamsController(SciLeagueTeamContext context)
        {
            _context = context;
        }


        [HttpGet("GetTeam")]
        public async Task<ActionResult<Team>> GetTeam(int? id, string name)
        {
            Team team = new Team();

            if (id != null)
            {
                team = await _context.Team.SingleOrDefaultAsync(x => x.Id == id);

                if (team != null)
                {
                    return team;
                }
            }


            if (name != null)
            {
                team = await _context.Team.SingleOrDefaultAsync(x => x.Name == name);

                if (team != null)
                {
                    return team;
                }
            }

            return NotFound();
        }



        [HttpPost("CreateTeam")]
        public async Task<ActionResult<Team>> CreateTeam([FromBody] Team team)
        {
           if (await _context.Team.SingleOrDefaultAsync(x => x.Name == team.Name) == null)
           {
                return BadRequest("There is already a team with the same name in the database");
           }

            Team newTeam = new Team();
            newTeam.Name = team.Name;
            newTeam.Activate = true;
            newTeam.CreationDate = DateTime.Now.ToString();

            _context.Team.Add(newTeam);
            _context.SaveChanges();

            return Ok(newTeam);
        }


        [HttpPost("DesactivateTeam")]
        public async Task<ActionResult<Team>> DesactivateTeam(bool desactivate, string name)
        {
            Team team = new Team();

            team = await _context.Team.SingleOrDefaultAsync(x => x.Name == name);

            if (team != null)
            {
                return BadRequest("Not found any team with that name.");
            }

            team.Activate = desactivate;

            _context.SaveChanges();

            return Ok(team);
        }
    }
}
