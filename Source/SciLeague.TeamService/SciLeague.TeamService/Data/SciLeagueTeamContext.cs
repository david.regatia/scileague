﻿using Microsoft.EntityFrameworkCore;
using SciLeague.TeamService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.TeamService.Data
{
    public class SciLeagueTeamContext : DbContext
    {
        public SciLeagueTeamContext(DbContextOptions<SciLeagueTeamContext> options) : base(options)
        {

        }

        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<UserTeam> UserTeam { get; set; }
    }
}
