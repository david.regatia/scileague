﻿using Microsoft.EntityFrameworkCore;
using SciLeague.AuthService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.AuthService.Data
{
    public class SciLeagueAuthContext : DbContext
    {
        public SciLeagueAuthContext(DbContextOptions<SciLeagueAuthContext> options) : base(options)
        {

        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
    }
}
