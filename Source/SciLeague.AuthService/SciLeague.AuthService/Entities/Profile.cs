﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.AuthService.Entities
{
    public class Profile
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Birthday { get; set; }
        public string Country { get; set; }
        public string Role { get; set; }
        public string DateCreated { get; set; }
        public string PhotoName { get; set; }
        public int MonthlyPoints { get; set; }
        public int TotalPoints { get; set; }

    }
}
