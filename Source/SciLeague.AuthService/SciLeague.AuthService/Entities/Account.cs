﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.AuthService.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public bool Blocked { get; set; }
        public string ActivationToken { get; set; }
        public string ExternalToken { get; set; }
    }
}
