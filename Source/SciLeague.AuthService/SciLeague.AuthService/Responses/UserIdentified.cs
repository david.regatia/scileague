﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.AuthService.Responses
{
    public class UserIdentified
    {
        public string Username { get; set; }
        public string Role { get; set; }
    }
}
