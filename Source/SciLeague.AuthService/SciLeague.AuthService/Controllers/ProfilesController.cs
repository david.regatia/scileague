﻿using Microsoft.AspNetCore.Mvc;
using SciLeague.AuthService.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SciLeague.AuthService.Data;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Newtonsoft.Json;

namespace SciLeague.AuthService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly SciLeagueAuthContext _context;

        public ProfilesController(SciLeagueAuthContext context)
        {
            _context = context;
        }

        // GET: api/Profiles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Profile>>> GetProfile()
        {
            return Ok(await _context.Profile.ToListAsync());
        }

        // GET: api/Profiles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Profile>> GetProfile(string id)
        {
            var profile = await _context.Profile.FindAsync(id);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }



        [HttpPost("CreateProfile")]
        public async Task<ActionResult<Profile>> CreateProfile([FromBody] Profile profile)
        {
            // Verify if there is an account in the DB with the email passed as query.
            Account account = _context.Account.Where(x => x.Username == profile.Username).FirstOrDefault();

            if (account == null)
            {
                return BadRequest();
            }

            profile.MonthlyPoints = 0;
            profile.TotalPoints = 0;
            _context.Profile.Add(profile);

            account.Username = profile.Username;
            //account.Profile = profile;

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProfile", new { id = profile.Username }, profile);
        }

        [HttpGet("GetMyProfile")]
        public async Task<ActionResult> GetMyProfile(string username)
        {
            Profile profile = await _context.Profile.Where(x => x.Username == username).FirstOrDefaultAsync();

            if (profile == null)
            {
                return NotFound("The user was not found");
            }


            // Compute position in leaderboard.
            List<Profile> listProfile = await _context.Profile.OrderByDescending(x => x.MonthlyPoints).ToListAsync();

            int place = 1;

            foreach (var prof in listProfile)
            {
                if (prof.Username == username)
                {
                    break;
                }

                place++;
            }

            var data = new
            {
                Username = profile.Username,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                Birthday = profile.Birthday,
                Role = profile.Role,
                DateCreated = profile.DateCreated,
                PhotoName = profile.PhotoName,
                MonthlyPoints = profile.MonthlyPoints,
                TotalPoints = profile.TotalPoints,
                Position = place
            };
            string jsonData = JsonConvert.SerializeObject(data);

            return Ok(jsonData);
        }

        [HttpPost("UpdatePoints")]
        public async Task<ActionResult> UpdatePoints(string username, int points)
        {
            Profile profile = await _context.Profile.Where(x => x.Username == username).FirstOrDefaultAsync();

            if (profile == null)
                return NotFound();

            profile.MonthlyPoints += points;
            profile.TotalPoints += points;

            _context.SaveChanges();

            return Ok(profile);
        }

        [HttpGet("GetLeaderboardPosition")]
        public async Task<ActionResult> GetLeaderboardPosition(string username)
        {
            List<Profile> listProfile = await _context.Profile.OrderByDescending(x => x.MonthlyPoints).ToListAsync();

            int place = 1;
            bool found = false;

            foreach (var profile in listProfile)
            {
                if (profile.Username == username)
                {
                    found = true;
                    break;
                }

                place++;
            }

            if (found == false)
                return NotFound("The user was not found");

            var data = new { Position = found };
            string jsonData = JsonConvert.SerializeObject(data);

            return Ok(data);
        }



        [HttpGet("GetMonthlyTopPlayers")]
        public async Task<ActionResult> GetMonthlyTopPlayers(int limit)
        {
            List<Profile> profiles = new List<Profile>();

            profiles = await _context.Profile.OrderByDescending(x => x.MonthlyPoints).Take(limit).ToListAsync();

            if (profiles == null)
            {
                return NotFound("Query problem.");
            }

            return Ok(profiles);
        }
    }
}
