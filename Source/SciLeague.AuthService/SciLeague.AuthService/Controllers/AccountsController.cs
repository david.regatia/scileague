﻿using Google.Apis.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SciLeague.AuthService.Data;
using SciLeague.AuthService.Entities;
using SciLeague.AuthService.Helpers;
using SciLeague.AuthService.Requests;
using SciLeague.AuthService.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SciLeague.AuthService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly SciLeagueAuthContext _context;

        public AccountsController(SciLeagueAuthContext context)
        {
            this._context = context;
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            var customer = _context.Account.SingleOrDefault(customer => customer.Active && customer.Username == loginRequest.Username);

            if (customer == null)
            {
                return BadRequest("Not found any user with that username that is active.");
            }

            bool verifiedPassword = BCrypt.Net.BCrypt.Verify(loginRequest.Password, customer.Password);

            if (!verifiedPassword)
            {
                return BadRequest("The username or the password are not correct.");
            }

            var token = await Task.Run(() => TokenHelper.GenerateToken(customer));

            LoginResponse loginResponse = new LoginResponse { Username = customer.Username, Token = token };

            if (loginResponse == null)
            {
                return BadRequest($"Invalid credentials");
            }

            return Ok(loginResponse);
        }



        [HttpGet("{id}")]
        public async Task<ActionResult<Account>> GetAccount(int id)
        {
            var account = await _context.Account.FindAsync(id);

            if (account == null)
            {
                return NotFound();
            }
            account.Password = "";
            return Ok(account);
        }


        // POST: api/Accounts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("CreateAccount")]
        public async Task<ActionResult<Account>> CreateAccount([FromBody] Account account)
        {
            // Verifies if there is any account with the same email on the DB.
            if (_context.Account.Where(x => x.Email == account.Email || x.Username == account.Username).Any())
            {
                return BadRequest("There is already an user with the same email or username.");
            }

            // Highlights the fact that the accounts are not activated in the beginning. 
            account.Active = true;
            account.ActivationToken = null;

            // Hash password using BCrypt.Net-Next library.
            account.Password = BCrypt.Net.BCrypt.HashPassword(account.Password);

            // Generate token for account verification.
            // This token is going to be sent on a url to the email of the user in order for him to activate the account.
            account.ActivationToken = Guid.NewGuid().ToString();

            // Prepare and send account confirmation email.

            string subject = "SciLeague: Registration Confirmation";

            string body = "Welcome to SciLeague\n" +
                    "Confirm your account with the next url:\n" +
                    "https://localhost:44305/" + "api/accounts/AccountConfirmation?" +
                    "email=" + account.Email + "&token=" + account.ActivationToken;

            //await Task.Run(() => EmailHelper.SendEmail(account.Email, subject, body));

            // Saves the account object to the DB.
            _context.Account.Add(account);
            await _context.SaveChangesAsync();

            // Returns the email of the created account.
            account.Password = "";
            return Ok(account);
        }


        [HttpGet("GetUserAuthAsync")]
        public async Task<UserIdentified> GetUserAuthAsync([FromHeader] string authorization)
        {
            UserIdentified userIdentified = new UserIdentified();

            var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;

            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            if (claim == null)
            {
                userIdentified.Username = "";
                userIdentified.Role = "";
                return userIdentified;
            }

            userIdentified.Username = claim.Value;
            var user = await _context.Profile.FirstOrDefaultAsync(x => x.Username == userIdentified.Username);
            userIdentified.Role = user.Role;

            return userIdentified;
        }


        // GET: api/Accounts/AccountConfirmation?email=xxxx&token=xxxx
        [HttpGet("AccountConfirmation")]
        public ActionResult AccountConfirmation(string email, string token)
        {
            Account account = _context.Account.Where(x => x.Email == email).FirstOrDefault();

            if (account == null)
            {
                return NotFound();
            }

            if (account.ActivationToken != token)
            {
                return Unauthorized();
            }

            account.Active = true;
            _context.SaveChanges();

            return Ok("The account confirmation was successful.");
        }



        [HttpGet("DoesAccountExist")]
        public async Task<Account> DoesAccountExist(string username, string password)
        {
            foreach (var sameUser in await _context.Account.Where(x => x.Username == username).ToListAsync())
            {
                if (BCrypt.Net.BCrypt.Verify(password, sameUser.Password))
                {
                    return sameUser;
                }
            }

            return null;
        }



        //    [HttpPost("VerifyGoogleToken")]
        //    public async Task<GoogleJsonWebSignature.Payload> VerifyGoogleToken([FromBody] ExternalAuthDto externalAuth)
        //    {
        //        try
        //        {
        //            // Create a ValidationSettings object and populate the Audience property with the
        //            // clientId of our application.
        //            // This ensures that the token validation is for our application.
        //            var settings = new GoogleJsonWebSignature.ValidationSettings()
        //            {
        //                Audience = new List<string>() { _googleSettings.GetSection("clientId").Value }
        //            };

        //            // Validates the token received from the client.
        //            var payload = await GoogleJsonWebSignature.ValidateAsync(externalAuth.IdToken, settings);

        //            // If it is valid, returns the GoogleJsonWebSignature.Payload object
        //            // that contains different user's information.
        //            return payload;
        //        }
        //        catch (Exception)
        //        {
        //            return null;
        //        }
        //    }


        //    [HttpPost("SignUpGoogle")]
        //    public async Task<Account> SignUpGoogle([FromBody] ExternalAuthDto externalAuth)
        //    {
        //        // Validate idToken.
        //        var payload = await VerifyGoogleToken(externalAuth);
        //        if (payload == null)
        //        {
        //            // Validation failed.
        //            return null;
        //        }

        //        // Search if there is already an account in the DB linked to this Google account. 
        //        var account = await _context.Account.Where(x => x.ExternalToken == externalAuth.IdToken).SingleOrDefaultAsync();

        //        if (account == null)
        //        {
        //            // There isn't. 

        //            // Search if there is already an account in the DB with the same email (but not linked to Google account).
        //            account = await _context.Account.Where(x => x.Email == payload.Email).SingleOrDefaultAsync();

        //            if (account == null)
        //            {
        //                // There isn't.
        //                // We are going to create a new account from the start.

        //                account = new Account();
        //                account.Email = payload.Email;
        //                account.ExternalToken = externalAuth.IdToken;
        //                account.Active = true;
        //                account.ActivationToken = null;

        //                // Saves the account object to the DB.
        //                _context.Account.Add(account);
        //                await _context.SaveChangesAsync();
        //            }
        //            else
        //            {
        //                // If there is already an account with the same email, we are just going to link it to the Google account.
        //                account.ExternalToken = externalAuth.IdToken;
        //                await _context.SaveChangesAsync();
        //            }
        //        }

        //        if (account == null)
        //        {
        //            // There was a problem.
        //            return null;
        //        }

        //        return account;
        //    }

        //    public class ExternalAuthDto
        //    {
        //        public string Provider { get; set; }
        //        public string IdToken { get; set; }
        //    }




        //    [HttpPost("internalLogin")]
        //    public async Task<IActionResult> InternalLogin(LoginRequest loginRequest)
        //    {
        //        Account account = await DoesAccountExist(loginRequest.Username, loginRequest.Password);

        //        if (account != null)
        //        {
        //            var token = await Task.Run(() => TokenHelper.GenerateToken(account));

        //            return Ok(token);
        //        }

        //        return BadRequest("Internal Login Error");
        //    }

        //    [HttpPost("googleLogin")]
        //    public async Task<IActionResult> GoogleLogin([FromBody] ExternalAuthDto externalAuth)
        //    {
        //        Account account = await SignUpGoogle(externalAuth);

        //        if (account != null)
        //        {
        //            var token = await Task.Run(() => TokenHelper.GenerateToken(account));

        //            return Ok(token);

        //        }

        //        return BadRequest("Google Login Error");
        //    }


        //    [HttpGet("GetEmailLogged")]
        //    public async Task<IActionResult> GetUsernameLogged()
        //    {
        //        var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;

        //        var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

        //        if (claim == null)
        //        {
        //            return Unauthorized("Invalid account");
        //        }

        //        return Ok(claim.Value);
        //    }


        //    [HttpGet("getasync")]
        //    public async Task<IActionResult> GetAsync(LoginRequest loginRequest)
        //    {
        //        var loginResponse = await _accountService.Login(loginRequest);

        //        if (loginResponse == null)
        //        {
        //            return BadRequest($"Invalid credentials");
        //        }

        //        return Ok(loginResponse);
        //    }
        //}

        //public class Audience
        //{
        //    public string Secret { get; set; }
        //    public string Iss { get; set; }
        //    public string Aud { get; set; }
        //}

        //public class LoginData
        //{
        //    public string username { get; set; }
        //    public string password { get; set; }
        //}

        //public class ExternalAuthDto
        //{
        //    public string Provider { get; set; }
        //    public string IdToken { get; set; }
        //}
    }

}

