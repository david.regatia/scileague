﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SciLeague.AuthService.Migrations
{
    public partial class FirstName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profile_Account_AccountId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Profile_AccountId",
                table: "Profile");

            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "Profile");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountId",
                table: "Profile",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profile_AccountId",
                table: "Profile",
                column: "AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profile_Account_AccountId",
                table: "Profile",
                column: "AccountId",
                principalTable: "Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
