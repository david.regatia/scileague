﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.AuthService.Requirements
{
    public class AccountBlockedStatusRequirement : IAuthorizationRequirement
    {
        public bool IsBlocked { get; }
        public AccountBlockedStatusRequirement(bool isBlocked)
        {
            IsBlocked = isBlocked;
        }

    }
}
