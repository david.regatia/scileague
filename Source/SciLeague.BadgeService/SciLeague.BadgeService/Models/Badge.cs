﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.BadgeService.Models
{
    public class Badge
    {
        [Key]
        public int Id { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
    }
}
