﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.BadgeService.Models
{
    public class UserBadge
    {
        [Key]
        public int Id { get; set; }
        public string CreationDate { get; set; }
        public string Username { get; set; }
        public int BadgeId { get; set; }
        public bool IsActive { get; set; }
    }
}
