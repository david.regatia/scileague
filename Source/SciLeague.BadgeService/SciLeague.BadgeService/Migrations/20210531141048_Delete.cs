﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SciLeague.BadgeService.Migrations
{
    public partial class Delete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LowerLimit",
                table: "Badge");

            migrationBuilder.DropColumn(
                name: "UpperLimit",
                table: "Badge");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LowerLimit",
                table: "Badge",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UpperLimit",
                table: "Badge",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
