﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SciLeague.BadgeService.Migrations
{
    public partial class AddNewProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "UserBadge",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LowerLimit",
                table: "Badge",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UpperLimit",
                table: "Badge",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "UserBadge");

            migrationBuilder.DropColumn(
                name: "LowerLimit",
                table: "Badge");

            migrationBuilder.DropColumn(
                name: "UpperLimit",
                table: "Badge");
        }
    }
}
