﻿using Microsoft.EntityFrameworkCore;
using SciLeague.BadgeService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SciLeague.BadgeService.Data
{
    public class SciLeagueBadgeContext : DbContext
    {
        public SciLeagueBadgeContext(DbContextOptions<SciLeagueBadgeContext> options) : base(options)
        {

        }

        public virtual DbSet<Badge> Badge { get; set; }
        public virtual DbSet<UserBadge> UserBadge { get; set; }
    }
}
