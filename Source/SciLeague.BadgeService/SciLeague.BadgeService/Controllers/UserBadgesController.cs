﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SciLeague.BadgeService.Data;
using SciLeague.BadgeService.Models;

namespace SciLeague.BadgeService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserBadgesController : ControllerBase
    {
        private readonly SciLeagueBadgeContext _context;

        public UserBadgesController(SciLeagueBadgeContext context)
        {
            _context = context;
        }



        [HttpPost("CreateUserBadge")]
        public async Task<ActionResult> CreateUserBadge(UserBadge userBadge)
        {
            if (await _context.UserBadge.FirstOrDefaultAsync(x => x.Username == userBadge.Username || x.BadgeId == userBadge.BadgeId) != null)
            {
                return BadRequest("There is already an entry in the DB with for the same badge and user combination.");
            }

            UserBadge newUserBadge = new UserBadge();
            newUserBadge.BadgeId = userBadge.BadgeId;
            newUserBadge.CreationDate = DateTime.Today.ToString();
            newUserBadge.IsActive = true;
            newUserBadge.Username = userBadge.Username;

            _context.UserBadge.Add(newUserBadge);
            await _context.SaveChangesAsync();

            return Ok(newUserBadge);
        }

        [HttpPost("CreateUserBadgeForUsername")]
        public async Task<ActionResult> CreateUserBadgeForUsername(int level, string username)
        {
            Badge badge = await _context.Badge.Where(x => x.Level == level).FirstOrDefaultAsync();

            if (badge == null)
            {
                return BadRequest();

            }

            UserBadge newUserBadge = new UserBadge();
            newUserBadge.BadgeId = badge.Id;
            newUserBadge.CreationDate = DateTime.Today.ToString();
            newUserBadge.IsActive = true;
            newUserBadge.Username = username;

            _context.UserBadge.Add(newUserBadge);
            await _context.SaveChangesAsync();

            return Ok(newUserBadge);
        }


        [HttpPost("DeleteUserBadge")]
        public async Task<ActionResult> DeleteUserBadge(string username, int badgeId)
        {
            UserBadge userBadge = new UserBadge();

            userBadge = await _context.UserBadge.FirstOrDefaultAsync(x => x.Username == username && x.BadgeId == badgeId);

            if (userBadge == null)
            {
                return BadRequest("No entry found.");
            }

            _context.UserBadge.Remove(userBadge);
            await _context.SaveChangesAsync();

            return Ok("Entry deleted from database.");
        }



        [HttpGet("GetAllUserBadges")]
        public async Task<List<UserBadge>> GetAllUserBadges(string username)
        {
            List<UserBadge> userBadges = new List<UserBadge>();

            userBadges = await _context.UserBadge.Where(x => x.Username == username).ToListAsync();

            return userBadges;
        }


        [HttpGet("GetUserMostValuableBadge")]
        public async Task<UserBadge> GetUserMostValuableBadge(string username)
        {
            List<UserBadge> userBadges = new List<UserBadge>();
            List<int> userBadgesIds = new List<int>();
            UserBadge mostValuable = new UserBadge();

            // Get all user badges.
            userBadges = await GetAllUserBadges(username);

            // Keep in a list all user badges ids.
            foreach (var userBadge in userBadges)
            {
                userBadgesIds.Add(userBadge.BadgeId);
            }
            int[] idList = userBadgesIds.ToArray();


            // Send idsArray to BadgesController in order to receive the id of the most valuable badge in that array.
            string baseUrl = "https://localhost:44317/api/Badges/GetMostValuableBadge";
            string dataString;

            using (HttpClient client = new HttpClient())
            {
                var serializedProduct = JsonConvert.SerializeObject(idList);
                var body = new StringContent(serializedProduct, Encoding.UTF8, "application/json");

                using (HttpResponseMessage res = await client.PostAsync(baseUrl, body))
                using (HttpContent content = res.Content)
                {
                    dataString = await content.ReadAsStringAsync();
                }
            }

            mostValuable = await _context.UserBadge.FirstOrDefaultAsync(x => x.BadgeId == int.Parse(dataString));

            return mostValuable;
        }


        [HttpGet("GetDateFromLastBadgeWon")]
        public async Task<IActionResult> GetDateFromLastBadgeWon(string username)
        {
            UserBadge userBadge = new UserBadge();
            userBadge = await _context.UserBadge.Where(x => x.Username == username).OrderByDescending(x => x.CreationDate).Take(1).FirstOrDefaultAsync();

            return Ok(userBadge.CreationDate);
        }


        [HttpGet("GetAllActiveBadgesFromUser")]
        public async Task<IActionResult> GetAllActiveBadgesFromUser(string username)
        {
            List<UserBadge> userBadges = await _context.UserBadge.Where(x => x.Username == username && x.IsActive == true).ToListAsync();
            List<Badge> badges = new List<Badge>();

            foreach (var userB in userBadges)
            {
                badges.Add(_context.Badge.Where(x => x.Id == userB.Id).FirstOrDefault());
            }

            return Ok(badges);
        }


        [HttpPost("VerifyBadgeStatus")]
        public async Task<IActionResult> VerifyBadgeStatus(int numberDays, string username)
        {
            List<UserBadge> userBadge = new List<UserBadge>();
            UserBadge highestUserBadge = new UserBadge();
            int highestLevel = 0;
            Badge badge = new Badge();
            UserBadge found = new UserBadge();

            // Check user's highest active badge.
            userBadge = await _context.UserBadge.Where(x => x.Username == username && x.IsActive == true).ToListAsync();
            if (userBadge != null)
            {
                foreach (var userB in userBadge)
                {
                    badge = _context.Badge.Where(x => x.Id == userB.BadgeId).FirstOrDefault();

                    if (badge != null)
                    {
                        if (highestLevel <= badge.Level)
                        {
                            highestLevel = badge.Level;
                            highestUserBadge = userB;
                        }
                    }
                }
            }


            DateTime date = DateTime.Parse(highestUserBadge.CreationDate);

            if (numberDays >= 4)
            {
                // If the user has been playing for 4 or more consecutive days.

                if ((DateTime.Now.Date - date.Date).Days < 4)
                {
                    // If the last badge was won less than 4 days ago.
                    // The user is not going to win another one for now.

                    return BadRequest();
                }


                Badge newBadge = await _context.Badge.Where(x => x.Level == badge.Level + 1).FirstOrDefaultAsync();
                if (newBadge == null)
                {
                    // If there is not an higher badge to win.
                    // The user is not going to win one.
                    return BadRequest();
                }

                found = _context.UserBadge.Where(x => x.BadgeId == newBadge.Id && x.Username == username).FirstOrDefault();
                if (found == null)
                {
                    // If there is not an entry in the DB for the user associated to the next badge.
                    // We are going to create an entry.

                    UserBadge newUserBadge = new UserBadge();
                    newUserBadge.BadgeId = newBadge.Id;
                    newUserBadge.CreationDate = DateTime.Now.Date.ToString();
                    newUserBadge.IsActive = true;
                    newUserBadge.Username = username;
                }
                else
                {
                    // If there is already an entry.
                    // We are just going to make it active.
                    found.IsActive = true;
                }

           
            }
            else if (numberDays <= -3) {

                // If the user has not been playing for 3 or more consecutive days.

                if ((DateTime.Now.Date - date.Date).Days < 3)
                {
                    // If the last badge was lost less than 3 days ago.
                    // The user is not going to lose another one for now.

                    return BadRequest();
                }

                Badge newBadge = await _context.Badge.Where(x => x.Level == badge.Level).FirstOrDefaultAsync();
                if (newBadge == null || badge.Level == 0)
                {
                    // If there is not an lower badge than the current one.
                    // The user is not going to lose.
                    return BadRequest();
                }

                // We make the current badge as not active.
                found = _context.UserBadge.Where(x => x.BadgeId == newBadge.Id && x.Username == username).FirstOrDefault();

                found.IsActive = false;
            }
            else
            {
                return BadRequest();
            }

            return Ok(found);
        }


        [HttpGet("GetAllUserBadges")]
        public async Task<ActionResult> GetAllUserBadges()
        {
            List<UserBadge> badges = new List<UserBadge>();

            badges = await _context.UserBadge.ToListAsync();

            return Ok(badges);
        }
    }
}
