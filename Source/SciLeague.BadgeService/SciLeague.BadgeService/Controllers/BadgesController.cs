﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SciLeague.BadgeService.Data;
using SciLeague.BadgeService.Models;

namespace SciLeague.BadgeService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BadgesController : ControllerBase
    {
        private readonly SciLeagueBadgeContext _context;

        public BadgesController(SciLeagueBadgeContext context)
        {
            _context = context;
        }


        [HttpPost("CreateBadge")]
        public async Task<ActionResult> CreateBadge([FromBody] Badge badge)
        {
            if (await _context.Badge.FirstOrDefaultAsync(x => x.Name == badge.Name || x.Level == badge.Level) != null)
            {
                return BadRequest("There is already an entry in the DB with the same name or level.");
            }

            Badge newBadge = new Badge();
            newBadge.Level = badge.Level;
            newBadge.Name = badge.Name;
            newBadge.ImageName = badge.ImageName;

            _context.Badge.Add(newBadge);
            await _context.SaveChangesAsync();

            return Ok(newBadge);
        }


        [HttpPost("DeleteBadge")]
        public async Task<ActionResult> DeleteBadge(string name, int? level, int? id)
        {
            Badge badge = new Badge();
            bool found = false;

            if (name == null && id == null && level == null)
            {
                return BadRequest("No arguments were passed with the request.");
            }

            if (name != null && found == false)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Name == name);

                if (badge == null)
                {
                    return BadRequest("Not found any entry with the specified badge name");
                }

                found = true;
            }

            if (id != null && found == false)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Id == id);

                if (badge == null)
                {
                    return BadRequest("Not found any entry with the specified badge id");
                }
            }

            if (level != null && found == false)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Level == level);

                if (badge == null)
                {
                    return BadRequest("Not found any entry with the specified badge level");
                }
            }

            _context.Badge.Remove(badge);
            await _context.SaveChangesAsync();

            return Ok();
        }



        [HttpGet("GetAllAvailableBadges")]
        public async Task<ActionResult> GetAllAvailableBadges()
        {
            return Ok(await _context.Badge.ToListAsync());
        }


        [HttpGet("GetBadge")]
        public async Task<IActionResult> GetBadge(string name, int? level, int? id)
        {
            Badge badge = new Badge();
            bool found = false;

            if (name == null && id == null && level == null)
            {
                return BadRequest("No arguments were passed with the request.");
            }

            if (name != null && found == false)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Name == name);

                if (badge == null)
                {
                    return BadRequest("Not found any entry with the specified badge name");
                }

                found = true;
            }

            if (id != null && found == false)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Id == id);

                if (badge == null)
                {
                    return BadRequest("Not found any entry with the specified badge id");
                }
            }

            if (level != null && found == false)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Level == level);

                if (badge == null)
                {
                    return BadRequest("Not found any entry with the specified badge level");
                }
            }

            return Ok(badge);
        }


        [HttpGet("GetMostValuableBadge")]
        public async Task<IActionResult> GetMostValuableBadge(int[] idList)
        {
            if (idList == null)
            {
                return BadRequest("No arguments were passed with the request.");
            }

            Badge badge = new Badge();
            Badge mostValuableBadge = new Badge();

            mostValuableBadge = _context.Badge.FirstOrDefault(x => x.Id == idList[0]);

            if (mostValuableBadge == null)
            {
                return BadRequest();
            }

            foreach (var badgeId in idList)
            {
                badge = await _context.Badge.FirstOrDefaultAsync(x => x.Id == badgeId);

                if (mostValuableBadge.Level < badge.Level)
                {
                    mostValuableBadge = badge;
                }
            }

            return Ok(mostValuableBadge.Id);
        }


        [HttpGet("GetLevelOfBadge")]
        public async Task<ActionResult> GetLevelOfBadge(int id)
        {
            Badge badge = new Badge();

            badge = await _context.Badge.FirstOrDefaultAsync(x => x.Id == id);

            if (badge == null)
            {
                return NotFound(); ;
            }

            return Ok(badge.Level);
        }


        [HttpGet("GetIdOfBadge")]
        public async Task<ActionResult> GetIdOfBadge(int level)
        {
            Badge badge = new Badge();

            badge = await _context.Badge.FirstOrDefaultAsync(x => x.Level == level);

            if (badge == null)
            {
                return NotFound();
            }

            return Ok(badge.Id);
        }

    }
}
